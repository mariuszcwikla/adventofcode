#!/usr/bin/env ruby

fuel = 0

total_fuel = fuel
while x = gets
  x.strip!
  break if x.empty?
  f = (x.to_i/3).round - 2
  fuel += f
  total_fuel += f
  loop do
    f = (f/3).round - 2
    break if f<=0
    total_fuel += f
  end


end

puts "Part #1: #{fuel}"

puts "Part #2: #{total_fuel}"

#Answers:
#4980297 - too high
