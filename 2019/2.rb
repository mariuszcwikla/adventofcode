vals = gets.split(",").map &:to_i
def execute(vals)

    i = 0
    while true
        op = vals[i]
        a = vals[vals[i+1]]
        b = vals[vals[i+2]]
        if op==1
            c = a+b
        elsif op==2
            c = a*b
        elsif op==99
            break
        end
        vals[vals[i+3]]=c
        i+=4
    end
    return vals[0]
end

vals[1]=12
vals[2]=2

r = execute(vals.dup)
puts "Part #1: #{r}"

for i in 0...100
    for j in 0...100
        v = vals.dup
        v[1]=i
        v[2]=j
        if execute(v)==19690720
            puts "Part #2: #{i*100 + j}"
        end
    end
end
