
a = 197487
b = 673251

def meets_criteria(v)
    v = v.to_s
    same_adj = false
    for i in 1...v.length
        same_adj = true if v[i-1]==v[i]
        return false if v[i].ord < v[i-1].ord
    end
    return same_adj
end 

n = 0
for i in a..b
    n+=1 if meets_criteria(i)
end
puts "Part #1: #{n}"



def meets_criteria(v)
    v = v.to_s
    same_adj = false	
	cnt = 0
	has_double = false
    for i in 1...v.length
        same_adj = true if v[i-1]==v[i]
        return false if v[i].ord < v[i-1].ord			
		
		if v[i-1]==v[i]
			cnt+=1			
		else
			has_double = true if cnt==2
			cnt=0
		end
			
    end
		
    return same_adj && has_double
end 

n = 0
for i in a..b
    n+=1 if meets_criteria(i)
end
puts "Part #2: #{n}"


#Part 2 answers:
# 450 - too low