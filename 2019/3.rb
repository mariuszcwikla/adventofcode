def plot(map, cable)
    
end

c1 = gets.split(",")
c2 = gets.split(",")


map={}
x,y=0,0
step = 0

c1.each do |instr|
    dir = instr[0]
    len = instr[1..-1].to_i
    len.times do
        case dir 
            when 'R' then x+=1
            when 'L' then x-=1
            when 'U' then y+=1
            when 'D' then y-=1
        end        
        step+=1
        map[[y, x]]=step
    end
end

length1 = step

step = 0
mindist=nil   #For Part 1 
mindist2=nil  #For Part 2
x,y=0,0

length2 = 0
c2.each do |instr|
    length2 += instr[1..-1].to_i
end

c2.each do |instr|
    dir = instr[0]
    len = instr[1..-1].to_i
    len.times do
        case dir 
            when 'R' then x+=1
            when 'L' then x-=1
            when 'U' then y+=1
            when 'D' then y-=1
        end        
        step+=1
        if !map[[y,x]].nil?
            d = y.abs + x.abs
            mindist = d if mindist.nil? or d < mindist
            
            dist1 = map[[y,x]]
            dist2 = step
            mindist2 = (dist1+dist2) if mindist2.nil? or (dist1 + dist2)<mindist2
        end
        #map[[y,x]]=1
    end
end

puts mindist
puts mindist2

#Answer: 109 - too low
#330 - to low


