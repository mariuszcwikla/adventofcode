
vals = STDIN.gets.split(",").map &:to_i

input=1

def execute(code, input, output)

  memory = code
  def paramvalue(memory, param, mode)
    if mode==0
      return memory[param]
    else
      return param
    end
  end
  i = 0
  while i < memory.size
    instr=code[i]
    i+=1
    op = instr % 100
    v = instr/100
    mode1 = v%10
    v/=10
    mode2 = v%10
    v/=10
    mode3 = v%10
     if op==1 or op==2
      a = paramvalue(memory, memory[i], mode1)
      i+=1
      b = paramvalue(memory, memory[i], mode2)
      i+=1
      if op==1
        c = a+b
      else
        c = a*b
      end
      memory[memory[i]]=c
      i+=1
    elsif op==3  
      memory[memory[i]]=input
      i+=1
    elsif op==4
      a = paramvalue(memory, memory[i], mode1); i+=1
      output = a 
    elsif (5..8).include? op
      a = paramvalue(memory, memory[i], mode1); i+=1
      b = paramvalue(memory, memory[i], mode2); i+=1
      case op 
      when 5
        i = b if a != 0
      when 6
        i = b if a == 0
      when 7
        c = memory[i]; 
        i+=1
        if a < b
          memory[c]=1
        else
          memory[c]=0
        end
      when 8
        c = memory[i]; i+=1
        if a == b
          memory[c]=1
        else
          memory[c]=0
        end
      end 
    elsif op==99
      break
    end
  end
  return output
end

input = 5
if ARGV.length>0
  input = ARGV[0].to_i
end
r = execute(vals, input, 0)
puts "Part #2: #{r}"



