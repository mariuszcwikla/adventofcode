#!/usr/bin/env ruby

#orbits=Hash.new{|h, k| h[k]=[k]}

orbits={}
while line=gets
  a, b = line.strip.split ")"
  # orbits[a] += orbits[b]
  orbits[b]=a
end

n = 0
orbits.each{|k, v| 
  while orbits.has_key? k
    n+=1
    k=orbits[k]
  end
}
puts n


def get_path(h, k)
  path = []
  k = h[k]
  while h.has_key? k
    path << h[k]
    k=h[k]
  end
  path
end

path1=get_path(orbits, 'YOU')
path2=get_path(orbits, 'SAN')

steps = 0
path1.each{|v|
  steps+=1
  i=path2.index v
  unless i.nil?
    steps+=(i+1)
    break
  end
}


puts "Part 2: #{steps}"
