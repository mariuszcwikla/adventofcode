polymer = gets.strip
gets
$rules = {}
while line = gets&.strip
    a, _, b = line.split
    $rules[a]=b
end

$heights = {}

def compute_frequency(rule, c, height)
    return 0 if height == 0
        
    freq = $heights[[rule,c,height]]
    return freq unless freq.nil?

    incr = 0
    mid = $rules[rule]
    raise 'nil' if mid.nil?
    incr = 1 if mid == c
    freq = compute_frequency(rule[0] + mid, c, height - 1) + incr +  compute_frequency(mid + rule[1], c, height - 1)
    $heights[[rule, c, height]] = freq
    freq
end

rule = $rules.first[0]
puts compute_frequency(rule, rule[0], 40)
require 'set'
all_chars = Set.new
$rules.each do |r, _|
    all_chars << r[0]
    all_chars << r[1]
end


char_frequency = {}
char_frequency.default = 0
polymer.each_char do |c| char_frequency[c] += 1 end
for i in 1...polymer.size
    rule = polymer[i-1] + polymer[i]
    all_chars.each do |c|
        puts "Computing #{rule} (#{c})"
        f = compute_frequency(rule, c, 40)
        char_frequency[c] += f
    end
end
# $rules.each do |rule, _|
#     all_chars.each do |c|
#         puts "Computing #{rule} (#{c})"
#         f = compute_frequency(rule, c, 40)
#         char_frequency[c] += f
#     end
# end

p char_frequency

a = char_frequency.values.max
b = char_frequency.values.min

puts a - b

=begin
Aby zaimplementować optymalnie algorytm dla części 2, sprowadź to do problemu dwóch liter, np.

AB

AB -> B
BB -> A
BA -> A
AA -> B



=end