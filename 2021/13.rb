require 'scanf'
markers = []
width = 0
height = 0
while line = gets&.strip
    if line == ''
        break
    end
    x, y = line.scanf("%d,%d")
    markers << [x, y]
    width = [width, x].max
    height = [height, y].max
end
width +=1 
height += 1
fold_instructions = []
while line = gets&.strip
    axis, pos = line.scanf("fold along %c=%d")
    fold_instructions << [axis, pos]
end

#map = Array.new(height){Array.new(width){0}}
map = {}
markers.each do |x, y|
    map[[x, y]]=1
end

def fold_vertically(map, width, height, pos)
    new_map = {}
    for y in 0...height
        for x in 0..pos
            new_map[[x, y]] = 1 if map[[x,y]]==1
        end
    end
    for y in 0...height
        for x in 0..pos
            if map[[width - x - 1, y]] == 1
                new_map[[x, y]] = 1
            end
        end
    end
    return [new_map, pos, height]
end

def fold_horizontally(map, width, height, pos)
    new_map = {}
    for y in 0..pos
        for x in 0...width
            new_map[[x, y]] = 1 if map[[x,y]]==1
        end
    end
    for y in 0..pos
        for x in 0...width
            if map[[x, height - y - 1]] == 1
                new_map[[x, y]] = 1
            end
        end
    end
    return [new_map, width, pos]
end

def do_fold(map, w, h, axis, pos)
    if axis == 'x'
        fold_vertically(map, w, h, pos)
    else
        fold_horizontally(map, w, h, pos)
    end
end

def print_map(map, width, height)
    for y in 0...height
        for x in 0...width
            if map[[x,y]]==1
                print 'x'
            else
                print ' '
            end
        end
        puts ""
    end
end

# print_map(map, width, height)
map, width, height = do_fold(map, width, height, *fold_instructions[0])
# print_map(map, width, height)
puts map.size

fold_instructions[1..-1].each do |axis, pos|
    map, width, height = do_fold(map, width, height, axis, pos)
end

print_map(map, width, height)