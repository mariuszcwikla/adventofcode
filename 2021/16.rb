hex_string = gets.strip

class BitsParser
    attr_reader :pos
    def initialize(bits)
        @bits = bits
        @pos = 0
    end

    def fetch(num_of_bits)
        fetch_bits(num_of_bits).to_i(2)
    end
    def fetch_bits(num_of_bits)
        r = @bits[@pos...(@pos + num_of_bits)]
        @pos += num_of_bits
        r
    end
    def goto_next_byte
        return if @pos % 8 == 0
        @pos += (8 - @pos % 8)
    end
    def end?
        return @pos == @bits.size
    end
end

def parse(parser, level=0, &block)
    
    version = parser.fetch(3)
    type_id = parser.fetch(3)

    if type_id == 4
        literal = 0
        loop do
            group = parser.fetch(5)
            literal = (literal << 4) | (group & 0b1111)
            break if (group & 0b10000) == 0
        end
        # parser.goto_next_byte
        block.call(level, version, type_id, literal)
        return literal
    else
        length_type_id = parser.fetch(1)
        block.call(level, version, type_id, length_type_id)
        values = []
        if length_type_id == 0
            num_of_bits = parser.fetch(15)
            bits = parser.fetch_bits(num_of_bits)
            new_parser = BitsParser.new(bits)
            until new_parser.end?
                values << parse(new_parser, level+1, &block)
            end
        else
            num_of_subpackets = parser.fetch(11)
            num_of_subpackets.times do
                values << parse(parser, level+1, &block)
            end
        end
        result = case type_id
                when 0
                    values.reduce(:+)
                when 1
                    values.reduce(:*)
                when 2
                    values.min
                when 3
                    values.max
                when 5
                    if values[0] > values[1]
                        1
                    else
                        0
                    end
                when 6
                    if values[0] < values[1]
                        1
                    else
                        0
                    end
                when 7
                    if values[0]==values[1]
                        1
                    else
                        0
                    end
                end
        return result
    end

end

bits = hex_string.each_char.map{|c| c.to_i(16).to_s(2).rjust(4, '0')}.join('')
parser = BitsParser.new(bits)
sum_of_version = 0
part2 = parse(parser) do |level, v, t, literal|
    p [level, v, t, literal]
    sum_of_version += v
end

puts sum_of_version
puts part2