require 'scanf'
area = gets.scanf("target area: x=%d..%d, y=%d..%d")
x1, x2, y1, y2 = area
area[2], area[3] = area[3], area[2] if area[2] > area[3]

def is_overshoot(pos, area)
    return pos[0] > area[0..1].max || pos[1] < area[2..3].min
end

def is_in_area(pos, area)
    return (area[0]..area[1]).include?(pos[0]) && (area[2]..area[3]).include?(pos[1])
end

def run_simulation(velocity, area)
    pos = [0, 0]
    steps = 0
    loop do
        return [steps, true] if is_in_area(pos, area)
        return [steps, false] if is_overshoot(pos, area)
        return [steps, false] if velocity[0]==0 && !(area[0]..area[1]).include?(pos[0])
        
        pos[0]+=velocity[0]
        pos[1]+=velocity[1]

        velocity[0]-=1 if velocity[0] > 0
        velocity[1]-=1
        steps += 1
    end
end

run_simulation([1,25], area)
max = 0
y = 1
loop do
    # puts "y=#{y}, max=#{max}"
    # for x in 1..x2
        x = 19
        steps, b = run_simulation([x, y], area)
        p [steps, b, x, y] if b
        if b
            max = [max, y].max
            p max
        end
        # break if steps <= 1
    # end
    y += 1
end

p max