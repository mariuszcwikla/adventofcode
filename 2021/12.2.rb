graph = Hash.new{|h, k| h[k]=[]}
data = readlines.map do |line|
    a, b = line.strip.split '-'
    graph[a]<<b
    graph[b]<<a
end

def is_big_cave(name)
    name.upcase == name
end

total_paths = 0
all_paths = []
graph['start'].each do |node|
    q = [ [[node], false, node] ]
    until q.empty?
        path, small_visited_twice, n = q.shift
        graph[n].each do |b|
            next if b == 'start'
            if b == 'end'
                #puts "start,#{path.join ','},end"
                all_paths << ['start'] + path + ['end']
                total_paths += 1 
                next
            end

            if is_big_cave(b)
                q << [path + [b], small_visited_twice, b]
            else
                c = path.count(b)
                if small_visited_twice
                   if c == 0 
                        q << [path + [b], true, b]
                   end
                elsif c <= 1
                    if c == 1
                        q << [path + [b], true, b]
                    else
                        q << [path + [b], false, b]
                    end
                end
            end
        end
    end
end
#all_paths.sort.each do |path| puts path.join ',' end
puts total_paths