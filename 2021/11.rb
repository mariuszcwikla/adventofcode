map = readlines.map {|line| line.strip.each_char.map {|c| c.to_i}}

def do_game_of_life(map)
    width = map[0].size
    height = map.size

    map.each do |row|
        row.map!{|c| c+1}
    end
    q = []
    flashed = {}
    map.each_with_index do |row, y|
        row.each_with_index do |c, x|
            if c > 9
                q << [x, y]
                flashed[[x,y]]=true
            end
        end
    end
    
    until q.empty?
        x, y = q.shift
        [[-1, -1],
         [ 0, -1],
         [ 1, -1],
         [-1,  0],
         [ 1,  0],
         [-1,  1],
         [ 0,  1],
         [ 1,  1]].each do |dx, dy|
            nx, ny = x+dx, y+dy
            next if nx < 0 || ny < 0 || nx >= width || ny >= height
            next if flashed[[nx, ny]]
            energy = (map[ny][nx] += 1)
            if energy > 9
                q << [nx, ny]
                flashed[[nx, ny]]=true
            end
         end
    end
    map.each do |row|
        row.each_with_index do |c, x|
            row[x]=0 if row[x] > 9
        end
    end
    flashed.size
end

total_flashes = 0
100.times do |step|
    puts "step #{step}"
    map.each do |row| puts row.join '' end
    puts ""
    total_flashes += do_game_of_life(map)
end
puts total_flashes

step = 100
loop do
    step += 1
    do_game_of_life(map)
    if map.flatten.uniq.size == 1
        puts step
        break
    end
end