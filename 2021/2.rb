pos = 0
depth = 0
commands = readlines.map do |line|
    command, n = line.split
    n = n.to_i
    [command, n]
end

commands.each do |command, n|
    case command
    when 'forward' then pos += n
    when 'down' then depth += n
    when 'up' then depth -= n
    end
end

puts depth * pos

depth = 0
pos = 0
aim = 0
commands.each do |command, n|
    case command
    when 'forward' then pos += n; depth += aim * n
    when 'down' then aim += n
    when 'up' then aim -= n
    end
end

puts depth * pos