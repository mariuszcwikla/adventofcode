data = gets.split(',').map(&:to_i)

fishes = {}
fishes.default = 0
data.each do |t|
    fishes[t] += 1
end

def one_day_in_game_of_life(fishes)
    eldest = fishes[0]
    0.upto(7).each do |t|
        fishes[t] = fishes[t+1]
    end
    fishes[6] += eldest
    fishes[8] = eldest
end

1.upto(80).each do
    one_day_in_game_of_life(fishes)
end

puts fishes.values.sum

81.upto(256) do 
    one_day_in_game_of_life(fishes)
end
puts fishes.values.sum