data = gets.split(',').map(&:to_i)

class LanternFish
    def initialize(timer, is_new)
        @timer = timer
        @is_new = is_new
    end

    def countdown(fishes)
        if @is_new == true
            @is_new = false
        else
            @timer -= 1
            if @timer < 0
                @timer = 6
                fishes << LanternFish.new(8, true)
            end
        end
    end
end

fishes = data.map {|t| LanternFish.new(t, false)}

80.times do 
    fishes.each do |f|
        f.countdown(fishes)
    end
end

puts fishes.size

# too slow :( See - 6.2.rb for optimzied solution ;)
80.upto(256).each do |i|
    puts i
    fishes.each do |f|
        f.countdown(fishes)
    end
end

puts fishes.size