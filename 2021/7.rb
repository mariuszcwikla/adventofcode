data = gets.split(',').map(&:to_i)

width = data.max

def compute_fuel(data, loc)
    data.map{|d| (d-loc).abs}.sum
end

min_fuel = nil
min_loc = nil
0.upto(width).each do |x|
    f = compute_fuel(data, x)
    if min_fuel.nil? || f < min_fuel
        min_fuel = f
        min_loc = x
    end
end

puts min_fuel

def compute_fuel2(data, loc)
    data.map do |d| 
        x = (d-loc).abs
        next 0 if x == 0
        (1+x)*x/2    #sum of elements of arithmetic sequence
    end.sum
end
min_fuel = nil
min_loc = nil
0.upto(width).each do |x|
    f = compute_fuel2(data, x)
    if min_fuel.nil? || f < min_fuel
        min_fuel = f
        min_loc = x
    end
end
puts min_fuel