input = gets.split(',').map(&:to_i)

class Table
    attr_reader :winning
    def initialize
        @tables = []
        @markers = {}
        @markers.default = false
        @winning = false
    end

    def add(vals)
        @tables << vals
    end

    def apply_num(num)
        for y in 0...5
            for x in 0...5
                if @tables[y][x]==num
                    @markers[[x,y]] = true
                end
            end
        end
        c = find_winner_column
        r = find_winner_row
        @winning = true if !c.nil? || !r.nil?
    end

    def find_winner_column
        for x in 0...5
            for y in 0..5
                if y == 5
                    return x
                elsif @markers[[x,y]] == false
                    break
                end
            end
        end
        nil
    end

    def find_winner_row
        for y in 0...5
            for x in 0..5
                if x == 5
                    return y
                elsif @markers[[x,y]] == false
                    break
                end
            end
        end
        nil
    end

    def sum_unmarked
        sum = 0
        for y in 0...5
            for x in 0...5
                sum += @tables[y][x] if @markers[[x,y]]==false
            end
        end
        sum
    end
end

tables = []
while gets
    t = Table.new
    5.times do
        t.add(gets.split.map(&:to_i))
    end
    tables << t
end

def apply_num_to_all(tables, num)
    tables.each do |t|
        t.apply_num(num)
    end
    tables.each_with_index do |t, idx|
        i = t.find_winner_column
        i = t.find_winner_row if i.nil?
        unless i.nil?
            sum = t.sum_unmarked
            puts "Found winner #{idx}, sum=#{sum}, answer to the puzzle=#{sum * num}"
            return idx
        end
    end
    nil
end

# Code became shitty after implementing part 2 ;) but I got the answer ;)
input.each do |num|
    winning_table_idx = apply_num_to_all(tables, num)
    if tables.size > 1
        tables.delete_if {|t| t.winning}
    else
        if tables[0].winning
            sum = tables[0].sum_unmarked
            puts "Part 2: Found winner sum=#{sum}, answer to the puzzle=#{sum * num}"
            break
        end
    end
end