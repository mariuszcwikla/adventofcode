data = readlines.map(&:strip)

error_score_map = {
    ')' => 3,
    ']' => 57,
    '}' => 1197,
    '>' => 25137
}

CORRESPONDING_PAREN = {
    ')' => '(',
    ']' => '[',
    '}' => '{',
    '>' => '<'
}

def check_syntax(line)
    s = []
    line.each_char do |c|
        if [')', ']', '}', '>'].include? c
            if s.empty?
                return :corrupted, c, s
            else
                cc = s.pop
                if CORRESPONDING_PAREN[c] != cc
                    return :corrupted, c, s
                end
            end
        else
            s << c
        end
    end
    if s.empty?
        return :ok, nil, s
    else
        return :incomplete, nil, s
    end
end

def compute_part2_score(stack)
    map = {
        '(' => 1,
        '[' => 2,
        '{' => 3,
        '<' => 4
    }
    s = 0
    stack.reverse.each do |c|
        s = s * 5 + map[c]
    end
    s
end

score = 0
part2_scores = []
incomplete_lines = []
data.each do |line|
    result, c, stack = check_syntax(line)
    if result == :corrupted
        score += error_score_map[c]
    elsif result == :incomplete
        incomplete_lines << [line, stack]
        puts stack.join ''
        part2_score = compute_part2_score(stack)
        part2_scores << part2_score
    end
end

puts score

puts part2_scores.sort[part2_scores.size / 2]