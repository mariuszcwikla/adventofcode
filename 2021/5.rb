require 'scanf'

data = readlines.map { |line| line.scanf("%d,%d -> %d,%d") }

width = data.map { |x1, _, x2, _| [x1, x2]}.flatten.max + 1
height = data.map { |_, y1, _, y2|  [y1, y2]}.flatten.max + 1

map = Array.new(height){Array.new(width){0}}


def draw_ortho(map, x1, y1, x2, y2)
    if x1 == x2
        y = [y1, y2].min
        while y <= [y1, y2].max
            map[y][x1] += 1
            y += 1
        end
    elsif y1 == y2
        x = [x1, x2].min
        while x <= [x1, x2].max
            map[y1][x] += 1
            x += 1
        end
    end
end

data.each do |x1, y1, x2, y2|
    draw_ortho(map, x1, y1, x2, y2)
end

# Part 1
puts map.flatten.count {|c| c > 1}

def draw_diagonal(map, x1, y1, x2, y2)
    if x1 > x2
        x1, x2 = x2, x1
        y1, y2 = y2, y1
    end
    dy = if y2 > y1 then 1 else -1 end
    y = y1
    for x in x1..x2
        map[y][x] += 1
        y += dy
    end
end

data.each do |x1, y1, x2, y2|
    if x1 != x2 && y1 != y2     # only diagonals
        draw_diagonal(map, x1, y1, x2, y2)
    end
end

puts map.flatten.count {|c| c > 1}
