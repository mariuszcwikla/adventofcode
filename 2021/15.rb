cave = readlines.map {|line| line.strip.chars.map(&:to_i)}

def compute_lowest_risk(cave)
    risks = {}
    risks.default = 9999999999
    risks[[0, 0]] = 0

    width = cave[0].length
    height = cave.length

    q = [[0, 0, 0]]

    until q.empty?
        x, y = q.shift
        [[ 0, 1],
        [ 0, -1],
        [ -1, 0],
        [ 1, 0]
        ].each do |dx, dy|
            nx = x + dx
            ny = y + dy
            next if nx < 0 || ny < 0 || nx >= width || ny >= height
            new_risk = risks[[x, y]] + cave[ny][nx]
            if new_risk < risks[[nx, ny]]
                risks[[nx, ny]] = new_risk
                q << [nx, ny]
            end
        end
    end

    risks[[width - 1, height - 1]]
end

puts compute_lowest_risk(cave)

width = cave[0].length
height = cave.length

big_map = Array.new(height * 5){Array.new(width * 5){0}}

for y in 0...5
    for x in 0...5
        for i in 0...height
            for j in 0...width
                r = cave[i][j]
                big_map[y * height + i][x * width + j] = (r + x + y - 1) % 9 + 1
            end
        end
    end
end
# Dijkstra would be much faster, but I didn't want to write whole priority queue stuff ;)
# BFS-based algo did also the trick, but in 27 seconds.
puts compute_lowest_risk(big_map)