polymer = gets.strip
gets
rules = {}
while line = gets&.strip
    a, _, b = line.split
    rules[a]=b
end

class Node
    attr_accessor :next
    attr_reader :char
    def initialize(char)
        @char = char
        @next = nil
    end
    def insert_after(c)
        n = @next
        @next = Node.new(c)
        @next.next = n
        @next
    end
    def to_a
        n = self
        a = []
        until n.nil?
            a << n.char
            n = n.next
        end
        a
    end
end

root = tail = Node.new(polymer[0])
polymer[1..-1].each_char do |c|
    tail = tail.insert_after(c)
end

def apply(n, rules)
    until n.next.nil?
        r = n.char + n.next.char
        raise 'unable to find rule for ' + r if rules[r].nil?
        n.insert_after(rules[r])
        n = n.next.next
    end
end

10.times do
    apply(root, rules)
    puts root.to_a.join ''
end

counts = root.to_a.inject(Hash.new(0)) { |h, e| h[e] += 1 ; h }
p counts

a = b = counts.first[1]
counts.each do |k, v|
    a = [a, v].min
    b = [b, v].max
end
puts b - a