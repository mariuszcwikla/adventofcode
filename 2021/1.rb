last = nil
cnt = 0
data = readlines.map(&:to_i)
data.each do |x|
    if last.nil?
        last = x
    else
        cnt +=1 if x > last
        last = x
    end
end
puts cnt

#part 2

cnt = 0
last = data[0] + data[1] + data[2]
1.upto(data.size - 3).each do |i|
    s = data[i] + data[i+1] + data[i+2]
    cnt += 1 if s > last
    last = s
end

puts cnt