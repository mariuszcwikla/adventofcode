data = readlines.map do |line|
    a, b = line.split('|')
    [a.strip.split, b.strip.split]
end

# part 1
cnt = 0
data.each do |_, row|
    cnt += row.select{|s| [2, 3, 4, 7].include?(s.length)}.size
end

puts cnt

=begin

Mapa cyfr
  | 1 | 4 | 7   | 2 | 9
-----------------
0 | 2 | 3 | 3   | 4 | 5
2 | 1 | 2 | 2   | -
3 | 2 | 3 | 3   | 4 | 4
5 | 1 | 3 | 2   | 3 | 5 
6 | 1 | 3 | 2   | 4 | 4
9 | 2 | 4 | 3
=end