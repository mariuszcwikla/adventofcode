data = readlines.map do |line|
    line.strip.each_char.map {|c| c.ord - '0'.ord}
end
width = data[0].size
height = data.size
risk = 0

low_points = []

for y in 0...height
    for x in 0...width
        h = data[y][x]
        next if x > 0 && data[y][x-1] <= h
        next if y > 0 && data[y-1][x] <= h
        next if x < width-1 && data[y][x+1] <= h
        next if y < height-1 && data[y+1][x] <= h
        low_points << [x, y]
        risk += h + 1
    end
end

puts risk

visited = {}
visited.default = false
part2 = []
low_points.each do |x, y|
    q = [[x,y]]
    basin_size = 0
    until q.empty?
        x, y = q.pop
        basin_size += 1
        [
            [-1, 0],
            [ 1, 0],
            [ 0, 1],
            [ 0, -1]
        ].each do |dx, dy|
            next if x + dx < 0
            next if y + dy < 0
            next if x + dx >= width
            next if y + dy >= height
            if !visited[[x+dx,y+dy]] && data[y+dy][x+dx] != 9 && data[y+dy][x+dx] >= data[y][x]
                visited[[x+dx,y+dy]]=true
                q << [x+dx, y+dy]
            end
        end
    end
    part2 << basin_size
end

p part2.sort
puts part2.sort[-3..-1].reduce(&:*)

# 959904 too low?