data = readlines.map(&:strip)       # strip to drop newline chars   

def count_bits(data, pos)
    zeros = 0
    data.each do |line|
        zeros += 1 if line[pos]=='0'
    end
    [zeros, data.size - zeros]
end

num_of_bits = data[0].size

gamma = 0
epsilon = 0
for i in 0...num_of_bits
    zeros, ones = count_bits(data, i)
    gamma <<= 1
    epsilon <<=1
    if ones > zeros
        gamma += 1
    elsif zeros > ones
        epsilon += 1
    end
end
puts gamma * epsilon

oxygen = data.dup
co2 = data.dup

def pick(data, pos, bit)
    data.select {|line| line[pos]==bit}
end

for i in 0...num_of_bits
    zeros, ones = count_bits(oxygen, i)
    if zeros > ones
        oxygen = pick(oxygen, i, '0')
    else
        oxygen = pick(oxygen, i, '1')
    end
    break if oxygen.size == 1
end


for i in 0...num_of_bits
    zeros, ones = count_bits(co2, i)
    if zeros > ones
        co2 = pick(co2, i, '1')
    else
        co2 = pick(co2, i, '0')
    end
    break if co2.size == 1
end

o = oxygen[0].to_i(2)
c = co2[0].to_i(2)
puts c * o