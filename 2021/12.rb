graph = Hash.new{|h, k| h[k]=[]}
data = readlines.map do |line|
    a, b = line.strip.split '-'
    graph[a]<<b
    graph[b]<<a
end

def is_big_cave(name)
    name.upcase == name
end

total_paths = 0

graph['start'].each do |node|
    q = [ [[node], node] ]
    until q.empty?
        path, n = q.shift
        graph[n].each do |b|
            next if b == 'start'
            if b == 'end'
                puts "start,#{path.join ','},end"
                total_paths += 1 
                next
            end

            if is_big_cave(b)
                q << [path + [b], b]
            else
                if !path.include? b
                    q << [path + [b], b]
                end
            end
        end
    end
end
puts total_paths