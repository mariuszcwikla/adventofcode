use std::io;

fn main(){
    for line in io::stdin().lines(){
        let i: i32 = line.unwrap().parse().unwrap();
        println!("{}", i * 2);
    }

    //alt.pattern
    let mut lines = io::stdin().lines();
    while let Some(Ok(line)) = lines.next(){
        let line2 = lines.next().unwrap().unwrap();
        let line3 = lines.next().unwrap().unwrap();
    }
}