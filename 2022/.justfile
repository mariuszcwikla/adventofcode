#set windows-shell := ["cmd.exe", "/c"]

alias b := bench
alias current := day17-2-visualize

default:
  @just --choose

day14:
  cargo run --bin 14 < src/bin/14.in

day15:
  cargo run --bin 15 < src/bin/15.in

day16:
  cargo run --bin 16 < src/bin/16.in

day17:
  cargo run --bin 17 < src/bin/17.in

day17-visualize:
  cargo run --bin 17 -- --render < src/bin/17.in

day17-2:
  cargo run -r --bin 17-2 < src/bin/17.in

day17-2-visualize:
  cargo run --bin 17-2 -- --render < src/bin/17.in


# runs benchmark for given day, e.g. just b 15
bench day:
  cargo build -r --bin {{day}}
  time target/release/{{day}} < src/bin/{{day}}.in

bench-debug day:
  cargo build --bin {{day}}
  time target/debug/{{day}} < src/bin/{{day}}.in