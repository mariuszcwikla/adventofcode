use std::io;

use regex::Regex;

#[derive(Debug, Clone)]
enum Operand{
    Add,
    Mul
}

impl Operand{
    fn calculate(&self, arg1: i128, arg2: i128 )-> i128 {
        match self {
            Operand::Add => arg1 + arg2,
            Operand::Mul => arg1 * arg2
        }
    }

    fn parse(operand: &str) -> Option<Operand>{
        match operand {
            "*" => Some(Operand::Mul),
            "+" => Some(Operand::Add),
            _ => None
        }
    }
}
#[derive(Debug, Clone)]
struct Monkey{
    items: Vec<i128>,
    operand: Operand,
    parsed_arg2: Option<i128>,    //if arg2 is integer, then parsed_arg2 is the integer. Otherwise arg2="old", then parsed_arg2 is None
    divisible_by: i128,
    throw_to_monkey_if_true: usize,
    throw_to_monkey_if_false: usize,
    total_inspections: usize    
}

impl Monkey{
    
    fn calculate_worry(&self, item: i128) -> i128 {
        let old = item;
        if let Some(arg) = self.parsed_arg2{
            self.operand.calculate(old, arg as i128)
        } else {
            self.operand.calculate(old, old)
        }
    }
}

fn monkey_play(monkeys: &mut Vec<Monkey>, monkey_idx: usize, divide_worry_by: i128, modulus: Option<i128>) {
    //gettin tricky to make it right in Rust. 
    let items = monkeys[monkey_idx].items.drain(..).collect::<Vec<_>>();
    monkeys[monkey_idx].total_inspections += items.len();
    // let monkey = &monkeys[monkey_idx];
    for item in items{

        let (new_monkey, new_worry) = {
            let monkey = &monkeys[monkey_idx];
            let mut new_worry = monkey.calculate_worry(item) / divide_worry_by;
            let new_monkey = if new_worry % monkey.divisible_by == 0{
                monkey.throw_to_monkey_if_true
            } else {
                monkey.throw_to_monkey_if_false
            };
            if let Some(modulus) = modulus{
                new_worry = new_worry % modulus;
            }
            (new_monkey, new_worry)
        };
        monkeys[new_monkey].items.push(new_worry);
    }
}



fn main(){
    let mut lines = io::stdin().lines();
    let mut monkeys = Vec::new();

    while let Some(Ok(_)) = lines.next(){                 //line is "Monkey 0"
        let line2 = lines.next().unwrap().unwrap();          //Starting items: 54, 65, 75, 74
        let line3 = lines.next().unwrap().unwrap();          //Operation: new = old + 6
        let line4 = lines.next().unwrap().unwrap();          //Test: divisible by 19
        let line5 = lines.next().unwrap().unwrap();          //  If true: throw to monkey 2
        let line6 = lines.next().unwrap().unwrap();          //  If false: throw to monkey 0
        lines.next();                                        //blank line or EOF
        let (_, items) = line2.split_once(":").unwrap();
        let items: Vec<i128> = items.split(",").map(|item| item.trim().parse().unwrap()).collect();

        let re3 = Regex::new(r"Operation: new = old (\+|\*) (\w*)").unwrap();
        let captures = re3.captures(&line3).unwrap();
        let operand = captures.get(1).unwrap().as_str();
        let arg2 = captures.get(2).unwrap().as_str();       //might be integer or word ("new/old")
        let parsed_arg2 = arg2.parse::<i128>().ok();

        let re4 = Regex::new(r"Test: divisible by (\d*)").unwrap();
        let divisible_by: i128 = re4.captures(&line4).unwrap().get(1).unwrap().as_str().parse().unwrap();

        let re5 = Regex::new(r"If true: throw to monkey (\d*)").unwrap();
        let monkey_if_true: usize = re5.captures(&line5).unwrap().get(1).unwrap().as_str().parse().unwrap();

        let re6 = Regex::new(r"If false: throw to monkey (\d*)").unwrap();
        let monkey_if_false: usize = re6.captures(&line6).unwrap().get(1).unwrap().as_str().parse().unwrap();
        
        let monkey = Monkey{
            operand: Operand::parse(operand).unwrap(),
            parsed_arg2: parsed_arg2,
            divisible_by: divisible_by,
            items: items,
            throw_to_monkey_if_false: monkey_if_false,
            throw_to_monkey_if_true: monkey_if_true,
            total_inspections: 0
        };

        monkeys.push(monkey);
    }

    let mut monkeys_part2 = monkeys.clone();
    for _ in 0..20 {
        for i in 0..monkeys.len(){
            monkey_play(&mut monkeys, i, 3, None);
        }
    }
    let mut total_inspections = monkeys.iter().map(|m| m.total_inspections).collect::<Vec<_>>();
    total_inspections.sort();
    let a = total_inspections[total_inspections.len() - 1];
    let b = total_inspections[total_inspections.len() - 2];
    println!("{}", a * b);

    let mut lcm = 1;        //not a true lcm but works ;)
    for m in &monkeys_part2{
        lcm *= m.divisible_by;
    }
    for _k in 0..10_000{
        for i in 0..monkeys_part2.len(){
            monkey_play(&mut monkeys_part2, i, 1, Some(lcm));
        }
    }
    let mut total_inspections = monkeys_part2.iter().map(|m| m.total_inspections).collect::<Vec<_>>();
    total_inspections.sort();
    let a = total_inspections[total_inspections.len() - 1];
    let b = total_inspections[total_inspections.len() - 2];
    println!("{}", a * b);
}