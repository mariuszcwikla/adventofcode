//Sensor at x=20, y=1: closest beacon is at x=15, y=3

use std::{io, collections::HashMap};

use regex::Regex;

#[derive(PartialEq, Hash, Eq, Debug)]
struct Point{
    x: i32,
    y: i32
}

impl Point{
    fn manhattan(&self, other: &Point) -> i32{
        (self.x - other.x).abs() + (self.y - other.y).abs()
    }
}

type Beacon = Point;
type Sensor = Point;

#[derive(PartialEq, Debug)]
struct Range{
    start: i32,
    end: i32
}

impl Range{
    fn is_after(&self, other: &Range) -> bool{
        self.start >= other.start
    }

    fn can_merge(&self, other: &Range) -> bool {
        if other.end +1 < self.start{
            return false;
        }
        if other.start -1 > self.end{
            return false;
        }
        true
    }
    fn merge(&self, other: &Range) -> Range{
        Range{
            start: self.start.min(other.start),
            end: self.end.max(other.end)
        }
    }

    fn new(start: i32, end: i32) -> Range {
        Range{start: start, end: end}
    }

    fn range_at_scan_line(s: &Point, b: &Point, scan_y: i32) -> Option<Range> {
        let radius = s.manhattan(b);
        if scan_y > s.y + radius || scan_y < s.y - radius{
            return None;
        } else {
            let scan_dist_y = (s.y - scan_y).abs();
            let width = radius - scan_dist_y;
            Some(Range::new(s.x - width, s.x + width))
        }
    }
}

struct RangeList{
    ranges: Vec<Range>
}

impl RangeList{
    fn new() -> Self{
        RangeList{ranges: Vec::new()}
    }

    fn add(&mut self, range: Range){
        let mut i = 0;
        while i < self.ranges.len() {
            if self.ranges[i].can_merge(&range){
                self.ranges[i] = self.ranges[i].merge(&range);
                while i < self.ranges.len() - 1 &&
                      self.ranges[i].can_merge(&self.ranges[i+1]){
                    self.ranges[i] = self.ranges[i].merge(&self.ranges[i+1]);
                    self.ranges.remove(i+1);
                }
                return;
            }
            if range.is_after(&self.ranges[i]){
                i+=1;
            }
            else {
                self.ranges.insert(i, range);
                return;
            }
        }
        self.ranges.push(range);
    }
}

#[cfg(test)]
mod tests{
    use crate::*;

    #[test]
    fn test_range_exclusive(){
        let mut r = RangeList::new();
        r.add(Range::new(2, 5));
        assert_eq!(1, r.ranges.len());

        r.add(Range::new(8, 10));
        assert_eq!(2, r.ranges.len());
        assert_eq!(
            vec![
                Range::new(2, 5),
                Range::new(8, 10),
            ],
            r.ranges
        );

        r.add(Range::new(-1, 0));
        assert_eq!(3, r.ranges.len());
        assert_eq!(Range::new(-1, 0), r.ranges[0]);

        assert_eq!(
            vec![
                Range::new(-1, 0),
                Range::new(2, 5),
                Range::new(8, 10),
            ],
            r.ranges
        );
    }

    #[test]
    fn test_range_merge(){
        let mut r = RangeList::new();
        r.add(Range::new(2, 5));
        r.add(Range::new(8, 10));
        r.add(Range::new(-1, 0));
        r.add(Range::new(3, 7));

        assert_eq!(
            vec![
                Range::new(-1, 0),
                Range::new(2, 10)
            ],
            r.ranges
        );
    }

    #[test]
    fn test_range_add1(){
        let mut r = RangeList::new();
        r.add(Range::new(2, 3));
        r.add(Range::new(5, 6));

        assert_eq!(
            vec![
                Range::new(2, 3),
                Range::new(5, 6)
            ],
            r.ranges
        );
    }

    #[test]
    fn test_range_add2(){
        let mut r = RangeList::new();
        r.add(Range::new(5, 6));
        r.add(Range::new(2, 3));
        

        assert_eq!(
            vec![
                Range::new(2, 3),
                Range::new(5, 6)
            ],
            r.ranges
        );
    }

    #[test]
    fn test_range_add3(){
        let mut r = RangeList::new();
        r.add(Range::new(2, 3));
        r.add(Range::new(4, 5));
        

        assert_eq!(
            vec![
                Range::new(2, 5)
            ],
            r.ranges
        );
    }

    #[test]
    fn test_range_at_scan_line(){
        let r = Range::range_at_scan_line(
            &Point{x: 5, y: 5},
            &Point{x: 5, y: 6},
            5);
        assert_eq!(Some(Range::new(4,6)), r);
    }
    #[test]
    fn test_range_at_scan_line2(){
        let r = Range::range_at_scan_line(
            &Point{x: 5, y: 5},
            &Point{x: 5, y: 6},
            4);
        assert_eq!(Some(Range::new(5,5)), r);
    }
    #[test]
    fn test_range_at_scan_line3(){
        let r = Range::range_at_scan_line(
            &Point{x: 5, y: 5},
            &Point{x: 5, y: 6},
            3);
        assert_eq!(None, r);
    }
    #[test]
    fn test_range_at_scan_line4(){
        let r = Range::range_at_scan_line(
            &Point{x: 5, y: 5},
            &Point{x: 5, y: 6},
            8);
        assert_eq!(None, r);
    }
    #[test]
    fn test_range_at_scan_line5(){
        let r = Range::range_at_scan_line(
            &Point{x: 5, y: 5},
            &Point{x: 6, y: 6},
            5);
        assert_eq!(Some(Range::new(3,7)), r);
    }
}

fn main(){
    let mut sensors: HashMap<Sensor, Beacon> = HashMap::new();

    for line in io::stdin().lines(){
        let line = line.unwrap();

        let re3 = Regex::new(r"Sensor at x=(-?\d+), y=(-?\d+): closest beacon is at x=(-?\d+), y=(-?\d+)").unwrap();
        let captures = re3.captures(&line).unwrap();
        let x1: i32 = captures.get(1).unwrap().as_str().parse().unwrap();
        let y1: i32 = captures.get(2).unwrap().as_str().parse().unwrap();
        let x2: i32 = captures.get(3).unwrap().as_str().parse().unwrap();
        let y2: i32 = captures.get(4).unwrap().as_str().parse().unwrap();
        
        sensors.insert(Sensor{x: x1, y: y1}, Beacon{x: x2, y: y2});
    }

    let min_x = sensors.iter().map(|(s, b)| s.x - b.manhattan(s)).min().unwrap();
    let max_x = sensors.iter().map(|(s, b)| s.x + b.manhattan(s)).max().unwrap();

    let mut part1 = 0;
    // const LINE_Y: i32 = 10;
    const LINE_Y: i32 = 2000000;
    for x in min_x..=max_x{
        let possible_beacon_loc = Point{x: x, y: LINE_Y};
        for (s, b) in sensors.iter(){
            if s.manhattan(&possible_beacon_loc) > s.manhattan(b){
                continue;
            }
            if possible_beacon_loc == *b {
                continue;
            }
            part1 += 1;
            break;
        }
    }
    println!("{}", part1);

    // const SEARCH_SPACE: i32 = 20;
    const SEARCH_SPACE: i32 = 4000000;
    for y in 0..SEARCH_SPACE{
        let mut ranges = RangeList::new();
        for (s, b) in sensors.iter(){
            if let Some(r) = Range::range_at_scan_line(s, b, y){
                ranges.add(r);
                
            }
        }
        if ranges.ranges.len() > 1{
            let x = ranges.ranges[0].end + 1;
            let ans = (x as i64) * 4000000i64 + (y as i64);
            println!("{} {}, part2={}", x, y, ans);
            break;
        }
    }
    // This is very slow
    // for x in 0..SEARCH_SPACE{
    //     let ranges = RangeList::new();
    //     for y in 0..SEARCH_SPACE{
    //         let p = Point{x, y};
    //         let mut found = true;
    //         for (s, b) in sensors.iter(){
    //             if s.manhattan(&p) <= s.manhattan(b){
    //                 found = false;
    //                 break;
    //             }
    //         }
    //         if found{
    //             let part2 = x * 4000000 + y;
    //             println!("{} {}, part2={}", x, y, part2);
    //             break;
    //         }
    //     }
    // }
}