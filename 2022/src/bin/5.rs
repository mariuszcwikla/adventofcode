use std::io;

fn main(){
    let mut crates: Vec<Vec<char>> = Vec::new();

    let lines = io::stdin().lines().map(|line| line.unwrap()).collect::<Vec<_>>();

    let n = (lines[0].len() + 1)/4;
    for _ in 0..n{
        crates.push(Vec::new());
    }

    let mut i = 0;
    loop {
        let line = &lines[i];
        if line.chars().nth(1).unwrap() == '1'{
            i+=2;
            break;
        }
        let mut x = 1;
        let mut j = 0;
        while x < line.len(){
            let c = line.chars().nth(x).unwrap();
            if c != ' '{
                crates[j].push(c);
            }
            x += 4;
            j += 1;
        }
        i+=1;
    }

    {
        //borrow checker, you bitch
        for v in crates.iter_mut(){
            v.reverse();
        }
    }


    let mut part2 = crates.clone();

    while i < lines.len(){
        let line = &lines[i];
        let v = line.split(" ").collect::<Vec<_>>();
        let a = v[1].parse::<usize>().unwrap();
        let b = v[3].parse::<usize>().unwrap();
        let c = v[5].parse::<usize>().unwrap();

        move_crates(&mut crates, a, b - 1, c - 1);
        move_crates_part2(&mut part2, a, b - 1, c - 1);
        i+=1;
    }

    let mut s = String::from("");
    let mut s2 = String::from("");
    for c in crates{
        s.push(*c.last().unwrap());
    }
    for c in part2{
        s2.push(*c.last().unwrap());
    }
    println!("{}", s);
    println!("{}", s2);
}

// Following code does not compile. Borrow Checker: you drive me crazy sometimes ;)
// error[E0499]: cannot borrow `*crates` as mutable more than once at a time
// fn move_crates(crates: &mut Vec<Vec<char>>, quantity: usize, from: usize, to: usize) -> () {
//     let from = &mut crates[from];
//     let to = &mut crates[to];

//     for _ in 0..quantity{
//         let c = from.pop();
//         to.push(c.unwrap());
//     }
// }

// This one compiles fine ;)
// One alternative I have tried was split_mut but it complicates the code much more
fn move_crates(crates: &mut Vec<Vec<char>>, quantity: usize, from: usize, to: usize) -> () {
    for _ in 0..quantity{
        let c = crates[from].pop().unwrap();
        crates[to].push(c);
    }
}

fn move_crates_part2(crates: &mut Vec<Vec<char>>, quantity: usize, from: usize, to: usize) -> () {
    let len = crates[from].len();

    //Borrow checker, you bitch...
    // let slice = &crates[from][(len-quantity)..];
    // crates[to].clone_from_slice(slice);

    let b = crates[from].split_off(len - quantity);
    crates[to].extend(b);
}
