use std::io;
use sscanf::sscanf;

fn overlaps_fully((a, b): (i32, i32), (c, d): (i32, i32)) -> bool {
    if c>=a && d <=b {
        return true;
    }
    if a>=c && b <= d{
        return true;
    }
    false
}

fn non_overlapping((a, b): (i32, i32), (c, d): (i32, i32)) -> bool {
    if b < c || a > d {
       return true
    }
    false
}

fn overlaps_partially((a, b): (i32, i32), (c, d): (i32, i32)) -> bool {
    return !non_overlapping((a, b), (c, d));
}

fn main(){
    let mut num_of_overlaps = 0;
    let mut pairs = Vec::new();
    let mut part2 = 0;
    for line in io::stdin().lines(){
        let s = line.unwrap();
        let parsed = sscanf!(s, "{}-{},{}-{}", i32, i32, i32, i32).unwrap();

        if overlaps_fully((parsed.0, parsed.1), (parsed.2, parsed.3)) {
            num_of_overlaps += 1;
        }

        if overlaps_partially ((parsed.0, parsed.1), (parsed.2, parsed.3)) {
            part2 += 1;
        }
        pairs.push(parsed);
    }
    println!("{}", num_of_overlaps);

    println!("{}", part2);
}