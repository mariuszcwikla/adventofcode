use std::{io, collections::HashMap, rc::{Rc}, cell::RefCell};

#[allow(dead_code)]
struct File{
    name: String,
    size: usize
}

type DirectoryPtr = Rc<RefCell<Directory>>;

#[allow(dead_code)]
struct Directory{
    name: String,
    dirs: HashMap<String, DirectoryPtr>,
    files: HashMap<String, File>,
    parent: Option<DirectoryPtr>
}



impl Directory{

    fn new(name: &str) -> Directory{
        Directory{ name: name.to_string(), dirs: HashMap::new(), files: HashMap::new(), parent: None}
    }

    fn mkdir(parent: &Rc<RefCell<Directory>>, name: &str) -> DirectoryPtr {
        if parent.as_ref().borrow().dirs.contains_key(name){
            // println!("dir {} already created", name);
            return parent.as_ref().borrow().dirs.get(name).unwrap().clone();
        }

        let dir = Rc::new(RefCell::new(Directory{
            name: name.to_string(),
            dirs: HashMap::new(),
            files: HashMap::new(),
            parent: Some(parent.clone()),
        }));
        parent.as_ref().borrow_mut().dirs.insert(name.to_string(), dir.clone());
        dir
    }

    fn create_file(&mut self, name: &str, size: usize) -> () {
        self.files.insert(name.to_string(), File{name: name.to_string(), size: size});
    }

    #[cfg(test)]
    fn to_string_recusive(&self, s: &mut String, level: usize){
        let prefix = "  ".repeat(level);
        for (_, file) in self.files.iter(){
            s.push_str(&format!("{}{}    size={}\n", prefix, file.name, file.size));
        }
        for (_, dir) in self.dirs.iter(){
            s.push_str(&prefix);
            s.push_str(&dir.borrow().name);
            s.push_str("\n");
            dir.as_ref().borrow().to_string_recusive(s, level+1);
        }
    }

    #[cfg(test)]
    fn to_string(&self) -> String{
        let mut s = String::from("/\n");

        self.to_string_recusive(&mut s, 0);

        s
    }

    fn go_up(&self) -> DirectoryPtr {
        match &self.parent{
            Some(d) => {
                d.clone()
            },
            None => panic!("lready on the root")
        }
    }

    fn cd(current: DirectoryPtr, dir: &str) -> DirectoryPtr {
        match current.as_ref().borrow().dirs.get(dir){
            Some(dir) => dir.clone(),
            None => {
                Directory::mkdir(&current, dir)
            }
        }
    }
}

fn calculate_directory_size(dir: &DirectoryPtr, sizes: &mut Vec<(String, usize)>) -> usize{

    let mut size = 0;
    for (_, file) in dir.as_ref().borrow().files.iter(){
        size += file.size;
    }

    for (_, dir) in dir.as_ref().borrow().dirs.iter(){
        let dirsize = calculate_directory_size(&dir, sizes);
        size += dirsize;
    }
    //TODO: I should have calculate fullpath instead..
    sizes.push((dir.as_ref().borrow().name.clone(), size)); //TODO: I am cloning here. Would it be possible to use just ref instead?
    size
}

#[cfg(test)]
mod tests{

    use self::super::*;
    #[test]
    fn test_create_dir(){
        let _b = Box::new(5);
        let filesystem = Rc::new(RefCell::new(Directory::new("/")));
        let dir = Directory::mkdir(&filesystem, "foo");
        assert!(filesystem.as_ref().borrow().dirs.contains_key("foo"));

        let dir2 = Directory::mkdir(&dir, "bar");

        // let dir2 = dir.borrow_mut().mkdir("bar");
        assert!(dir.as_ref().borrow().dirs.contains_key("bar"));

        dir2.as_ref().borrow_mut().create_file("file1.txt", 55);
        assert!(dir2.as_ref().borrow().files.contains_key("file1.txt"));

        println!("{}", filesystem.as_ref().borrow().to_string());
    }

    #[test]
    fn test_cd_known(){
        let filesystem = Rc::new(RefCell::new(Directory::new("/")));
        Directory::mkdir(&filesystem, "foo");

        let dir = Directory::cd(filesystem, "foo");
        assert_eq!("foo", dir.as_ref().borrow().name);
    }

    #[test]
    fn test_cd_up(){
        let filesystem = Rc::new(RefCell::new(Directory::new("/")));
        Directory::mkdir(&filesystem, "foo");

        let dir = Directory::cd(filesystem, "foo");
        let root = dir.as_ref().borrow().go_up();
        assert_eq!("/", root.as_ref().borrow().name);
    }
}

fn main(){
    let filesystem = Rc::new(RefCell::new(Directory::new("/")));
    let mut current = filesystem.clone();
    // let mut lines = io::stdin().lines().peekable();
    // while let Some(line) = lines.next(){
    for line in io::stdin().lines(){
        let line = line.unwrap();
        let split = line.split_whitespace().collect::<Vec<_>>();
        match split[..] {
            ["$", "cd", "/"] => current = filesystem.clone(),
            ["$", "cd", ".."] => {
                let new_current = current.as_ref().borrow_mut().go_up();
                current = new_current;
            },
            ["$", "cd", dir_name] => {
                let new_current = Directory::cd(current, dir_name);
                current = new_current;
            },
            ["$", "ls"] => (),  //do nothing. Current directory is already set up. In next iteration we'll create dirs/files
            ["dir", name] => {
                Directory::mkdir(&current, name);
            },
            [filesize, filename] => {
                let filesize: usize = filesize.parse().unwrap();
                current.as_ref().borrow_mut().create_file(filename, filesize);
            },
            _ => panic!("Unsupported operation {}", line)  
        } 
    }

    let mut sizes = Vec::new();
    calculate_directory_size(&filesystem, &mut sizes);

    let mut part1 = 0;

    for (_, size) in sizes.iter(){
        if *size <= 100000{
            part1 += size;
        }
    }
    println!("{}", part1);

    //part2
    sizes.sort_by(|a, b| a.1.partial_cmp(&b.1).unwrap());
    let total_occupied = sizes.last().unwrap().1;
    let disk_size = 70000000;
    let free = disk_size - total_occupied;
    for (_, size) in sizes.iter(){
        if size + free > 30000000{
            println!("{}", size);
            break;
        }
    }
}