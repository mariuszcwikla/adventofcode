use std::{io, collections::{VecDeque, HashSet}};

#[derive(Eq, Hash, PartialEq, Clone, Debug)]
struct Point{
    x: i32,
    y: i32
}

fn main(){
    let mut map:Vec<Vec<u32>> = Vec::new();
    let mut start = Point{x: 0, y: 0};
    let mut end = Point{x: 0, y: 0};

    let mut y = 0;
    for line in io::stdin().lines(){
        let line = line.unwrap();
        let mut row = Vec::new();
        let chars = line.chars();
        for (x, c) in chars.enumerate(){
            let c = match c {
                'S' => {
                    start.x = x as i32;
                    start.y = y;
                    'a'
                },
                'E' => {
                    end.x = x as i32;
                    end.y = y;
                    'z'
                }
                _ => c
            };
            row.push(c as u32 - 'a' as u32);
        }
        map.push(row);
        y+=1;
    }

    let width = map[0].len() as i32;
    let height = map.len() as i32;

    let mut visited = HashSet::new();
    let mut q: VecDeque<(Point, i32)> = VecDeque::new();
    q.push_back((start.clone(), 0));
    visited.insert(start.clone());
    'outer:
    while !q.is_empty(){
        let (p, d) = q.pop_front().unwrap();
        for (dx, dy) in vec![(-1, 0), (1, 0), (0, -1), (0, 1)]{
            let pp = Point{x: p.x + dx , y: p.y + dy};
            if pp.x < 0 || pp.y < 0 || pp.x >= width || pp.y >= height{
                continue;
            }

            let h1 = map[p.y as usize][p.x as usize];
            let h2 = map[pp.y as usize][pp.x as usize];
            if h2 > h1 + 1{
                continue;
            }

            if pp == end{
                println!("{}", d + 1);
                break 'outer;
            }
            if !visited.contains(&pp){
                visited.insert(pp.clone());
                q.push_back((pp, d+1));
            }
        }
    }

    //part 2

    let mut visited = HashSet::new();
    let mut q: VecDeque<(Point, i32)> = VecDeque::new();
    q.push_back((end.clone(), 0));
    visited.insert(end.clone());
    'outer:
    while !q.is_empty(){
        let (p, d) = q.pop_front().unwrap();
        for (dx, dy) in vec![(-1, 0), (1, 0), (0, -1), (0, 1)]{
            let pp = Point{x: p.x + dx , y: p.y + dy};
            if pp.x < 0 || pp.y < 0 || pp.x >= width || pp.y >= height{
                continue;
            }

            let h1 = map[p.y as usize][p.x as usize];
            let h2 = map[pp.y as usize][pp.x as usize];
            if h1 > h2 + 1{
                continue;
            }

            if h2 == 0 {
                println!("{}", d + 1);
                break 'outer;
            }
            if !visited.contains(&pp){
                visited.insert(pp.clone());
                q.push_back((pp, d+1));
            }
        }
    }
}