use std::{io};

struct Circuit{
    cycle_num: i32,
    x: i32,
    total_signal_strength: i32
}

impl Circuit{
    fn new() -> Self{
        Circuit{cycle_num: 0, x: 1, total_signal_strength: 0}
    }

    fn noop(&mut self){
        self.incr_cycle();
    }

    fn incr_cycle(&mut self){
        
        let pixel_idx = self.cycle_num % 40;
        if pixel_idx == self.x || pixel_idx == self.x - 1 || pixel_idx == self.x + 1 {
            print!("#");
        } else {
            print!(".");
        }

        self.cycle_num += 1;
        if (self.cycle_num - 20) % 40 == 0{
            let signal_strength = self.cycle_num * self.x;
            self.total_signal_strength += signal_strength;

        }
        if self.cycle_num % 40 == 0 {
            println!();
        }
    }

    fn addx(&mut self, x: i32){
        self.incr_cycle();
        self.incr_cycle();
        self.x += x;
    }
}

fn main(){
    let mut circuit = Circuit::new();
    for line in io::stdin().lines(){
        let line = line.unwrap();
        if line == "noop"{
            circuit.noop();
        } else {
            let (_, b) = line.split_once(" ").unwrap();
            let b: i32 = b.parse().unwrap();
            circuit.addx(b);
        }
    }
    println!();
    println!("{}", circuit.total_signal_strength);

    //answer for part 2 in my case its:
    /*
    ###..####.#..#.###..###..#....#..#.###..
    #..#.#....#..#.#..#.#..#.#....#..#.#..#.
    #..#.###..####.#..#.#..#.#....#..#.###..
    ###..#....#..#.###..###..#....#..#.#..#.
    #.#..#....#..#.#....#.#..#....#..#.#..#.
    #..#.####.#..#.#....#..#.####..##..###..

    
    */
}