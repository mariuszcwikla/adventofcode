use std::{io, collections::HashSet, thread, time::Duration};

struct Rope{
    hx: i32,
    hy: i32,
    tx: i32,
    ty: i32
}

impl Rope{
    fn new() -> Self{
        Self{hx: 0, hy: 0, tx: 0, ty: 0}
    }

    fn move_rope(&mut self, direction: &str, len: usize, visited: &mut HashSet<(i32, i32)>) {
        for _ in 0..len{
            let hx = self.hx;
            let hy = self.hy;
            match direction {
                "U" => self.hy += 1,
                "D" => self.hy -= 1,
                "L" => self.hx -= 1,
                "R" => self.hx += 1,
                _ => panic!()
            }
            if (self.hx - self.tx).abs() == 2 || (self.hy - self.ty).abs() == 2{
                self.tx = hx;
                self.ty = hy;
            }
            visited.insert((self.tx, self.ty));
        }
    }
}

//I don't want to break old solution. So made a copy of part1 and adapted to part 2
#[derive(Clone)]
struct Point{
    x: i32,
    y: i32
}
struct Rope10{
    rope: Vec<Point>
}

impl Rope10 {
    fn new() -> Self{
        let mut rope = Vec::new();
        for _ in 0..10{
            rope.push(Point{x: 0, y: 0});
        }
        Self{rope: rope}
    }

    fn head<'a> (&'a mut self) -> &'a mut Point{
        &mut self.rope[0]
    }
    fn move_rope(&mut self, direction: &str, len: usize, visited: &mut HashSet<(i32, i32)>) {
        println!("{direction} {len}");
        for _ in 0..len{
            let mut prev = self.head().clone();
            match direction {
                "U" => {
                    self.head().y += 1;
                },
                "D" => {
                    self.head().y -= 1;
                },
                "L" => {
                    self.head().x -= 1;
                },
                "R" => {
                    self.head().x += 1;
                },
                _ => {
                    panic!();
                }
            }
            for i in 1..self.rope.len(){
                let cur = self.rope[i].clone();
                // if (self.rope[i-1].x - cur.x).abs() == 2 || (self.rope[i-1].y - cur.y).abs() == 2{
                    // self.rope[i] = prev;
                // }
                if (self.rope[i-1].x - cur.x).abs() == 2{
                    self.rope[i].x = prev.x;
                } else if (self.rope[i-1].y - cur.y).abs() == 2{
                    self.rope[i].y = prev.y;
                }
                prev = cur;
            }
            let tail = self.rope.last().unwrap();
            visited.insert((tail.x, tail.y));
            println!("{} {}", tail.x, tail.y);
            self.print();
            
        }
        println!("");
    }

    fn print(&self ){
        // return;
        const SIZE: i32 = 40;
        let mut map = vec![vec!['.'; SIZE as usize]; SIZE as usize];
        for (i, p) in self.rope.iter().enumerate(){
            if p.x.abs() < SIZE/2 && p.y.abs() < SIZE/2{
                let x = (p.x + SIZE/2) as usize;
                let y = (p.y + SIZE/2) as usize;
                map[y][x]=char::from_digit(i as u32, (SIZE/2) as u32).unwrap();
            }
        }

        let mut buffer = String::from("");
        for _ in 0..SIZE/2{
            // println!();
            buffer += "\n";
        }

        let b =map.iter().rev().map(|v| v.iter().collect::<String>()).collect::<Vec<_>>().join("\n");
        buffer.push_str(&b);
        for _v in map.iter().rev(){
            // println!("{}", v.iter().collect::<String>());
        }
        println!("{}", buffer);
        thread::sleep(Duration::from_millis(200));
    }
}

fn main(){
    let mut rope = Rope::new();
    let mut rope2 = Rope10::new();
    let mut visited = HashSet::new(); 
    let mut visited2 = HashSet::new(); 
    visited.insert((0, 0));
    visited2.insert((0, 0));

    for line in io::stdin().lines(){
        let line = line.unwrap();
        let mut split = line.split_whitespace();
        let direction = split.next().unwrap();
        
        let len: usize = split.next().unwrap().parse().unwrap();
        
        rope.move_rope(direction, len, &mut visited);
        rope2.move_rope(direction, len, &mut visited2);
        thread::sleep(Duration::from_millis(5000));
    }
    println!("{}", visited.len());
    println!("{}", visited2.len());
    println!("{:?}", visited);
}