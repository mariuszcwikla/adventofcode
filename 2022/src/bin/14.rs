use std::{io::{self, Error}, collections::HashMap};
#[cfg(feature="simulation")]
use std::{thread, time::Duration};

use console::Term;

// To run this:
// Standard run:
//
// cargo run --bin 14 < src/bin/14.in
//
// Simulation mode (animation)
//
// cargo run --features=simulation --bin 14 < src/bin/14.in

#[derive(Eq, PartialEq, Hash, Clone)]
struct Point{
    x: i32,
    y: i32
}

fn main(){
    let mut map: HashMap<Point, char> = HashMap::new();

    for line in io::stdin().lines(){
        let mut it = line.as_deref().unwrap().split(" ");

        let mut path: Vec<(i32, i32)> = Vec::new();
        while let Some(word) = it.next(){
            let v: Vec<&str> = word.split(",").collect();
            let a: i32 = v[0].parse().unwrap();
            let b: i32 = v[1].parse().unwrap();
            it.next();      //to parse the "->" element
            path.push((a, b));
        }

        for i in 1..path.len(){
            draw_wall(&path[i-1], &path[i], &mut map);
        }
    }

    let Point{x: mut min_x, y: mut max_y} = map.iter().next().unwrap().0;
    let mut min_y = max_y;
    for (p, _) in map.iter(){
        max_y = max_y.max(p.y);
        min_y = min_y.min(p.y);
        min_x = min_x.min(p.x);
    }

    min_x -= 10;     //for display purposes

    let console = Term::buffered_stdout();

    display(&console, &map, min_x, None);

    let mut steps = 0;
    while let Ok(true)=simulate(&mut map, min_x, max_y, &console){
        steps += 1;

        display(&console, &map, min_x, None);

    }

    display(&console, &map, min_x, None);
    println!("{}", steps);

    while let Ok(true)=simulate_part2(&mut map, min_x, max_y, &console){
        steps += 1;

        display(&console, &map, min_x, None);
    }
    println!("{}", steps);
}

fn draw_wall(from: &(i32, i32), to: &(i32, i32), map: &mut HashMap<Point, char>) -> () {
    if from.0 == to.0{
        let x = from.0;
        let y1 = from.1.min(to.1);
        let y2 = from.1.max(to.1);
        for y in y1..=y2{
            map.insert(Point{x, y}, '#');
        }
    } else {
        let y = from.1;
        let x1 = from.0.min(to.0);
        let x2 = from.0.max(to.0);
        for x in x1..=x2{
            map.insert(Point{x, y}, '#');
        }
    }
}


fn simulate(map: &mut HashMap<Point, char>, min_x: i32, max_y: i32, console: &Term) -> Result<bool, Error>
{
    
    let mut p = Point{x: 500, y:0};

    while p.y <= max_y{
        if let None = map.get(&Point{x: p.x, y: p.y + 1}){
            p.y += 1;
        }
        else if let None = map.get(&Point{x: p.x - 1, y: p.y + 1}){
            p.y += 1;
            p.x -= 1;
        } else if let None = map.get(&Point{x: p.x + 1, y: p.y + 1}){
            p.y += 1;
            p.x += 1;
        }
        else {
            map.insert(p, 'o');
            return Ok(true);
        }
        display(&console, &map, min_x, Some(&p));
    }

    Ok(false)
}

fn simulate_part2(map: &mut HashMap<Point, char>, min_x: i32, max_y: i32, console: &Term) -> Result<bool, Error>
{
    
    let mut p = Point{x: 500, y:0};
    if let Some('o') = map.get(&p){
        return Ok(false);
    }

    while p.y < max_y + 1{
        if let None = map.get(&Point{x: p.x, y: p.y + 1}){
            p.y += 1;
        }
        else if let None = map.get(&Point{x: p.x - 1, y: p.y + 1}){
            p.y += 1;
            p.x -= 1;
        } else if let None = map.get(&Point{x: p.x + 1, y: p.y + 1}){
            p.y += 1;
            p.x += 1;
        }
        else {
            map.insert(p, 'o');
            return Ok(true);
        }
        display(&console, &map, min_x, Some(&p));
    }
    map.insert(p, 'o');
    Ok(true)
}

#[cfg(not(feature="simulation"))]
fn display(_console: &Term, _map: &HashMap<Point, char>, _min_x: i32, _sand_grain_location: Option<&Point>) -> () {

}

#[cfg(feature="simulation")]
fn display(console: &Term, map: &HashMap<Point, char>, min_x: i32, sand_grain_location: Option<&Point>) -> () {
    console.clear_screen().unwrap();
    const WIDTH: usize = 60;
    const HEIGHT: usize = 40;
    let mut buf: Vec<char> = vec![' '; WIDTH];
    for y in 0..HEIGHT{
        for i in 0usize..WIDTH{
            if let Some(c) = map.get(&Point{x: min_x + i as i32, y: y as i32}){
                buf[i] = *c;
            } else {
                buf[i] = '.';
            }

            if let Some(sand_loc) = sand_grain_location{
                if sand_loc.y == y as i32 && sand_loc.x == i as i32 + min_x{
                    buf[i] = 'o';
                }
            }
        }
        let s = buf.iter().collect::<String>();
        // println!("{}", s);
        console.write_line(&s).unwrap();
    }
    console.flush().unwrap();
    // thread::sleep(Duration::from_millis(2));
}
