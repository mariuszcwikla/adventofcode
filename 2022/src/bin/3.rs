use std::{io, collections::{HashMap, BTreeSet}};

fn main(){
    
    let mut part1 = 0;
    let lines = io::stdin().lines().map(|r|r.unwrap()).collect::<Vec<_>>();
    for line in &lines {
        let a = &line[0..(line.len()/2)];
        let b = &line[(line.len()/2)..];

        let mut items_a: HashMap<char, i32> = HashMap::new();
        for c in a.chars(){
            if let Some(n) = items_a.get(&c){
                items_a.insert(c, n+1);
            } else {
                items_a.insert(c, 1);
            }
        }

        for c in b.chars(){
            if let Some(_) = items_a.get(&c){
                let i = rank(c);
                
                part1 += i;
                // println!("{} {}", c, i);
                break
            }
        }
    }

    println!("{}", part1);

    let mut it = lines.iter();
    
    let mut part2 = 0;
    while let Some(first) = it.next(){
        let second = it.next().unwrap();
        let third = it.next().unwrap();

        let first = first.chars().collect::<BTreeSet<_>>();
        let second = second.chars().collect::<BTreeSet<_>>();
        let third = third.chars().collect::<BTreeSet<_>>();

        let result = first.intersection(&second).map(|c| *c).collect::<BTreeSet<_>>();
        let result = result.intersection(&third).map(|c| *c).collect::<Vec<_>>();
        //println!("{:?}", result);

        let result = rank(result[0]);
        part2 += result;
    }

    println!("{}", part2)
}

fn rank(c: char) -> u32 {
    let i = if c>='a' && c<='z'{
        c as u32 - 'a' as u32 + 1
    } else {
        c as u32 - 'A' as u32 + 27
    };
    i
}
