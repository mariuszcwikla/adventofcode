use std::{io, cmp::Ordering};

#[derive(Debug)]
enum Element{
    Value(i32),
    List(Vec<Element>)
}

impl PartialOrd for Element{
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (Self::Value(v1), Self::Value(v2)) => v1.partial_cmp(v2),
            (Self::List(list1), Self::List(list2)) => list1.partial_cmp(list2),
            (Self::Value(v1), Self::List(list2)) => {
                let list1 = vec![Element::Value(*v1)];
                list1.partial_cmp(list2)
            },
            (Self::List(list1), Self::Value(v2)) => {
                let list2 = vec![Element::Value(*v2)];
                list1.partial_cmp(&list2)
            }
        }
    }
}

impl PartialEq for Element{
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Value(v1), Self::Value(v2)) => v1 == v2,
            (Self::List(list1), Self::List(list2)) => list1 == list2,
            (Self::Value(v1), Self::List(list2)) => {
                let list1 = vec![Element::Value(*v1)];
                list1 == *list2
            },
            (Self::List(list1), Self::Value(v2)) => {
                let list2 = vec![Element::Value(*v2)];
                *list1 == list2
            }
        }
    }
}


fn parse_packet(line: &str) -> Element {
    let mut stack: Vec<Element> = Vec::new();
    let mut it = line.chars().peekable();
    loop {
        let c = it.next().unwrap();
        match c {
            '[' => {
                let list = Element::List(Vec::new());
                stack.push(list);
            }
            ']' => {
                let last = stack.pop().unwrap();
                match stack.last_mut(){
                    None => {
                        //stack is empty. We have finished parsing
                        return last;
                    }
                    Some(Element::List(list)) => {
                        list.push(last);
                    },
                    Some(Element::Value(_))=> panic!("Got Value but List was expected"),
                    
                }
            },
            ',' => (),
            c if c.is_digit(10) => {
                let mut value = c.to_digit(10).unwrap() as i32;
                loop {
                    let c = it.peek().unwrap();
                    if c.is_digit(10){
                        value = value * 10 + c.to_digit(10).unwrap() as i32;
                        it.next();
                    } else {
                        break;
                    }
                }
                //To solve some issue with the borrow checker I do pop and push
                let mut element = stack.pop().unwrap();
                match &mut element {
                    Element::List(list) => {
                        list.push(Element::Value(value));
                    },
                    _ => panic!("List was expected")
                }
                stack.push(element);
            },
            _ => panic!("Invalid char {}", c)
        }
    }
}

fn main(){
    let mut lines = io::stdin().lines();
    let mut index = 1;
    let mut part1 = 0;

    let mut packets = Vec::new();

    while let Some(Ok(line)) = lines.next(){
        let line2 = lines.next().unwrap().unwrap();
        lines.next();

        let packet1 = parse_packet(&line);
        let packet2 = parse_packet(&line2);

        if packet1 < packet2{
            part1 += index;
        }
        index += 1;

        packets.push((false, packet1));
        packets.push((false, packet2));
    }

    println!("{}", part1);

    packets.push((true, Element::List(vec![Element::List(vec![Element::Value(2)])])));
    packets.push((true, Element::List(vec![Element::List(vec![Element::Value(6)])])));

    packets.sort_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap());

    let mut a = None;

    for (i, (flag, _)) in packets.iter().enumerate(){
        if *flag{
            match a {
                None => a = Some(i),
                Some(a) => {
                    println!("{}", (a+1) * (i+1));
                    break;
                }
            }
        }
    }
}
