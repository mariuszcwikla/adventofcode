use std::{io::{self}, collections::{VecDeque, BTreeSet, HashMap}, time::Duration, thread, env};
use console::Term;

const CHAMBER_WIDTH: usize = 7;
struct Shape<'a> {
    shape: Vec<&'a str>
}


struct CaveMap{
    map: VecDeque<u8>,
    bottom_line: usize,
    full_lines_locations: BTreeSet<usize>,
    last_full_line_loc: Option<usize>,
    has_full_line_already: bool  
}

impl CaveMap{
    fn new() -> Self{
        CaveMap{
            map: VecDeque::new(),
            bottom_line: 0,
            full_lines_locations: BTreeSet::new(),
            last_full_line_loc: None,
            has_full_line_already: false
        }
    }

    fn get(&self, x: usize, y: usize) -> char {
        if y < self.bottom_line {
            return '?'
        }
        if y - self.bottom_line < self.map.len(){
            let line = self.map[y - self.bottom_line];
            if line & (1 << x) > 0{
                return '#';
            }
        }
        '.'
    }

    fn mark(&mut self, x: usize, y: usize) {
        if y - self.bottom_line >= self.map.len(){
            self.map.resize(y + 1 - self.bottom_line, 0);
        }
        let mut line = self.map[y - self.bottom_line];
        line |= 1<<x;
        self.map[y - self.bottom_line] = line;
        while self.map.len() > 50{
            self.map.pop_front();
            self.bottom_line += 1;
        }

    }

    fn height(&self) -> usize {
        self.map.len() + self.bottom_line
    }
    
    fn is_line_full(&self, y: usize) -> bool{
        if y < self.bottom_line{
            return false;
        }
        if y > self.height(){
            return false;
        }

        return self.map[y - self.bottom_line] == 0b01111111u8;
    }

    fn find_full_line(&self) -> Option<usize> {
        for i in 0..self.map.len(){
            if self.map[i] == 0b01111111u8{
                return Some(self.bottom_line + i);
            }
        }
        None
    }

    fn mark_shape(&mut self, x: usize, y: usize, shape: &Shape) -> Option<(usize, usize)> {
        // let new_full_lines = BTreeSet::new();
        let mut y1: Option<usize> = None;
        let mut y2: Option<usize> = None;
        for yy in 0..shape.height(){
            for xx in 0..shape.width(){
                if let Some('#') = shape.shape[yy].chars().nth(xx){
                    self.mark(x + xx, y + yy);
                }
            }
            if self.is_line_full(yy + y){
                y1 = y1.or(Some(y+yy)).min(Some(y+yy));
                y2 = y1.or(Some(y+yy)).max(Some(y+yy));
                self.last_full_line_loc = Some(yy + y);
                self.full_lines_locations.insert(yy + y);
            }
        }
        if let None = y1 {
            None
        } else {
            Some((y1.unwrap(), y2.unwrap()))
        }
    }

    fn get_block(&self, y1: usize,  y2: usize) -> Vec<u8> {
        let mut block = Vec::new();
        for y in (y1 - self.bottom_line)..(y2 - self.bottom_line){
            block.push(self.map[y]);
        }
        block
    }
}

impl<'a> Shape<'a>{

    fn new(shape: Vec<&str>) -> Shape {
        let mut shape = shape.clone();
        shape.reverse();
        Shape{shape: shape}
    }

    fn width(&self) -> usize {
        self.shape[0].len()
    }

    fn height(&self) -> usize {
        self.shape.len()
    }

    fn try_apply_gas(&self, c: char, x: usize) -> usize {
        if c == '<'{
            if x > 0 {
                return x - 1;
            }
        }
        else {
            if x + self.width() < CHAMBER_WIDTH {
                return x + 1;
            } 
        }

        x
    }

    fn overlaps(&self, x: usize, y: usize, map: &CaveMap) -> bool {
        for yy in 0..self.height(){
            for xx in 0..self.width(){
                if let Some('#') = self.shape[yy].chars().nth(xx){
                    if let '#' = map.get(x + xx, y + yy){
                        return true
                    }
                }
            }
        }
        false
    }
}

#[derive(Hash, PartialEq, Eq)]
struct BlockSignature{
    block: Vec<u8>,
    shape_index: usize
}

fn main(){
    let args: Vec<String> = env::args().collect();
    
    let console = Term::buffered_stdout();

    let console_opt = if args.len() > 1 && args[1]=="--render"{
                Some(&console)
            } else {
                None
            };
            
    let mut jet_pattern = String::new();
    io::stdin().read_line(&mut jet_pattern).unwrap();

    let shape1 = Shape::new(vec![
        "####"
    ]);
    let shape2 = Shape::new(vec![
        ".#.",
        "###",
        ".#."
    ]);
    let shape3 = Shape::new(vec![
        "..#.",
        "..#",
        "###"
    ]);
    let shape4 = Shape::new(vec![
        "#",
        "#",
        "#",
        "#",
    ]);
    let shape5 = Shape::new(vec![
        "##",
        "##"
    ]);
    
    let shapes = vec![shape1, shape2, shape3, shape4, shape5];
    
    let mut iteration = 0;

    let mut map = CaveMap::new();

    let mut state = SimulationState{jet_index: 0, time: 0};
    let mut signatures: HashMap<BlockSignature, usize> = HashMap::new();

    let mut signatures_total_size = 0;

    let mut last_full: Option<usize> = None;
    loop {
        let shape_idx = iteration % shapes.len();
        iteration += 1;
        let full_lines = simulate(&mut map, &shapes[shape_idx], &jet_pattern, console_opt, &mut state);
        if let Some((y1, y2)) = full_lines{
            if let Some(block_bottom) = last_full{
                let block = map.get_block(block_bottom, y1);
                let signature = BlockSignature{
                    block,
                    shape_index: shape_idx
                };
                if let Some(time) = signatures.get(&signature){
                    println!("Found duplicate block signature at {}, iteration ={}", time, iteration);
                    return;
                } else {
                    signatures_total_size += signature.block.len();
                    println!("Size of signatures {}", signatures_total_size);
                    signatures.insert(signature, iteration);
                }
            }
            last_full = Some(y2);
        }
        // if let Some(y) = map.find_full_line(){
        //     println!("Full line found at {} time={} block={}", y, state.time, shape_idx);
        //     if let Some(_) = console_opt{
        //         println!("Hit ENTER to continue");
        //         let mut dontcare = String::new();
        //         io::stdin().read_line(&mut dontcare).unwrap();
        //     }
        // }
        // shape_idx = (shape_idx + 1) % shapes.len();
    }
}

impl CaveMap{

    fn print_map(&self, shape_opt: Option<(&Shape, usize, usize)>, jet :char, console: Option<&Term>, time: i32) {
        if let None = console{
            return;
        }

        let min_y = if self.bottom_line > 5{
            self.bottom_line - 5
        } else {
            0
        };
        let mut max_y = self.height();
        if let Some((shape, _, y)) = shape_opt{
            max_y = max_y.max(y + shape.height());
        }

        let mut buf = vec![vec!['.'; CHAMBER_WIDTH]; max_y - min_y + 1];

        for y in min_y..=max_y{
            for x in 0..CHAMBER_WIDTH{
                buf[y - min_y][x]=self.get(x, y);
            }
        }

        if let Some((shape, x, y)) = shape_opt{
            for (sy, line) in shape.shape.iter().enumerate(){
                for (sx, c) in line.chars().enumerate(){
                    if c == '#'{
                        buf[y + sy - min_y][x + sx] = '@';
                    }
                }
            }
        }

        let console = console.unwrap();
        console.clear_screen().unwrap();
        let (_, console_height) = console.size();
        let console_height = console_height as usize;
        // let h = (console_height as usize).min(buf.len());
        
        for console_y in (0..console_height).rev(){
            if console_y >= buf.len(){
                console.write_line("").unwrap();
            } else {
                let line = buf[console_y].iter().collect::<String>();
                let line = format!("{} {}", line, min_y + console_y);
                console.write_line(&line).unwrap();    
            }
        }
        console.write_line(&format!("time {} {}", time, jet)).unwrap();
        console.flush().unwrap();
        thread::sleep(Duration::from_millis(50));
    }
}
struct SimulationState{
    jet_index: usize,
    time: i32
}

fn simulate(map: &mut CaveMap, shape: &Shape, 
            jet_pattern: &String, console: Option<&Term>, 
            state: &mut SimulationState) -> Option<(usize, usize)> {
    
    let mut x = 2;
    let mut y = map.height() + 3;

    loop {
        state.time += 1;
        let c = jet_pattern.chars().nth(state.jet_index).unwrap();
        state.jet_index = (state.jet_index + 1) % jet_pattern.len();
        map.print_map(Some((shape, x, y)), c, console, state.time);
        let x1 = shape.try_apply_gas(c, x);
        if !shape.overlaps(x1, y, &map){
            x = x1;
        }
        if shape.overlaps(x, y - 1, &map) || y == 0{
            return map.mark_shape(x, y, &shape);
        } else {
            y -= 1;
        }
    }
}