use std::{io::{self}, collections::HashMap, time::Duration, thread, env};
use console::Term;

const CHAMBER_WIDTH: usize = 7;
struct Shape<'a> {
    shape: Vec<&'a str>
}

impl<'a> Shape<'a>{

    fn new(shape: Vec<&str>) -> Shape {
        let mut shape = shape.clone();
        shape.reverse();
        Shape{shape: shape}
    }

    fn width(&self) -> usize {
        self.shape[0].len()
    }

    fn height(&self) -> usize {
        self.shape.len()
    }

    fn try_apply_gas(&self, c: char, x: usize) -> usize {
        if c == '<'{
            if x > 0 {
                return x - 1;
            }
        }
        else {
            if x + self.width() < CHAMBER_WIDTH {
                return x + 1;
            } 
        }

        x
    }

    fn overlaps(&self, x: usize, y: usize, map: &HashMap<(usize, usize), char>) -> bool {
        for yy in 0..self.height(){
            for xx in 0..self.width(){
                if let Some('#') = self.shape[yy].chars().nth(xx){
                    if let Some('#') = map.get(&(x + xx, y + yy)){
                        return true
                    }
                }
            }
        }
        false
    }

    fn mark_on_map(&self, x: usize, y: usize, map: &mut HashMap<(usize, usize), char>) {
        for yy in 0..self.height(){
            for xx in 0..self.width(){
                if let Some('#') = self.shape[yy].chars().nth(xx){
                    map.insert((x + xx, y + yy), '#');
                }
            }
        }
    } 
}

fn main(){
    let args: Vec<String> = env::args().collect();
    
    let console = Term::buffered_stdout();

    let console_opt = if args.len() > 1 && args[1]=="--render"{
                Some(&console)
            } else {
                None
            };
            
    let mut jet_pattern = String::new();
    io::stdin().read_line(&mut jet_pattern).unwrap();

    let shape1 = Shape::new(vec![
        "####"
    ]);
    let shape2 = Shape::new(vec![
        ".#.",
        "###",
        ".#."
    ]);
    let shape3 = Shape::new(vec![
        "..#.",
        "..#",
        "###"
    ]);
    let shape4 = Shape::new(vec![
        "#",
        "#",
        "#",
        "#",
    ]);
    let shape5 = Shape::new(vec![
        "##",
        "##"
    ]);
    
    let shapes = vec![shape1, shape2, shape3, shape4, shape5];
    
    let mut block_num = 0;

    let mut map: HashMap<(usize, usize), char> = HashMap::new();

    let mut state = SimulationState{jet_index: 0, time: 0};
    for _s in 0..2022{
        simulate(&mut map, &shapes[block_num], &jet_pattern, console_opt, &mut state);
        block_num = (block_num + 1) % shapes.len();
    }
    let max_y = *map.keys().map(|(_, y)| y).max().unwrap() + 1;
    println!("{}", max_y);
}

fn print_map(map: &HashMap<(usize, usize), char>, shape_opt: Option<(&Shape, usize, usize)>, jet :char, console: Option<&Term>, time: i32) {
    // let max_y = *map.keys().map(|(_, y)| y).max().unwrap();
    // let min_y = *map.keys().map(|(_, y)| y).min().unwrap();
    if let None = console{
        return;
    }

    let min_y = 0;
    let mut max_y = *map.keys().map(|(_, y)| y).max().unwrap_or(&0usize);
    if let Some((shape, _, y)) = shape_opt{
        max_y = max_y.max(y + shape.height());
    }

    let mut buf = vec![vec!['.'; CHAMBER_WIDTH]; max_y - min_y + 1];

    for y in min_y..=max_y{
        for x in 0..CHAMBER_WIDTH{
            if let Some('#') = map.get(&(x, y as usize)){
                buf[y][x]='#';
            }
        }
    }

    if let Some((shape, x, y)) = shape_opt{
        for (sy, line) in shape.shape.iter().enumerate(){
            for (sx, c) in line.chars().enumerate(){
                if c == '#'{
                    buf[y + sy][x + sx] = '@';
                }
            }
        }
    }

    let console = console.unwrap();
    console.clear_screen().unwrap();
    let (_, console_height) = console.size();
    let console_height = console_height as usize;
    // let h = (console_height as usize).min(buf.len());
    
    for console_y in (0..console_height).rev(){
        if console_y >= buf.len(){
            console.write_line("").unwrap();
        } else {
            let line = buf[console_y].iter().collect::<String>();
            console.write_line(&line).unwrap();    
        }
    }
    console.write_line(&format!("time {} {}", time, jet)).unwrap();
    console.flush().unwrap();
    thread::sleep(Duration::from_millis(100));
}

struct SimulationState{
    jet_index: usize,
    time: i32
}

fn simulate(mut map: &mut HashMap<(usize, usize), char>, shape: &Shape, jet_pattern: &String, console: Option<&Term>, state: &mut SimulationState) {
    let mut y = map.keys().map(|(_, y)| y).max().unwrap_or(&0) + 3 + 1;
    let mut x = 2;

    loop {
        let c = jet_pattern.chars().nth(state.jet_index).unwrap();
        state.jet_index = (state.jet_index + 1) % jet_pattern.len();
        print_map(&map, Some((shape, x, y)), c, console, state.time);
        let x1 = shape.try_apply_gas(c, x);
        if !shape.overlaps(x1, y, &map){
            x = x1;
        }
        if shape.overlaps(x, y - 1, &map){
            shape.mark_on_map(x, y, &mut map);
            return;
        } else {
            y -= 1;
        }
        state.time += 1;
        if y == 0{
            shape.mark_on_map(x, y, &mut map);
            return;
        }
    }
}