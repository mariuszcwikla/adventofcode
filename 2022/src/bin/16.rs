use std::{io, collections::{HashMap, VecDeque, HashSet}};
use std::hash::{Hash, Hasher};
use regex::Regex;

#[derive(Clone, Eq, PartialEq)]
struct ValvePressureAt<'a>{
    valve: &'a String,
    // pressure_per_minute: i32,
    total_pressure: i32,            //total pressure so far
    opened_valves: HashSet<String>,
    time: i32
}

impl<'a> Hash for ValvePressureAt<'a>{
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.valve.hash(state);
        self.total_pressure.hash(state);
        for s in self.opened_valves.iter(){
            s.hash(state);
        }
        self.time.hash(state);
    }
}

struct ValveEdge{
    rate: i32,
    leads_to: Vec<String>
}

fn main(){
    //Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
    //alt.pattern

    let mut valve_map: HashMap<String, ValveEdge> = HashMap::new();
    let mut lines = io::stdin().lines();
    while let Some(Ok(line)) = lines.next(){
        let re = Regex::new(r"Valve (\w\w) has flow rate=(\d*); tunnels? leads? to valves? ((?:\w\w)(?:, \w\w)*)").unwrap();
        let captures = re.captures(&line).unwrap();
        let cur_valve = captures.get(1).unwrap().as_str();
        let flow: i32 = captures.get(2).unwrap().as_str().parse().unwrap();
        let valves_str = captures.get(3).unwrap().as_str();
        let mut valves = Vec::new();
        for part in valves_str.split(","){
            valves.push(part.trim().to_string());
        }
        let valve_edge = ValveEdge{rate: flow, leads_to: valves};
        valve_map.insert(cur_valve.to_string(), valve_edge);
    }

    let start = ValvePressureAt{valve: &"AA".to_string(), total_pressure: 0, opened_valves: HashSet::new(), time: 0};

    //Nope this won't work. Runs in exponential time
    //Need to track "visited"

    
    let mut queue: VecDeque<ValvePressureAt> = VecDeque::new();
    queue.push_back(start);
    let mut visited: HashSet<ValvePressureAt> = HashSet::new();

    let mut max = 0;

    const MAX_TIME: i32 = 30;
    loop {
        
        let p = queue.pop_front().unwrap();
        // println!("time {}, total pressure={}", p.time, p.total_pressure);
        let valve = valve_map.get(p.valve).unwrap();
 
        if p.time == MAX_TIME{
            println!("{}", p.total_pressure);
            max = max.max(p.total_pressure);
        }
        if p.time == MAX_TIME + 1{
            println!("Part 1: {}", max);
            break;
        }
        for outgoing in valve.leads_to.iter(){
            //cloning stuff is not very optimized... I guess
            let mut vp = p.clone();
            vp.valve = outgoing;
            vp.time += 1;
            // vp.total_pressure += vp.pressure_per_minute;
            if let None = visited.get(&vp){
                visited.insert(vp.clone());
                queue.push_back(vp);
            }
        }

        let time_remaining = MAX_TIME - p.time;
        if let None = p.opened_valves.get(p.valve){
            let mut vp = p.clone();
            vp.time += 1;
            vp.opened_valves.insert(p.valve.clone());
            vp.total_pressure += valve.rate * time_remaining;
            // vp.pressure_per_minute += valve.rate;
            if let None = visited.get(&vp){
                queue.push_back(vp.clone());
                visited.insert(vp);
            }
        }
    }
}