use std::io;


fn get_visibility(map: &Vec<Vec<u8>>, x: usize, y: usize) -> usize {
    let h = map[y][x];
    let mut visibility = 4;
    for i in 0..x{
        if map[y][i] >= h {
            visibility -= 1;
            break;
        }
    }
    for i in 0..y{
        if map[i][x] >= h {
            visibility -= 1;
            break;
        }
    }
    for i in (x+1)..(map[0].len()){
        if map[y][i] >= h {
            visibility -= 1;
            break;
        }
    }
    for i in (y+1)..(map.len()){
        if map[i][x] >= h {
            visibility -= 1;
            break;
        }
    }

    visibility
}

fn scenic_score(map: &Vec<Vec<u8>>, x: usize, y: usize) -> usize {
    let h = map[y][x];

    let mut a = 0;
    for i in (0..x).rev(){
        a += 1;
        if map[y][i] >= h {
            break;
        }
    }

    let mut b = 0;
    for i in (0..y).rev(){
        b += 1;
        if map[i][x] >= h {
            break;
        }
    }

    let mut c = 0;
    for i in (x+1)..(map[0].len()){
        c += 1;
        if map[y][i] >= h {
            break;
        }
    }

    let mut d = 0;
    for i in (y+1)..(map.len()){
        d += 1;
        if map[i][x] >= h {
            break;
        }
    }
    a * b * c * d
}
fn main(){
    let mut map = Vec::new();
    for line in io::stdin().lines(){
        let line = line.unwrap();
        let mut row: Vec<u8> = Vec::new();
        for c in line.chars(){
            let digit = c.to_digit(10).unwrap() as u8;
            row.push(digit);
        }
        map.push(row);
    }

    let mut part1 = 0;
    let mut part2 = 0;
    for y in 0..(map.len()){
        for x in 0..(map[y].len()){
            let vis = get_visibility(&map, x, y);
            if vis > 0 {
                part1 += 1;
            }
            let score = scenic_score(&map, x, y);
            if score > part2{
                part2 = score;
            }
        }
    }

    println!("{part1}");

    println!("{part2}");
}
