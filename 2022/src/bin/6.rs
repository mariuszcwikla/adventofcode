use std::io;

fn main(){
    let s = io::stdin().lines().next().unwrap().unwrap();
    
    for i in 3..s.len(){
        if is_uniq(&s[(i-3)..=i]) {
            println!("{}", i + 1);
            break
        }
    }

    for i in 13..s.len(){
        if is_uniq(&s[(i-13)..=i]) {
            println!("{}", i + 1);
            break
        }
    }
}

fn is_uniq(s: &str) -> bool {
    for i in 0..(s.len() - 1){
        for j in i+1 .. (s.len()){
            if s.chars().nth(i) == s.chars().nth(j){
                return false;
            }
        }
    }
    true
}