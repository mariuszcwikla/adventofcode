use std::io;

#[derive(PartialEq, Clone, Copy)]
enum GameOption{
    Rock,
    Paper,
    Scissors
}

use GameOption::*;

impl GameOption{
    fn of_player1(c: &str) -> GameOption{
        match c{
            "A" => Rock,
            "B" => Paper,
            "C" => Scissors,
            _ => panic!()
        }
    }
    fn of_player2(c: &str) -> GameOption{
        match c{
            "X" => Rock,
            "Y" => Paper,
            "Z" => Scissors,
            _ => panic!()
        }
    }
}

fn calculate_score(player1: &GameOption, player2: &GameOption) -> i32{
    let mut score = 0;

    score += match player2{
        Rock => 1,
        Paper => 2,
        Scissors => 3
    };
    if player1 == player2{
        score += 3; //draw
    }
    else {
        if *player2 == Rock && *player1 == Scissors{
            score += 6;
        } else if *player2 == Paper && *player1 == Rock {
            score += 6;
        } else if *player2 == Scissors && *player1 == Paper {
            score += 6;
        }
        //else -> lost
    }
    score
}

fn calculate_score_part2(player1: &GameOption, player2: &str) -> i32{

    let player2_strategy = match player2{
        "X" => match player1 {
            Rock => Scissors,
            Paper => Rock,
            Scissors => Paper
        },
        "Y" => *player1,
        "Z" => match player1 {
            Rock => Paper,
            Paper => Scissors,
            Scissors => Rock
        },
        _ => panic!()
    };

    calculate_score(player1, &player2_strategy)
}

fn main(){
    let mut score = 0;
    let mut score_part2 = 0;
    for line in io::stdin().lines(){
        let line = line.unwrap();
        let data = line.split(" ").collect::<Vec<_>>();
        let player1 = data[0];
        let player2 = data[1];
        let player1 = GameOption::of_player1(player1);
        let player2 = GameOption::of_player2(player2);
        score += calculate_score(&player1, &player2);
        score_part2 += calculate_score_part2(&player1, data[1]);
    }

    println!("{}", score);
    println!("{}", score_part2);
}