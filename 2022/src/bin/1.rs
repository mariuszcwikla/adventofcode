use std::{io};

fn main(){
    
    let mut elves_calories: Vec<i32> = Vec::new();
    let mut max_calories = 0;
    let mut current_calories = 0;
    for line in io::stdin().lines(){
        let line = line.unwrap();
        let line = line.trim();
        if line.is_empty(){
            elves_calories.push(current_calories);
            current_calories = 0;
            continue;
        }
        let calories: i32 = line.parse().unwrap();
        current_calories += calories;

        if current_calories > max_calories{
            max_calories = current_calories;
        }
    }
    println!("{}", max_calories);

    elves_calories.sort();
    let size = elves_calories.len();
    let sum = elves_calories[size - 3] +  elves_calories[size - 2] + elves_calories[size - 1];

    // ans 198010 - too high
    
    //println!("{}", elves_calories.iter().map(|x| x.to_string()).collect::<Vec<_>>().join(" "));
    println!("{}", sum);
}