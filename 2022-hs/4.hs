import Data.List.Split

containsFully a1 a2 b1 b2 = (b1>=a1 && b2<=a2) || (a1>=b1 && a2 <= b2)

overlap a1 a2 b1 b2 = not $ a2 < b1 || a1 > b2

main = do
    content <- getContents
    let x = map (\line ->
            let (left:right:_) = splitOn "," line
                (l1:l2:_) = map (read :: String -> Int) $ splitOn "-" left
                (r1:r2:_) = map (read :: String -> Int) $ splitOn "-" right
                part1 = containsFully l1 l2 r1 r2
                part2 = overlap l1 l2 r1 r2
            in (part1, part2)
            ) $ lines content
    print $ length $ filter (fst) x
    print $ length $ filter (snd) x