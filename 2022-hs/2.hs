import Data.List

data Shape = Rock | Paper | Scissors deriving (Enum, Eq, Show)

toShape :: String -> Shape
toShape s = case s
    of "A" -> Rock
       "X" -> Rock
       "B" -> Paper
       "Y" -> Paper
       "C" -> Scissors
       "Z" -> Scissors

shapeScore :: Shape -> Int
shapeScore Rock = 1
shapeScore Paper = 2
shapeScore Scissors = 3

gameScore :: Shape -> Shape -> Int
gameScore a b
    | a == b = 3
    | b == Rock && a == Scissors = 6
    | b == Paper && a == Rock = 6
    | b == Scissors && a == Paper = 6
    | otherwise = 0

score :: Shape -> Shape -> Int
score a b = gameScore a b + shapeScore b

lineToScore :: String -> Int
lineToScore line = 
    let (a:b:_) = words line
        shape_a = toShape a
        shape_b = toShape b
    in score shape_a shape_b

-- part 2 solution
getWinningShapeFor :: Shape -> Shape
getWinningShapeFor Rock = Paper
getWinningShapeFor Paper = Scissors
getWinningShapeFor Scissors = Rock


line2ToScore:: String -> Int
line2ToScore line = 
    let (a:b:_) = words line
        shape_a = toShape a
        shape_b = getWinningShapeFor shape_a
    in score shape_a shape_b

day2 :: [String] -> Int
day2 list = sum $ map lineToScore list

day2Part2 :: [String] -> Int
day2Part2 list = sum $ map line2ToScore list

main = do
    input <- getContents
    print $ day2 $ lines input
    print $ day2Part2 $ lines input
