import Data.List.Split
import Data.List
import Data.Char (isDigit, isSpace) 
import qualified Data.Map as M
import Debug.Trace

type Stacks = M.Map Int String

parseStacks :: [String] -> Stacks
parseStacks top = M.fromList $ zip [1..] stacks  where
        stacks = map (dropWhile isSpace . init) $ filter (isDigit . last) $ transpose top

move :: Stacks -> Bool -> String -> Stacks
--move stacks instruction | trace (show stacks ++ " <- " ++ instruction) False = undefined
move stacks part1 instruction = 
    let ["move", nStr, "from", fromStr ,"to", toStr] = words instruction
        n = read nStr :: Int
        from = read fromStr :: Int 
        to = read toStr :: Int
        (Just stackA) = M.lookup from stacks
        (Just stackB) = M.lookup to stacks
        (payload, stackANew) = splitAt n stackA
        stackBNew = if part1 then
                        reverse payload ++ stackB       --part1
                    else
                        payload ++ stackB               --part2
    in M.insert from stackANew $ M.insert to stackBNew stacks

move1 stacks = move stacks True
move2 stacks = move stacks False

main = do
    input <- getContents
    let top:bottom:_ = splitWhen null $ lines input
    let stacks = parseStacks top
    let after1 = foldl move1 stacks bottom
    let part1 = map head $ M.elems after1
    putStrLn part1
    let after2 = foldl move2 stacks bottom
    let part2 = map head $ M.elems after2
    putStrLn part2