import Data.List

dropEmptyLine :: [String] -> [String]
dropEmptyLine (x:xs)
    | null x = xs
    | otherwise = x:xs

groupInput :: [String] -> [[String]]
groupInput a = 
    let grouped = groupBy (\ _ b -> not $ null b) a
    in map (dropEmptyLine) grouped 
    --in grouped 

main = do 
    d <- getContents
    let grouped = groupInput $ lines d
        asInt = map (map (read::String->Int)) grouped
        summed = map (sum) asInt
    print $ maximum summed
    print $ sum $ take 3 $ reverse $ sort summed