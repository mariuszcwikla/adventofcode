import Data.List
import Data.Char ( ord, isAsciiLower, isAsciiUpper )
import Debug.Trace ( trace )
import Data.List.Split ( chunksOf )
elementToPriority :: Char -> Int
--elementToPriority c | trace (show c) False = undefined
elementToPriority c 
    | isAsciiLower c = ord c - ord 'a' + 1
    | isAsciiUpper c = ord c - ord 'A' + 27
    | otherwise = error "Invalid character"

part1 :: String -> Int
part1 s = 
    let mid = length s `div` 2
        (a, b) = splitAt mid s
        commonLetter = head $ a `intersect` b
        ans = elementToPriority commonLetter
    in elementToPriority commonLetter

part2 :: [String] -> Int
part2 [a, b, c] = elementToPriority $ head $ a `intersect` b `intersect` c
part2 _ = undefined

main = do
    input <- getContents
    print $ sum . map part1 $ lines input
    print $ sum . map part2 $ chunksOf 3 $ lines input