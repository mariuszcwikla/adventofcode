import Data.List (nub)

findMarker :: String -> Int -> Int
findMarker s n = findMarker' s + n
    where findMarker' s
            | isMarker' s = 0
            | otherwise = findMarker' (tail s) + 1
                where isMarker' ss = length (nub $ take n ss) == n
    
main = do
    line <- getLine
    print $ findMarker line 4
    print $ findMarker line 14