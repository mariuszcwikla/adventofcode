require 'scanf'
class Rule
	attr_accessor :no_other, :bag_full_color, :may_contain, :total
	def initialize(bag_variant, bag_color)
		@bag_variant = bag_variant
		@bag_color = bag_color
		@bag_full_color = bag_variant + ' ' + bag_color
		@may_contain = []
		@no_other = false
		@total = 0
	end
	def add_rule(variant, color, count)
		raise if variant=='bag' || variant=='bags' || color=='bag' || color=='bags'
		@may_contain << [variant, color, count]
	end
	
	def inspect
		s = @bag_variant + ' ' + @bag_color
		if @no_other
			return s + ', no other'
		else
			s += ', ' + @may_contain.inspect
		end
	end
	
	def is_eligible
		return false if @no_other
		
		@may_contain.each do |v, c, cnt|
			full_color = v + ' ' + c
			return true if cnt>0 && full_color == 'shiny gold'
		end
		
		return false
	end
end

def parse(line)
	words = line.split
	r = Rule.new(words[0], words[1])
	if line.include? ("no other")
		r.no_other = true
	else
		# light red bags contain 1 bright white bag, 2 muted yellow bags.
		n = words[4].to_i
		color = words[6]
		r.add_rule(words[5], color, n)
		
		punct = words[7][-1]
		i = 8
		while punct==','
			n = words[i].to_i
			variant, color = words[i+1], words[i+2]
			r.add_rule(variant, color, n)
			punct = words[i+3][-1]
			i+=4
		end
	end
	r
end

rules = {}
cnt = 0
i = 0
while line=gets
	rule = parse(line)
	rules[rule.bag_full_color]=rule
end

def is_eligible(rules, r)
	stack = [r]
	until stack.empty?
		r = stack.pop
		return true if r.is_eligible
		
		r.may_contain.each do |v, c, cnt|
			cc = v + ' ' + c
			r = rules[cc]
			stack << r unless r.nil?
		end
	end
	false
end

def visit(rules, r)
	puts "visiting: " + r.bag_full_color
	t = 0
	r.may_contain.each do |v, c, cnt|
		t += cnt
		t += cnt * visit(rules, rules[v + ' ' + c]) if cnt > 0
	end
	t
end

rules.each_value do |r|
	cnt += 1 if is_eligible(rules, r)
end

puts cnt

puts visit(rules, rules['shiny gold'])


# Ans: 13 - WA, 18-WA