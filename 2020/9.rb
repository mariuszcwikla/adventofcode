data = []
while line=gets
	data << line.to_i
end

def is_valid(data, index, preamble_len)
	start = index - preamble_len
	for i in start...(start+preamble_len - 1)
		for j in (i+1)...(start + preamble_len)
			if data[i] + data[j] == data[index]
				return true
			end
		end
	end
	false
end

preamble_len = 25

first_invalid = 0
preamble_len.upto(data.length-1).each do |i|
	if !is_valid(data, i, preamble_len)
		first_invalid = data[i]
		break
	end
end

puts first_invalid

def find_sum(data, expected)
	for i in 0...(data.length-1)
		sum = data[i]
		for j in (i+1)...data.length
			sum += data[j]
			if sum == expected
				return data[i..j]
			end
		end
	end
	nil
end

found = find_sum(data, first_invalid)
puts found.min + found.max

=begin
preamble_len.upto(data.length-1).each do |i|
	if !is_valid(data, i, preamble_len) && !is_valid(data, i+1, preamble_len)
		smallest = [data[i], data[i+1]].min
		largest = [data[i], data[ii+1]].max
		j = i+2
		while j < data.length && !is_valid(data, j, preamble_len)
			smallest = data[j] if data[j] < smallest
			largest = data[j] if data[j] > largest
			j+=1
		end
		puts smallest + largest
		break
	end
end
=end

