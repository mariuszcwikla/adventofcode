require 'scanf'

memory = {}
memory.default = 0

def apply_mask(mask, value)
	output = mask.gsub('X', '0')
	i = 35
	while value > 0
		if mask[i]=='X'
			output[i] = value[0].to_s
		end
		
		value >>= 1
		i -= 1
	end
	output.to_i(2)
end

while line = gets&.chomp
	if line.start_with? 'mask'
		mask = line.split[2]
	else
		address, value = line.scanf("mem[%d] = %d")
		new_value = apply_mask(mask, value)
		memory [address] = new_value
	end
end

puts memory.values.sum