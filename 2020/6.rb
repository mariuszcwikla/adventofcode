def count(ans)
	ans.reduce(:+).chars.uniq.size
end

def count2(ans)
	x = ans[0].chars.uniq
	for i in 1...ans.length
		x = x & ans[i].chars
	end
	x.length
end

c1 = 0
c2 = 0
while line=gets
	ans = [line.chomp]
	while line=gets
		break if line.nil?
		line.chomp!
		break if line.empty?
		ans << line
	end
	
	c1 += count(ans)
	cc = count2(ans)
	#puts cc
	c2 += cc
end
puts c1
puts c2

#part 938 - WA (too low)