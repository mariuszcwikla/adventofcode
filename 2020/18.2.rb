def evaluate(expr)
	stack = []
	i = 0
	out = []
	while i < expr.size		
		if expr[i]=='('
			stack.push '('
		elsif expr[i]==')'
			until (op=stack.pop)=='('
				out << op
			end
		elsif expr[i]=='*'
			until stack.empty? || stack[-1]=='('
				out << stack.pop
			end
			stack << '*'
		elsif expr[i]=='+'
			stack << '+'
		elsif expr[i] >= '0' && expr[i] <='9'
			val = 0
			while i < expr.size && expr[i] >= '0' && expr[i] <='9'
				val = val * 10 + expr[i].to_i
				i+=1
			end
			i-=1	# because later there is i+=1
			out << val
			
		end
		i+=1
	end
	until stack.empty?
		out << stack.pop
	end
	
	stack = []
	out.each do |token|
		if token=='*'
			stack << (stack.pop * stack.pop)
		elsif token=='+'
			stack << (stack.pop + stack.pop)
		else
			stack << token
		end
	end
	stack[0]
end

sum = 0
while line=ARGF.gets
	x = evaluate(line.chomp.gsub(/ /, ''))
	#puts x
	sum += x
end

puts "sum: #{sum}"