numbers = {}

input = gets.split(',').map(&:to_i)
input.each_with_index do |x, i|
	numbers[x]=i+1
end

last = input.last
next_val = 0		# puzzle input does not have duplicate numbers

index = input.size
N = 30000000
(N-input.size - 1).times do |k|
	perc=k *100.0/ N
	puts "#{k} - #{perc.round(2)}%" if k % 100_000 == 0
	index += 1
	if numbers[next_val].nil?		
		numbers[next_val]=index
		next_val = 0
	else
		i = numbers[next_val]
		#puts numbers.inspect
		numbers[next_val]=index
		#puts "index=#{index}, i=#{i}"
		next_val = index - i
	end	
	#puts next_val
end

puts next_val