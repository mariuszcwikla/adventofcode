def parse_loc(s)
	lx, hx = 0, 128
	for i in 0...7
		m = (lx+hx)/2
		if s[i]=='F'
			hx = m
		else
			lx = m
		end
	end
	ly, hy = 0, 8
	for i in 7...10
		m = (ly + hy)/2
		if s[i]=='R'
			ly = m
		else
			hy = m
		end
	end
	#puts [lx, hx, ly, hy].inspect
	[lx, ly]
end

#seats = Array.new(128) { Array.new(8) {0} }

occupied = {}
max = 0
while line=gets&.chomp
	next if line.empty?
	x, y = parse_loc(line)
	id = x * 8 + y
	occupied[id]=1
	max = id if id > max
end
puts "Part 1 #{max}"

start = false
0.upto (127*8) do |i|
	start = true if occupied[i]
	if start && occupied[i].nil?
		puts "Part 2: #{i}"
		break
	end
end
# 889 - wrong answer

