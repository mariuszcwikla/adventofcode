map = {}
y = 0
while line = gets	
	line.chomp!
	line.chars.each_with_index do |c, x|
		map[ [x - line.size / 2, y - line.size / 2, 0] ] = c
	end
	y+=1
end

ACTIVE='#'
INACTIVE='.'

def cycle(map, size)	
	new_map = {}
	nsize = size+1
	for z in -nsize..nsize
		for y in -nsize..nsize
			for x in -nsize..nsize
				num_active = 0
				num_inactive = 0
				[-1, 0, 1].repeated_permutation(3).each do |dx, dy, dz|
					nx, ny, nz = x+dx, y+dy, z+dz
					next if nx == x && ny==y && nz == z
					next if nx < -size || nx > size || ny < -size || ny > size || nz < -size || nz > size

					status = map[ [nx, ny, nz] ] || INACTIVE
					num_active += 1 if status==ACTIVE
					num_inactive += 1 if status==INACTIVE
				end
				status = map[ [x, y, z] ] || INACTIVE
				nstatus = if status == ACTIVE
							if [2,3].include? num_active
								ACTIVE
							else
								INACTIVE
							end
						  else
							if 3 == num_active
								ACTIVE
							else
								INACTIVE
							end
						  end
				new_map [[x, y, z]] = nstatus
			end
		end
	end

	[new_map, size+1]
end

def draw_layer(map, z, size)
	puts "z=#{z}"
	for y in -size..size
		line = ''
		for x in -size..size
			line += map[ [x, y, z] ] || ' '
		end
		puts line
	end
end

size = y/2

6.times do |i|
	puts "iteration #{i}"
	map, size = cycle(map, size)
end

puts map.values.count('#')
