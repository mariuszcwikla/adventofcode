require 'scanf'

images = {}
while line=gets
  id, _ = line.scanf("Tile %d:")
  image = []
  while line=gets&.chomp
	break if line.empty?
	image << line
  end
  images[id]=image
end

width = Math.sqrt(images.size).to_i
puts width

lefts = Hash.new{|h, k| h[k]=[]}
rights = Hash.new{|h, k| h[k]=[]}
tops = Hash.new{|h, k| h[k]=[]}
bottoms = Hash.new{|h, k| h[k]=[]}
all = Hash.new{|h, k|h[k]=[]}

images.each do |id, image|
	left = ""
	right = ""
	top = image[0]
	bottom = image.last
	image.each do |line|
		left += line[0]
		right += line[-1]
	end
	
	lefts[left] << id
	lefts[left.reverse] << id
	rights[right] << id
	rights[right.reverse] << id
	tops[top] << id
	tops[top.reverse] << id
	bottoms[bottom] << id
	bottoms[bottom.reverse] << id
	
	all[left] << id
	all[left.reverse] << id
	all[right] << id
	all[right.reverse] << id
	all[top] << id
	all[top.reverse] << id
	all[bottom] << id
	all[bottom.reverse] << id
	
end

singles = all.select{|k, v| v.size==1}

singles.each do |k, v|
	singles[k]=v[0]
end

mul = 1
singles.values.uniq.each do |id|
	count = singles.select{|k, v| v == id}.count
	mul *= id if count == 4
	#puts "#{id} - #{count}"
end

puts mul