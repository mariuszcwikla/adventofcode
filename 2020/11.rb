grid = STDIN.readlines.map(&:chomp)

def sorroundings(grid, w, h, x, y)
	s = []
	s << grid[y-1][x] if y > 0
	s << grid[y][x-1] if x > 0 
	s << grid[y+1][x] if y < h-1
	s << grid[y][x+1] if x < h-1
	
	s << grid[y-1][x-1] if y > 0 && x > 0
	s << grid[y-1][x+1] if y > 0 && x < w - 1
	s << grid[y+1][x-1] if y < h - 1 && x > 0
	s << grid[y+1][x+1] if y < h - 1 && x < w - 1
	
	s
end

def simulate(grid)
	grid2 = []
	grid.each do |row| grid2 << row.dup end
	w, h = grid[0].length, grid.length
	stabilized = true
	for y in 0...h
		for x in 0...w
			s = sorroundings(grid, w, h, x, y)
			grid2[y][x]=grid[y][x]
			if grid[y][x]=='L'
				if !s.include?('#')
					grid2[y][x]='#' 
					stabilized = false
				end
			elsif grid[y][x]=='#'
				if s.count('#') >= 4
					grid2[y][x]='L'
					stabilized = false
				end
			end
		end
	end
	[grid2, stabilized]
end

step = 1
while true
	puts "step: #{step}"
	step+=1
	grid, stabilized = simulate(grid)
	#puts grid
	#puts ''
	break if stabilized	
	#break if step > 5
end

cnt = grid.map { |row| row.count('#') }.reduce(&:+)
puts cnt