data = gets.strip.chars.map{|c| c.ord - '0'.ord}
p data


class Node
    attr_accessor :next, :prev, :value
    def initialize(value)
        @value = value
        @next = self
        @prev = self
    end
    def insert_after(value)
        n = Node.new(value)
        n.next = @next
        n.prev = self
        @next.prev = n
        @next = n
        n
    end

    def remove3
        last = n.next.next.next
        first = n.next
        @next = last.next
        last.next.prev = self

        first.prev = last
        last.next = first
        first
    end
end

root = nil
tail = nil
data.each do |d|
    if root.nil?
        tail = root = Node.new(d)
    else
        tail = tail.insert_after(d)
    end
end

current = root

10.times do
    headOf3 = current.remove3
end