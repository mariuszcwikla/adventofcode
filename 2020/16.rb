require 'scanf'
rules = {}

while line=ARGF.gets
	line.chomp!
	break if line.empty?
	i = line.index(':')
	name = line[0...i]
	ranges = []
	line[(i+1)..-1].split(" or ").each do |part|
		a, b = part.split('-')
		a = a.to_i
		b = b.to_i
		ranges << (a..b)
	end	
	rules[name]=ranges
end

#puts rules.inspect

line = ARGF.gets.chomp
raise unless line=='your ticket:'

your_ticket = ARGF.gets.split(',').map(&:to_i)

ARGF.gets

line = ARGF.gets.chomp
raise unless line=='nearby tickets:'

nearby_tickets = []
while line=ARGF.gets
	nearby_tickets << line.split(',').map(&:to_i)
end

rules_bits = {}
rules_bits.default = 0
rules.values.flatten.each do |range|
	range.each { |i| rules_bits[i] = 1}
end

def find_invalid(record, rules_bits)
	record.select { |i| rules_bits[i] == 0 }	
end

cnt = 0

valid_nearby = []
nearby_tickets.each do |ticket|
	invalid = find_invalid(ticket, rules_bits)
	cnt += invalid.reduce(&:+) || 0
	valid_nearby << ticket if invalid.empty?
end

puts cnt

# valid_nearby = nearby_tickets
# Part 2
# TODO: just use bipartite matching
def matches(rule, value)
	!rule.index{|r| r.include? value}.nil?
end
mapping = {}

require 'set'
def to_graph(rules, valid_nearby)
	set_s = *(0...rules.size)
	graph = Hash.new{|h, k| h[k]=Set.new}
	rules.each do |name, rule|
		#for i in 0...valid_nearby[0].length
		for i in 0...rules.size
			all_match = true
			for valid in valid_nearby
				val = valid[i]
				if !matches(rule, val)
					all_match = false
					break
				end
			end
			if all_match
				graph[name] << i
				graph[i] << name
			end
		end
	end
	[graph, set_s]
end

def bipartite_matching(graph, set_s)
	unmatched = set_s.dup
	matching = {}
	occupied_t = {}	
	done = false
	found_free_t = true
	while unmatched.size > 0
		q = [[unmatched.first, :s]]
		parent = {}
		visited = {}
		found_free_t = false
		t_vertex = nil
		visited[unmatched.first]=true
		while !q.empty? && !found_free_t
			v, type = q.shift
			visited[v]=true
			graph[v].each do |u|
				next unless visited[u].nil?	#i.e. visited
				parent[u]=v
				if type == :s && !occupied_t[u]
					found_free_t = true
					t_vertex = u
					occupied_t[u] = true
					break
				else
					u_type = type == :s ? :t : :s
					q << [u, u_type]
					visited[u]=true
				end
			end
		end
		# augment path
		if found_free_t
			n = t_vertex
			# S: T:
			# cases
			# 1. direct: S1 -> T1
			# 2. augmented, from: S1->T1
			#           new path: S1->T2->S2->T1 	(delete T1->M[T1], then add S2->T1)
			loop do
				a, b = n, matching[n]
				matching.delete a
				#matching.delete b
				#puts "augment: #{n} -> #{parent[n]}"
				matching[parent[n]] = n

				n = parent[n]	# from T to U
				break if n == unmatched.first
				n = parent[n]	# from U to T so that at start of each iteration we are at T
			end
			# matching[n] = parent[n]
			# matching[parent[n]]=n
			unmatched.delete n
		else
			puts "WARN: could not findd path from vertex #{unmatched.first}"
			unmatched.delete unmatched.first
		end
	end
	
	matching
end

puts valid_nearby.inspect
graph, set_s = to_graph(rules, valid_nearby)
#puts 'vertices: ' + graph.keys.inspect
puts "GRAPH:"
graph.each do |u, v|
	if set_s.include? u
		puts "#{u} => #{v.to_a.join ','}"
	end
end
#puts 'BIPARTITE_MATCHING'
matching = bipartite_matching(graph, set_s)
#puts matching.inspect
puts "MATCHING................."
matching.each do |u, v|
	if set_s.include? u
		puts "#{u} => #{v}"
	end
end

puts "YOUR TICKET................."
ans = 1
n = 0
your_ticket.each_with_index do |t, i|
	#puts "#{matching[i]}: #{t}"
	if matching[i]&.start_with? 'departure'
		puts "multiplying: #{matching[i]} = #{t}"
		ans *= t
		n +=1 
	end
end

puts "Found #{n} departure elements"
puts "Part 2: #{ans}"

# ANS 4066904279 - WA (too low)

=begin
should_continue = true
rules_found = {}
values_found = {}
while should_continue
	should_continue = false
	for i in 0...valid_nearby[0].length
		next if values_found[i]
		accepting_rules = rules.keys - rules_found.values
		for valid in valid_nearby
			val = valid[i]
			accepting_rules.each do |rule|
				accepting_rules.delete(rule) unless matches(rules[rule], val)
			end
			if accepting_rules.size==1
				values_found[i]=true
				rules_found[i]=accepting_rules[0]
				should_continue = true
			end
		end
		puts "#{i} #{accepting_rules.inspect}"
		
	end
	puts "========"
end
puts rules_found.inspect

puts "diff"
puts rules.keys - rules_found.values

=end

