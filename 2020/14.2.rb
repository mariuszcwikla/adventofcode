require 'scanf'

#memory = Hash.new {|h, k| h[k]='0' * 36}
memory = {}
memory.default = 0

def apply_mask(mask, address, value, memory)
	
	i = 35
	locations = []
	masked = address.to_s(2).rjust(36, '0')
	for i in 0...36
		if mask[i]=='X'
			masked[i] = 'X'
			locations << i
		elsif mask[i]=='1'
			masked[i] = '1'
		end
	end
	
	# powerset construction ;) - 
	for k in 0...(2**locations.size)		
		locations.each_with_index do |loc, index|
			if k[index]==0
				masked[loc]='0'
			else
				masked[loc]='1'
			end			
		end
		#puts "#{masked}, #{masked.to_i(2)}"
		memory[masked]=value
	end
end

while line = gets&.chomp
	if line.start_with? 'mask'		
		mask = line.split[2]
	else
		address, value = line.scanf("mem[%d] = %d")
		apply_mask(mask, address, value, memory)
	end
end

puts memory.values.sum