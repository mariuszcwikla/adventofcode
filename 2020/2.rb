require 'scanf'
valid = 0
second = 0
while line=gets
	break if line.chomp.empty?
	a, b, letter, pass=line.scanf("%d-%d %c: %s")
	valid += 1 if (a..b).include? (pass.count(letter))
	c1 = pass[a-1]
	c2 = pass[b-1]
	second += 1 if (c1==letter || c2==letter) && (c1 != c2)
end

puts valid
puts second