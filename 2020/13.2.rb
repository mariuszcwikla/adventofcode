
timestamp = gets.to_i
ids = []
gets.split(',').each_with_index do |d, i|
	if d!='x'
		a = d.to_i
		ids << [a, (a - i) % a]
	end
end

def ext_euclid(a, b)
	s0 = 1
	s1 = 0
	t0 = 0
	t1 = 1
	while a >= 1
		r = b / a
		s0, s1 = s1, s0 - r * s1
		t0, t1 = t1, t0 - r * t1
		b, a = a, b % a
	end
	[s0, t0]
end

a = ids.map{|x, _| x}
b = ids.map{|_, x| x}
m = a.reduce(&:*)

e = a.map do |x| 
	mi = m / x
	g, f = ext_euclid(x, mi)
	#puts "#{f}, #{g}, #{g * x + f * mi}"
	ei = g*mi % m
end

puts e.zip(b).map{|a, b| a * b}.reduce(&:+) % m