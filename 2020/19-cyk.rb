# I've been trying to implement CYK algorithm here
# but apparently I'm trying to kill a fly with a cannon. Later I've implemented regex - see 19.rb.
# This I'll try to finish someday for the sake of learning CYK algorithm.

Rule = Struct.new(:type, :value) do 
  def matches?(type, value)
    self.type == type && self.value == value
  end
end

rules = Hash.new {|h, k| h[k]=[]}
line_no = 0

rules_to_simplify = []
rules_to_split = []
while line=ARGF.gets&.chomp
  line_no += 1
  break if line.empty?
  id, rest = line.split(":")
  id = id.to_i
  rest.strip!
  
  if rest[0]=='"'
    righthand = rest.gsub(/"/, '')
    rules[id] << Rule.new(:terminal, righthand)
  else
    type = :nonterminal
    rest.split("|").each do |part|
      righthand = part.split.map(&:to_i)
	  
	  rules[id] << Rule.new(:nonterminal, righthand)
	  if righthand.size == 1
		rules_to_simplify << id
	  elsif righthand.size > 2
		rules_to_split << id
      elsif righthand.size == 0
	    raise 'empty rule'
	  end
	  #raise "not a Chomsky normal form; number of nonterminals #{righthand.size} at line #{line_no}" if righthand.size>2
    end
  end
end

last = -1		#negative indicates new rules
rules_to_simplify.each do |id|
  replacement = rules[id].value[0]
  rules.delete id
  rules.values.flatten.each do |r|
    #if r.value.include? (id)
	  
end

def find_start_rule(rules)
	all_keys = rules.keys
	rules.values.flatten.each do |r|
		if r.type==:nonterminal
			r.value.each {|k| all_keys.delete k}
		end
	end
	puts all_keys
	all_keys[0]
end

def find_terminal_rules(rules, char)
  rules.keys.select do |k|
    !rules[k].index{|r| r.matches?(:terminal, char)}.nil?
  end
end

def cyk_match(rules, start, string)
  inv_rules = Hash.new{|h, k| h[k]=[]}		# if rules contain rule   A (key of hash) -> BC | DE (value of hash)
											# then inv_rules contain  BC -> A and DE -> A for fast lookup
  rules.each do |id, rule|
	rule.each {|rh| inv_rules[rh.value] << id}
  end

  table = {}
  for i in 0...string.length
    table [[i,i]] = find_terminal_rules(rules, string[i])
  end
  
  for len in 2..string.length
	for i in 0...(string.length-len+1)
	  j = i + len - 1
	  set = []
	  for k in i...j
	  #binding.pry
	    a = table[[i, k]]
	    b = table[[k+1, j]]
		a.product(b).each do |r|
			set += inv_rules[r]
		end
	  end
	  table[[i, j]] = set
	end
  end
  puts table.inspect
  puts table[[0, string.length-1]]&.inspect
  table[[0, string.length-1]].include? start
end

start = find_start_rule(rules)


while line = ARGF.gets&.chomp
  puts "CYK for #{line}"
  if cyk_match(rules, start, line)
    puts "matches"
  else
    puts "does not match"
  end
  break
end