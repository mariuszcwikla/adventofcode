require 'scanf'
def parse_record(r, line)
	line.split.each do |part|
		a, b = part.split(":")
		r[a]=b
	end
end

FIELDS=%w(byr iyr eyr hgt hcl ecl pid cid)

def is_valid?(r)
	FIELDS.each do |f|
		next if f=='cid'
		return false if r[f].nil?
	end
	true
end

def is_valid2?(r)

	x = r['byr'].to_i
	return false unless (1920..2002).include? x
	
	x = r['iyr'].to_i
	return false unless (2010..2020).include? x
	
	x = r['eyr'].to_i
	return false unless (2020..2030).include? x
	
	h, type = r['hgt'].scanf("%d%s")
	return false if type.nil?
	return false if type=='cm' && !(150..193).include?(h)
	return false if type=='in' && !(59..76).include?(h)
	
	c = r['hcl']
	return false unless c[0]=='#'
	return false if (/^\h{6}$/ =~ c[1..-1]).nil?
#binding.pry	
	return false unless %w(amb blu brn gry grn hzl oth).include? r['ecl']
	
	return false if (/^\d{9}$/ =~ r['pid']).nil?
	
	true
end
valid = 0
valid2 = 0
while line=ARGF.gets
	record = {}
	parse_record(record, line.chomp)
	while line=ARGF.gets
		line.chomp!		
		break if line.empty?
		parse_record(record, line.chomp)
	end
	if is_valid?(record)
		valid +=1 
		valid2 +=1 if is_valid2?(record)
	end
end

puts valid
puts valid2