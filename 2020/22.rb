gets
player1 = []
loop do
	line = gets.chomp
	break if line.empty?
	player1 << line.to_i
end

player2 = []
gets
while line=gets&.chomp
	player2 << line.to_i
end

#player1.reverse!
#player2.reverse!


def play(player1, player2)
	until player1.empty? || player2.empty?
		a = player1.shift
		b = player2.shift
		if a < b
			#puts '2 wins'
			player2 << b << a
		elsif a > b
			#puts '1 wins'
			player1 << a << b
		else
			raise "Draw with #{a}"
		end
	end
	
	if player1.empty?
		[player2, :p2]
	else
		[player1, :p1]
	end
end


result = play(player1.dup, player2.dup)[0]

puts "Part 1: #{result.reverse.each_with_index.map {|x, i| x * (i+1) }.sum}"


$global_rounds = {}

def play2(player1, player2)
	puts "Global rounds: #{$global_rounds.size}"
	op1 = player1.dup
	op2 = player2.dup
	if $global_rounds[[player1, player2]]
		return $global_rounds[[player1, player2]]
	end
	rounds = {}
	until player1.empty? || player2.empty?
		if rounds[[player1,player2]]
			$global_rounds[[op1, op2]]=[player1, :p1]
			return [player1, :p1]
		end
		rounds[[player1.dup, player2.dup]]=true
		
		a = player1.shift
		b = player2.shift
		
		if player1.length >= a && player2.length >= b
			result, winner = play2(player1[0...a], player2[0...b])
			if winner == :p1
				player1 << a << b
			else
				player2 << b << a
			end
		else
			if a < b
				#puts '2 wins'
				player2 << b << a
			elsif a > b
				player1 << a << b
			end
		end
	end
	
	if player1.empty?
		$global_rounds[[op1, op2]]=[player2, :p2]
		[player2, :p2]
	else
		$global_rounds[[op1, op2]]=[player1, :p1]
		[player1, :p1]
	end
end

result = play2(player1.dup, player2.dup)[0]
puts "Part 2: #{result.reverse.each_with_index.map {|x, i| x * (i+1) }.sum}"

# ans 33441
# After solving I found some people implemented simple iterative solution? https://www.reddit.com/r/adventofcode/comments/khaiyk/2020_day_21_solutions/ - search for "when I read part 2, one loop got the answer"