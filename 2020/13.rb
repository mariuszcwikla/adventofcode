timestamp = gets.to_i
ids = gets.split(',').select{|x| x!='x'}.map(&:to_i)

min = ids[0]
min_time = ids[0] - timestamp % ids[0]
for i in 1...ids.size
	tt = ids[i] - timestamp % ids[i]
	if tt < min_time
		min_time = tt
		min = ids[i]
	end
end

puts min_time
puts min
puts min * min_time
