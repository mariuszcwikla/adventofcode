require 'scanf'

images = {}
while line=ARGF.gets
  id, _ = line.scanf("Tile %d:")
  image = []
  while line=ARGF.gets&.chomp
	break if line.empty?
	image << line
  end
  images[id]=image
end

def left(image)
	image.map{|row| row[0]}.join ''
end
def right(image)
	image.map{|row| row[-1]}.join ''
end
def top(image)
	image[0]
end
def bottom(image)
	image[-1]
end

width = Math.sqrt(images.size).to_i
puts width

all = Hash.new{|h, k|h[k]=[]}

images.each do |id, image|
	left = ""
	right = ""
	top = image[0]
	bottom = image.last
	image.each do |line|
		left += line[0]
		right += line[-1]
	end
	
	all[left] << id
	all[left.reverse] << id
	all[right] << id
	all[right.reverse] << id
	all[top] << id
	all[top.reverse] << id
	all[bottom] << id
	all[bottom.reverse] << id
	
end

singles = all.select{|k, v| v.size==1}

singles.each do |k, v|
	singles[k]=v[0]
end

mul = 1
corners = singles.values.uniq.select do |id|
	count = singles.select{|k, v| v == id}.count
	count == 4
end

puts corners.reduce(&:*)

def drop_borders(image)
	image[1..-2].map {|row| row[1..-2]}
end

def flip_vertical(image)
	image.map {|row| row.reverse}
end

def flip_horizontal(image)
	image.reverse
end

top_left = images[corners[0]]
if all[left(top_left)].size==2
	top_left = flip_vertical(top_left)
end
if all[top(top_left)].size==2
	top_left = flip_horizontal(top_left)
end

last_id = corners[0]
last_image = top_left
first_in_row = top_left
first_in_row_id = last_id

top_left = drop_borders(top_left)
full_image = top_left.dup


def render(buffer, image, y)
	
	if y==buffer.size
		#buffer += image		# crap... += creates new array; does not modify original object
		buffer.concat(image)
	else
		for yy in 0...image.size
			buffer[y + yy] += image[yy]
		end
	end
end

image_width = images.first[1].size

def rotate(image)
	rotated = Array.new(image.size){""}
	
	for x in 0...image.size
		for y in 0...image.size
			rotated[y]+=image[x][y]
		end
	end
	rotated
end

def do_transformations(image)
	img = image
	4.times do
		yield img
		img = rotate(img)
	end
	
	img = flip_horizontal(image)
	4.times do
		yield img
		img = rotate(img)
	end
	
	img = flip_vertical(image)
	4.times do
		yield img
		img = rotate(img)
	end
	
	img = flip_vertical(flip_horizontal((image)))
	4.times do
		yield img
		img = rotate(img)
	end
end

def match_left(image, border)
	do_transformations(image){|transformed|
		return transformed if left(transformed)==border
	}
	
	puts "Unable to match border: #{border}"
	puts image.join "\n"
	raise 'does not match'
end

def match_up(image, border)
	do_transformations(image){|transformed|
		return transformed if top(transformed)==border
	}
	raise 'does not match'
end

begin
for y in 0...width
	x = 1
	while x < width
		ids = all[right(last_image)].select{|id| id != last_id}
		last_id = ids[0]
		last_image = match_left(images[last_id], right(last_image))
		render(full_image, drop_borders(last_image), y * (image_width-2))
		x+=1
	end
	
	if y < width-1
		bo = bottom(first_in_row)
		last_id = all[bo].select{ |id| id != first_in_row_id }[0]
		
		last_image = match_up(images[last_id], bo)
		render(full_image, drop_borders(last_image), (y+1) * (image_width-2))
		
		first_in_row_id = last_id
		first_in_row = last_image
	end
end 
rescue => e
	puts e.message
	puts e.backtrace
end
#puts full_image.join "\n"

monster = ["                  # ",
		   "#    ##    ##    ###",
		   " #  #  #  #  #  #   "]


def monster_line_matches(monster_line, map_line)
	for i in 0...monster_line.size
		return false if monster_line[i]=='#' && map_line[i]!='#'
	end
	true
end

def find_monsters(full_image, monster)
	width = full_image.length
	monster_width = monster[0].length
	
	found = []
	for y in 0...(width - monster.length)
		for x in 0...(width - monster_width)
			match = true
			for yy in 0...monster.length
				if !monster_line_matches(monster[yy], full_image[y+yy][x...(x+monster_width)])
					match = false
					break
				end
			end
			found << [x, y] if match
		end
	end
	found
end

def clear(buffer, x, y, width, height, monster)
	for yy in y...(y+height)
		for xx in x...(x+width)
			buffer[yy][xx]='o' if monster[yy-y][xx-x]=='#'
			
		end
	end
end

puts "Searching monsters"
do_transformations(full_image){|transformed|
	transformed = transformed.dup
	transformed.map!{|row| row.dup}
	#puts transformed.join "\n"
	#puts ""
	n = find_monsters(transformed, monster)
	if n.size > 0
		puts "found #{n.size} monsters"
		n.each do |x, y|
			clear(transformed, x, y, monster[0].size, monster.size, monster)
		end
		puts transformed.join "\n"
		cnt = 0
		transformed.each {|row| cnt += row.count('#') }
		puts "Part 2 solution: #{cnt}"
		
	end
}

# Ans: 2881 - too high

#puts all.values.map{|n| n.length}.inspect