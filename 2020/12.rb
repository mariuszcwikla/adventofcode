data = STDIN.readlines.map do |line|
	[line[0], line[1..-1].to_i]
end

x, y = 0, 0
dir = 'E'

def move(x, y, dir, value)
	case dir 
	when 'E'
		x += value
	when 'W'
		x -= value
	when 'N'
		y += value
	when 'S'
		y -= value
	else
		raise 'unknown: ' + dir
	end
	[x, y]
end

def rotate(dir, value)
	value %= 360
	value /= 90
	dirs = ['N', 'E', 'S', 'W']
	i = dirs.index(dir)
	dirs[(i + value) % 4]
end
	

data.each do |instruction, value|
	if instruction == 'F'
		x, y = move(x, y, dir, value)
	elsif instruction == 'R'
		dir = rotate(dir, value)
	elsif instruction == 'L'
		dir = rotate(dir, 360 - value)
	else
		x, y = move(x, y, instruction, value)
	end
end


puts (x.abs + y.abs)