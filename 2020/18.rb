def evaluate(expr)
	stack = []
	i = 0
	vals = []
	while i < expr.size		
		if expr[i]=='('
			stack.push '('
		elsif expr[i]==')'
			j = -1
			until stack[j]=='('
				j -= 1
			end
			val = vals[j]
			for k in (j+1)..-1			
				op = stack[k]
				b = vals[k]
				if op=='*'
					val *=b
				else
					val += b
				end
				#j += 1
			end
			stack=stack[0...j]
			vals=vals[0...j]
			vals.push val
		elsif expr[i]=='+' || expr[i]=='*'
			stack.push expr[i]
		elsif expr[i] >= '0' && expr[i] <='9'
			#raise "Unknown char #{expr[i]}" unless expr[i]>='0' && expr[i]<='9'
			val = 0
			while i < expr.size && expr[i] >= '0' && expr[i] <='9'
				val = val * 10 + expr[i].to_i
				i+=1
			end
			i-=1	# because of next i+=1
			vals.push val
			
		end
		i+=1
	end
	val = vals[0]
	for i in 0...stack.size
		op = stack[i]
		b = vals[i+1]
		if op=='*'
			val *=b
		else
			val += b
		end
	end
	
	val
end

sum = 0
while line=ARGF.gets
	x = evaluate(line.chomp)
	puts x
	sum += x
end

puts "sum: #{sum}"