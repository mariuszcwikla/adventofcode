Rule = Struct.new(:type, :value)

rules = {}
line_no = 0

while line=ARGF.gets&.chomp
  line_no += 1
  break if line.empty?
  id, rest = line.split(":")
  id = id.to_i
  rest.strip!
  
  if rest[0]=='"'
    righthand = rest.gsub(/"/, '')
    rules[id] = Rule.new(:terminal, righthand)
  else	
    value = rest.split("|").map do |part|
      part.split.map(&:to_i)
    end
	rules[id] = Rule.new(:nonterminal, value)
  end
end

def find_start_rule(rules)
	all_keys = rules.keys
	rules.values.each do |r|
		if r.type==:nonterminal
			r.value.flatten.each {|k| all_keys.delete k}
		end
	end
	all_keys[0]
end

start = find_start_rule(rules)
puts "Start rule: #{start}"

def make_regex(rules, id)
	r = rules[id]
	if r.type == :terminal
		r.value
	else
		s = "("
		s += r.value.map{|vals| 
			vals.map { |v| make_regex(rules, v) }.join ''
		}.join '|'
		s += ')'
		s
	end
end

reg = /^#{make_regex(rules, start)}$/
puts reg

total = 0
while line = ARGF.gets&.chomp
  if reg =~line
    puts "matches"
	total +=1 
  else
    puts "does not match"
  end  
end
puts "ans: #{total}"