data = gets.strip.chars.map{|c| c.ord - '0'.ord}
10.upto(1_000_000) do |i|
    data << i
end
class Node
    attr_accessor :next, :value
    def initialize(value)
        @value = value
        @next = self
    end
    def insert_after(value)
        n = Node.new(value)
        n.next = @next
        @next = n
        n
    end

    def remove3
        last = self.next.next.next
        first = self.next
        @next = last.next
        last.next = nil
        first
    end
    def include3(v)
        return [@value, @next.value, @next.next.value].include? v
    end 
    def find(value)
        n = self
        loop do
            return n if n.value == value
            n = n.next
            raise 'infinite loop' if n == self
        end
    end
    def insert_listOf3(list)
        tail3 = list.next.next
        tail3.next = @next
        @next = list
    end
    def to_a
        a = [@value]
        n = self.next
        until n == self
            a << n.value
            n = n.next
        end
        a
    end
end

root = nil
tail = nil
data.each do |d|
    if root.nil?
        tail = root = Node.new(d)
    else
        tail = tail.insert_after(d)
    end
end

current = root

def decr(v)
    if v-1 == 0
        return 9
    else
        return v - 1
    end
end


10_000_000.times do |i|
    puts i if i % 10 == 0
    headOf3 = current.remove3
    v = decr(current.value)
    if headOf3.include3 v
        v = decr(v)
        if headOf3.include3 v
            v = decr(v)
            if headOf3.include3 v
                v = decr(v)
            end
        end
    end
    destination = current.next.find(v)
    destination.insert_listOf3(headOf3)
    current = current.next
end

starter = root.find(1).next
puts starter.to_a[..-2].join ''
