map = []
while line=gets
	map << line.chomp
end


def ride(map, sx, sy)
	x, y = 0, 0
	width = map[0].size
	height = map.size
	count = 0
	while y < height
		count += 1 if map[y][x]=='#'
		x = (x+sx) % width
		y+=sy
	end
	count
end
puts "#1: #{ride(map, 3, 1)}"

p2 = [ [1, 1], [3, 1], [5, 1], [7, 1], [1, 2] ].map{|sx, sy| ride(map, sx, sy)}.reduce(&:*)
puts "#2: #{p2}"