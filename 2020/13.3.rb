def ext_euclid(a, b)
	s0 = 1
	s1 = 0
	t0 = 0
	t1 = 1
	while a >= 1
		r = b / a
		s0, s1 = s1, s0 - r * s1
		t0, t1 = t1, t0 - r * t1
		b, a = a, b % a
	end
	#[s0, s1, t0, t1, b]
	[s0, t0]
end


a = [17, 13, 19]
b = [0, 11, 16]

#a = [13, 19]
#b = [2, 3]

#a = [4, 5, 7]
#b = [3, 4, 1]


m = a.reduce(&:*)
puts m
e = a.map do |x| 
	mi = m / x
	g, f = ext_euclid(x, mi)
	puts "#{f}, #{g}, #{g * x + f * mi}"
	ei = g*mi % m
end

puts e.zip(b).map{|a, b| a * b}.reduce(&:+) % m