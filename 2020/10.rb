
data = STDIN.readlines.map(&:to_i)
data << 0
data << data.max + 3
data.sort!

def count(data, diff)
	cnt = 0
	for i in 1...data.length
		if data[i]-data[i-1]== diff
			cnt += 1 
		end
	end
	cnt
end

count(data, 2)

puts count(data, 1) * count(data, 3)

def part2_groups(data)
	groups = []
	i = 1
	group = []
	while i < data.length
		if data[i] - data[i-1]==1
			group << data[i]
		else
			group = group[0..-2]
			groups << group if group.length > 0
			group = []
		end
		i+=1
	end
	groups
end

def combinations(n)
	if n == 1
		2
	elsif n == 2
		2 ** 2
	elsif n == 3
		2 ** 3 - 1
	else
		raise 'unexpected case'
	end
end


per_group = part2_groups(data).map{|d| combinations(d.length)}

puts per_group.reduce(&:*)