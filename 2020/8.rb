program = []
while line=gets
	a, b = line.chomp.split
	b = b.to_i
	program << [a, b]
end

def run_prog(program)
	ip = 0
	acc = 0
	visited = {}
	while ip < program.length
		if visited[ip]
			return [acc, true]		# true indicates infinite loop
		end
		visited[ip] = true
		i, v = program[ip]
		if i =='jmp'
			ip += v
		elsif i=='acc'
			ip += 1
			acc += v
		else
			ip += 1
		end
	end
	[acc, false]
end

puts run_prog(program)[0]

for i in 0...program.length	
	instr = program[i][0]
	if instr == 'jmp'
		program[i][0]='nop'
	elsif instr == 'nop'
		program[i][0]='jmp'
	else
		next
	end
	puts "Testing #{i}/#{program.length}"
	acc, infinite = run_prog(program)
	if !infinite
		puts acc
		break
	end
	program[i][0]=instr
end
	