data = STDIN.readlines.map do |line|
	[line[0], line[1..-1].to_i]
end

x, y = 0, 0
wx, wy = 10, 1
dir = 'E'

def move(x, y, dir, value)
	case dir 
	when 'E'
		x += value
	when 'W'
		x -= value
	when 'N'
		y += value
	when 'S'
		y -= value
	else
		raise 'unknown: ' + dir
	end
	[x, y]
end

def rotate_waypoint(x, y, wx, wy, value)
	dx = wx
	dy = wy
	
	value %= 360
	value /= 90
	# (0, 10) .. (10, 0) .. (0, -10) .. (-10, 0) .. (0, 10)
	# (1, 10) .. (10, -1) .. (-1, -10) .. (-10, 1) .. (1, 10)
	value.times do
		if dx>=0 && dy>=0
			dx, dy = dy, -dx
		elsif dx>=0 && dy < 0
			dx, dy = dy, -dx
		elsif dx < 0 && dy < 0
			dx, dy = dy, -dx
		else
			dx, dy = dy, -dx
		end
	end
	#[x + dx, y + dy]
	[dx, dy]
end

data.each do |instruction, value|
	if instruction == 'F'
		x += wx * value
		y += wy * value
	elsif instruction == 'R'
		wx, wy = rotate_waypoint(x, y, wx, wy, value)
	elsif instruction == 'L'
		wx, wy = rotate_waypoint(x, y, wx, wy, 360 - value)
	else
		wx, wy = move(wx, wy, instruction, value)
	end	
	#puts "#{instruction}#{value} ship=#{[x, y].inspect}, waypoint=#{[wx, wy].inspect}"
end


puts (x.abs + y.abs)
