# Brute force approach for part #2.
# Unfortunately this will take too long (days?)
# See solution #3 - uses Chinese remainder theorem.

timestamp = gets.to_i
ids = []
gets.split(',').each_with_index do |d, i|
	if d!='x'
		a = d.to_i
		ids << [a, (a - i) % a]
	end
end

def matches(t, ids)
	for id, shift in ids
		return false unless t % id == shift 
	end
	true
end

t = 0
loop do
	puts t if t%1000==0
	if matches(t, ids)
		puts "Matches at #{t}"
		break
	end
	t+=ids[0][0]
end
