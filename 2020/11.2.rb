grid = STDIN.readlines.map(&:chomp)


def find_occupied(grid, w, h, x, y, n)
	i = 1
	cnt = 0
	dirs = [[-1, -1],
		 [-1, 1],
		 [-1, 0],
		 [ 0, -1],
		 [ 0, 1],
		 [ 1, -1],
		 [ 1, 0],
		 [ 1, 1]]
	while true
		any = false
		ndirs = dirs.dup
		ndirs.each do |dx, dy|
			nx = x + dx * i
			ny = y + dy * i
			if nx < 0 || ny < 0 || nx >=w || ny >= h
				dirs.delete([dx, dy])
				next
			end
			if grid[ny][nx]=='#'
				cnt +=1 
			end
			dirs.delete([dx, dy]) unless grid[ny][nx]=='.'
			return true if cnt == n
			any = true
			
		end
		break unless any
		break if dirs.empty?
		i+=1
	end
	false
end

def simulate(grid)
	grid2 = []
	grid.each do |row| grid2 << row.dup end
	w, h = grid[0].length, grid.length
	stabilized = true
	for y in 0...h
		for x in 0...w
			grid2[y][x]=grid[y][x]
			if grid[y][x]=='L'
				unless find_occupied(grid, w, h, x, y, 1)
					grid2[y][x]='#' 
					stabilized = false
				end
			elsif grid[y][x]=='#'
				if find_occupied(grid, w, h, x, y, 5)
					grid2[y][x]='L'
					stabilized = false
				end
			end
		end
	end
	[grid2, stabilized]
end

step = 1
puts grid
puts ""
while true
	puts "step: #{step}"
	step+=1
	grid, stabilized = simulate(grid)
	if ARGV[0]=='--debug'
		puts grid
		puts ''
	end
	break if stabilized	
end

cnt = grid.map { |row| row.count('#') }.reduce(&:+)
puts cnt