import Data.List.Split (splitOn)

data Color  = Blue | Red | Green deriving (Show)
data Instruction = Instruction{
    instructionValue :: Int,
    instructionColor :: Color
} deriving (Show)

type GameSet = [Instruction]
type Game = (Int, [GameSet])

type RGB = (Int, Int, Int)

toColor :: String -> Color
toColor "red" = Red
toColor "green" = Green
toColor "blue" = Blue
toColor c = error $ "unexpected color: [" ++ c ++ "]"

parseSet :: String -> GameSet
parseSet s = let 
    s1 = splitOn "," s
    in map parseElem s1
    where
        parseElem :: String -> Instruction
        parseElem e = let 
            (a:b:_) = words e
            in Instruction {instructionValue = read a :: Int, instructionColor = toColor b}

-- Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
parseInstruction :: String -> Game
parseInstruction s = let
    [left1, right1] = splitOn ":" s
    sets = splitOn ";" right1
    (_:id:_) = words left1
    in (read id, map parseSet sets)

accumInstruction (r, g, b) Instruction{instructionValue=v, instructionColor=color} = case color of
        Red -> (r+v, g, b)
        Green -> (r, g+v, b)
        Blue -> (r, g, b+v)

countGameSet :: GameSet -> (Int, Int, Int)
countGameSet gameset = foldl accumInstruction (0,0,0) gameset

countGame1 :: Game -> [(Int, Int, Int)]
countGame1 (_, g) = map countGameSet g

isPossible1 :: Game -> Bool
isPossible1 g = all (\(r, g, b) -> r <= 12 && g <= 13 && b <= 14) $ countGame1 g

accumRgbByMax::RGB -> RGB -> RGB
accumRgbByMax (r, g, b) (r', g', b') = (max r r', max g g', max b b')

countGame2 :: Game -> RGB
countGame2 (_, gamesets) = foldl accumRgbByMax (0, 0, 0) $ map countGameSet gamesets

powerOfCubes :: RGB -> Int
powerOfCubes (x, y, z) = x * y * z
main = do 
    d <- getContents
    let games = map parseInstruction $ lines d
    print . sum . map fst $ filter isPossible1 games
    print $ sum $ map (powerOfCubes . countGame2) games
