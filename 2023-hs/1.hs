import Data.Char
import Data.List
import Data.Maybe (fromJust, isJust, catMaybes)
import Data.Foldable (msum)

firstDigit s = d where
    x = find isDigit s
    d = digitToInt $ fromJust x

lastDigit s = d where
    x = find isDigit $ reverse s
    d = digitToInt $ fromJust x

strDigits = [
    "zero",
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine"
    ]

findString :: (Eq a) => [a] -> [a] -> Maybe Int
findString search str = findIndex (isPrefixOf search) (tails str)

extractDigits :: String -> (Int, Int)
extractDigits s = (firstDigit s, lastDigit s)

calibrationValue :: String -> Int
calibrationValue s = a * 10 + b
    where (a, b) = extractDigits s

stringPrefixToDigit :: String -> Maybe (Int, String)
stringPrefixToDigit str = find (\(i, s) -> isPrefixOf s str) $ zip [0..] strDigits

unwrapMaybeAnswer Nothing = 0
unwrapMaybeAnswer (Just (x, _)) = x

stringSuffixToDigit :: String -> Maybe (Int, String)
stringSuffixToDigit str = find (\(i, s) -> isSuffixOf s str) $ zip [0..] strDigits

unwrap :: Maybe (Int, String) -> Maybe (Int)
unwrap Nothing = Nothing
unwrap (Just (n, _)) = Just n

strOrInt :: String -> Maybe Int
strOrInt s@(x:_)
    | isDigit x = Just $ digitToInt x
    | otherwise = let
        maybeDigit = stringPrefixToDigit s
        in unwrap maybeDigit
strOrInt [] = Nothing

firstDigit2 :: String -> Int
firstDigit2 str = fromJust $ msum $ map strOrInt $ tails str

lastDigit2 :: String -> Int
lastDigit2 str = fromJust $ msum $ map strOrInt $ reverse $ tails str

calibrationValue2 :: String -> Int
calibrationValue2 s = a * 10 + b
    where a = firstDigit2 s
          b = lastDigit2 s

main = do 
    d <- getContents
    print $ sum $ map calibrationValue $ lines d
    print $ sum $ map calibrationValue2 $ lines d
