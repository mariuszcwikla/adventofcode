instructions=[]
while line=gets
  instructions << line.to_i
end

ip = 0
n = 0
until ip>=instructions.size or ip<0
  k = instructions[ip]
  instructions[ip]+=1
  ip += k
  n+=1
end

puts "Part #1=#{n}"
