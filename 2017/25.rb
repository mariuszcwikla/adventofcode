initial_state = gets.match(/Begin in state (\w)\./)[1]
iterations = gets.match(/Perform a diagnostic checksum after (\d*) steps\./)[1].to_i

gets

program = {}
while line=gets
    state = line.match(/In state (\w):/)[1]

    gets    #If the current value is 0:
    value1 = gets.match(/- Write the value (\d)\./)[1].to_i
    direction1 = gets.match(/- Move one slot to the (left|right)\./)[1].to_sym
    next_state1 = gets.match(/- Continue with state (\w)./)[1]

    gets   #If the current value is 1:
    value2 = gets.match(/- Write the value (\d)\./)[1].to_i
    direction2 = gets.match(/- Move one slot to the (left|right)\./)[1].to_sym
    next_state2 = gets.match(/- Continue with state (\w)./)[1]

    subprogram = {
        0 => {
            :out => value1,
            :direction => direction1,
            :next_state => next_state1
        },
        1 => {
            :out => value2,
            :direction => direction2,
            :next_state => next_state2
        }
    }
    program[state] = subprogram

    gets
end

state = initial_state
tape = {}
tape.default = 0;
slot = 0;
tape[slot] = 0;

iterations.times do
    value = tape[slot]
    subprogram = program[state]
    new_val = subprogram[value][:out]
    direction = subprogram[value][:direction]
    new_state = subprogram[value][:next_state]
    tape[slot] = new_val
    if direction == :left
        slot -= 1
    else
        slot += 1
    end
    state = new_state
    # puts state
end


puts tape.select{|k, v| v == 1}.size
