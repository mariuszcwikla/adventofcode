map=[]
while line=gets
	map << line
end

x,y = map[0].index("|"), 0
dir = :down

def move(x,y, dir, map, width, height)
	x,y = case dir
		when :down then [x, y+1]
		when :up then [x, y-1]
		when :right then [x+1, y]
		when :left then [x-1, y]
	end

    return nil if x<0 or x>=width and y<0 and y>=height
	
	case map[y][x]
		when 'A'..'Z', '|', '-' then return x, y, dir	
		when '+'
          #             |  |
          #   -+   +-  -+  +-
          #    |   |

          if [:down, :up].include? dir
            if x>0 and (map[y][x-1]=='-' or ('A'..'Z').include? map[y][x-1])
              dir=:left
            else
              dir=:right
            end
          else
            if y>0 and (map[y-1][x]=='|' or ('A'..'Z').include? map[y-1][x])
              dir=:up
            else
              dir=:down
            end
          end
          return x, y, dir
    end	
end

x,y = map[0].index("|"), 0
puts map[0]
dir = :down
puts x, y
width=map[0].size
height=map.size
path=""
count=1
while true
  val=move(x,y,dir,map,width, height)
  break if val==nil
  x,y,dir=val
  path << map[y][x] if ('A'..'Z').include? map[y][x]
  count+=1
  #puts "#{x}, #{y}: #{map[y][x]}"
end 

puts "Day 1: #{path}"
puts "Day 2: #{count}"
