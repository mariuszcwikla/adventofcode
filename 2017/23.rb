program=[]
while line=gets
    a,b,c=line.split
    program << [a,b,c]
end

regs={}
regs.default=0

ip = 0

def reg_val(regs, arg)
    if arg =~ /[a-z]/
        return regs[arg]
    else        
        return arg.to_i
    end
end

def do_command(program, ip, regs)
    a,b,c=program[ip]
    
    c = reg_val(regs, c)
    case a 
    when 'set'
        regs[b]=c
        ip+=1
    when 'sub'
        regs[b]-=c
        ip+=1
    when 'mul'
        regs[b]*=c
        ip+=1
    when 'jnz'
        b = reg_val(regs, b)
        if b!=0            
            ip+=c
            #puts "jnz + #{c}"
        else
            #puts "jnz + 1"
            ip+=1
        end
    else
        puts 'unknown command: ' + a
    end
    return ip
end

muls=0
while ip>=0 and ip<program.size
    if program[ip][0]=='mul'
        muls+=1 
    end
    ip=do_command(program, ip, regs)
end

puts "Part #1 #{muls}"

regs={'a'=>1}
regs.default=0

ip = 0
steps=0
while ip>=0 and ip<program.size  and steps < 100
    puts "#{steps}: ip=#{ip} #{program[ip]} #{regs}"
    ip=do_command(program, ip, regs)
    steps+=1
    
    
    
    if steps%10000==0
        puts "#{steps} #{regs['h']}"
    end
end
puts "Part #2 #{regs['h']}"
