input = gets.to_i

n=0
next_to_zero=0
current=0

N = 50000000
while n<N
	n+=1
    current = (current+input) % n
      if current==0
        next_to_zero=n
      end
    current+=1
    puts n if n%10_000_000==0
end

puts "Day 2: #{next_to_zero}"


# Answer: 11995607 in 5.4 seconds
