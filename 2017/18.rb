f = $stdin
#f = File.open('18.1.in')
program=[]
while line=f.gets
  instr, *args = line.strip.split
  program << [instr, args]
end

ip = 0
regs = {}
regs.default=0
lastsound = 0

require 'pry'
require 'pry-byebug'
#binding.pry


class String
  def arg_to_i(regs)
    if self =~ /-?\d+/
      return self.to_i
    else
      return regs[self]
    end
  end
end

while true
  instr, args = program[ip]
  case instr 
  when 'snd'
    lastsound=regs[args[0]]
  when 'set'
    regs[args[0]]=args[1].arg_to_i(regs)
  when 'add'
    regs[args[0]]+=args[1].arg_to_i(regs)
  when 'mul'
    regs[args[0]]*=args[1].arg_to_i(regs)
  when 'mod'
    regs[args[0]]%=args[1].arg_to_i(regs)
  when 'rcv'
    if regs[args[0]]!=0
      break
    end
  when 'jgz'
    if regs[args[0]]>0
      ip+=args[1].arg_to_i(regs)
      next
    end
  end
  ip+=1
end

puts "Part #1: #{lastsound}"
