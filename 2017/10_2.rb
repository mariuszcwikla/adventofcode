vals = gets.strip.bytes.map{|c| c.ord}
puts vals.join ' '
list = *(0..255)
skip=0
pos =0

def hash(vals, list, pos, skip)

  vals.each{|length|
    0.upto(length/2-1){|i|
      a=(pos+i) % list.size
      b=(pos+length-i-1) % list.size
      list[a],list[b]=list[b],list[a]
    }
    pos=(pos + length + skip) % list.size
    skip+=1

  }
  [pos, skip]
end

vals+=[17, 31, 73, 47, 23]
64.times{
  pos, skip=hash(vals, list, pos, skip)
}

out = ""
16.times{|i|
  v = 0
  16.times{|j|
    v^=list[i*16+j]
  }
  v=(v.to_s(16))
  v = '0' + v if v.length==1
  out << v
}

puts out

