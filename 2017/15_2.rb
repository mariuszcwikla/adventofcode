require 'scanf'
a = gets.scanf("Generator A starts with %d")[0]
b = gets.scanf("Generator B starts with %d")[0]

factorA = 16807
factorB = 48271

N = 2147483647


n = 0
5_000_000.times{
  begin
     a = (a*factorA) % N
  end while !(a%4==0)

  begin
    b = (b*factorB) % N
  end while !(b%8==0)
  #puts "#{a} - #{b}"
  n+=1 if (a & 0xffff)==(b & 0xffff)
}

puts "Part #2: #{n}"


