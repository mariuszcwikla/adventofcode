
def parse(pattern)
  return pattern.split("/")
end

def flip_vertical(p)
  return p.map{|x| x.reverse}
end

def flip_horizontal(p)
  return p.reverse
end

def rotate(p)
  out = []
  p.size.times do |i|
    out[i]=""
    p.size.times do |j|
      out[i] << p[j][i]
    end
  end
  return out
end

mapping={}

while line=gets
  k, _, v=line.split
  k=parse(k)
  v=parse(v)
  
  4.times do   
    mapping[k]=v
    mapping[flip_vertical(k)]=v
    mapping[flip_horizontal(k)]=v
    mapping[flip_horizontal(flip_vertical(k))]=v
    k=rotate(k)
  end
  #puts "total mappings: #{mapping.size}"
end

puts "Mappings: "
mapping.each{|k, v|
  puts "#{k.join '/'} -> #{v.join '/'}"
}
puts "total mappings: #{mapping.size}"
pattern=[
  ".#.",
  "..#",
  "###"
]

def process(pattern, size, mapping)
  size=pattern.size
  out=[]
  if size%2==0
    k=2
    (3*size/2).times do out << "" end
  else
    k=3
    (4*size/3).times do out << "" end
  end
  #out=[""]*(size/k)
  #out=[]
  #map={}
  for y in 0..(size/k-1)
    for x in 0..(size/k-1)
      p=[]
      for i in 0..(k-1)
        fragment=pattern[y*k+i][(x*k)..(x*k+k-1)]
        p << fragment
      end
      #puts p.join "\n"
      #puts "---"
      m = mapping[p]
      if m.nil?
          raise "unable to find mapping for:" + (p.join "/")
      end
      for i in 0..k
        #puts "#{size} = #{y*k+i}"
        out[y*(k+1)+i] << m[i]
      end
      #return out, size+1
    end
  end   
  return out, out.size
end

5.times do
  pattern, size=process(pattern, size, mapping)
  #puts pattern.join "\n"
  #puts "----------"
end

puts pattern.join "\n"

n = pattern.map{|line| line.count "#"}.sum
puts "Part #1: #{n}"

6.upto(18) do   
  pattern, size=process(pattern, size, mapping)
end

n = pattern.map{|line| line.count "#"}.sum
puts "Part #2: #{n}"


