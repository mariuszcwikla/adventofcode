require 'set'
invalid = 0
total=0
while line=gets
  s = Set.new  
  total+=1
  line.split.each{|word|
    if s.include? word
      invalid+=1
      break
    end
    s << word
  }
end
puts "Part #1=#{total-invalid} (total=#{total}, invalid=#{invalid}"

