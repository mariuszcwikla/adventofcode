require 'scanf'
programs = *('a'..'p')
moves = gets.strip.split(',').map{|move|
	if move[0]=='s'
		x = move[1..move.size-1].to_i
		[:s, x]
	elsif move[0]=='x'
		a, b = move.scanf("x%d/%d")
		[:x, a, b]
	elsif move[0]=='p'
		a, b = move.scanf("p%c/%c")
		[:p, a, b]
	end
}

visited={}
visited[programs.dup]=0
program_states=[programs.dup]



def dance(moves, programs,start=0)
	moves.each{|move|
	  if move[0]==:s
		x = move[1]
		#programs = programs[programs.size-x..(programs.size-1)] + programs[0..programs.size-1-x]
		start=(start-x)%programs.size
	  elsif move[0]==:x
		a, b = move[1], move[2]
		a = (a+start)%programs.size
		b = (b+start)%programs.size
		programs[a], programs[b]=programs[b],programs[a]
	  elsif move[0]==:p
		a, b = move[1], move[2]
		a = programs.index(a)
		b = programs.index(b)
		#a = (a+start)%programs.size
		#b = (b+start)%programs.size
		programs[a], programs[b]=programs[b],programs[a]
	  end
	}
	return start
end

def ptos(programs, start)
	puts "start=#{start}"
	x=programs[start..programs.size-1].join ''
	x+=programs[0..start-1].join '' if start>0
	return x
end

start=dance(moves, programs)
puts "Part 1: #{ptos(programs, start)}"

visited[programs.dup]=1
program_states << programs.dup


N = 1000000000
n=2
while true
	#puts "#{n}: #{programs.join ''}"
	start=dance(moves, programs,start)
	if(visited[programs]!=nil)
		puts "collision at #{n}"
		period = n
		break
	end
	pp = programs.dup
	visited[pp]=n
	program_states << pp	
	n+=1
	puts n if n%100==0
end

k=N%period
puts "k=#{k}"
puts "Part #2: #{program_states[k].join ''}"