require 'scanf'

class Particle
  attr_reader :index
  def initialize(p, v, a, i)
    @p=p
    @v=v
    @a=a
    @index=i
  end
  def move
    v1 = @v.map{|a| a*a}.sum
    0.upto(2) do |i|
      @v[i]+=@a[i] 
      @p[i]+=@v[i]
    end
    v2 = @v.map{|a| a*a}.sum  
  end
  def dist
    @p.map{|i| i.abs}.sum
  end
end

require 'set'
particles=Set.new
i=0
while line=gets
  p=line.scanf("p=<%d,%d,%d>, v=<%d,%d,%d>, a=<%d,%d,%d>")
  particles << Particle.new(p[0..2], p[3..5], p[6..8], i)
  i+=1
end

puts "num of particles: #{particles.size}"

mindist=nil
minpart=nil
iter=0
while true
  min = particles.min {|a,b| a.dist <=> b.dist}
  if mindist==nil or min.dist < mindist
    mindist=min.dist
    minpart=min
  end
  puts "particle #{min.index}, dist=#{min.dist}, min thusfar=#{mindist}, #{minpart.index}"
  particles.each do |p| p.move end
  iter+=1
  #break if iter>1000
end

#Answers
# 468 - too high
# 363 - too low
# 364 - OK - trzeba znaleźć cząsteczkę, która "w nieskonczonosci" będzie najbliżej do 0,0,0
# a nie najbliżej w ogole

