lines=[]
while line=gets
    lines << line.strip
end

x=lines[0].size/2
y=lines.size/2

map={}
lines.each_with_index do |line, i|
    for j in 0..(line.length-1)    
        map[[j,i]]=line[j]
    end
end

LEFT=0
UP=1
RIGHT=2
DOWN=3

def do_step(map, x, y, dir)
    c = map[[x,y]]
    c = '.' if c.nil?
    
    if c=='#'
        dir=(dir+1)%4
    else
        dir=(dir-1)%4
    end
    
    infected=false
    if c=='.'
        map[[x,y]]='#'
        infected=true
    else
        map[[x,y]]='.'
    end
    
    case dir 
        when LEFT then x-=1 
        when RIGHT then x+=1
        when UP then y-=1 
        else y+=1
    end
    
    [x,y,dir,infected]
end


dir=UP

def dump(map, x, y)
    x1,x2=map.keys.map{|xx,yy| xx}.minmax
    y1,y2=map.keys.map{|xx,yy| yy}.minmax
    
    for yy in y1..y2
        for xx in x1..x2
            c = map[[xx,yy]] || '.'
            if x==xx and y==yy
                print "[#{c}]"
            else
                print " #{c} "
            end
        end
        puts ""
    end
end


def count_infected(map)
    map.values.filter{|c| c=='#'}.count
end
    
#N=70
N=10000

total_infected=0
last_infected=count_infected(map)
N.times do |i|
    #puts "Iteration #{i}"
    #dump(map, x,y)    
    #puts ""
    x,y,dir,infected=do_step(map, x, y, dir)    
    
    total_infected+=1 if infected
end

puts "Part #1: #{total_infected}"
