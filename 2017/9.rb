line = gets.strip

i=0
groupLvl=0
garbage=false
score=0
garbageChars=0
while i<line.size
  c = line[i]
  case c 
    when '{' then groupLvl+=1
    when '}' then 
      score+=groupLvl
      groupLvl-=1
    when '!' then i+=1
    when '<' then
      i+=1 
      while i<line.size and line[i]!='>'
        if line[i]=='!'
          i+=2
        else
          garbageChars+=1
          i+=1
        end
      end
        
  end
  i+=1
end


puts "Part #1: #{score}"
puts "Part #2: #{garbageChars}"
