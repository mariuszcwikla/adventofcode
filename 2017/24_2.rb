require 'scanf'

starters = []
components = Hash.new{|h, k| h[k]=[]}

while line=gets
	a,b=line.scanf("%d/%d")
	if a==0
		starters << [a,b]
	elsif b==0
		starters << [b,a]
	else
		components[a] << b
		components[b] << a
	end
end

def maximize(b, components, bridge)
	list = components[b].dup
	maxstrength = nil
	maxlength = nil
	maxbridge = nil
	
	list.each{|x|
		components[b].delete x
		components[x].delete b
		
		bridge << [b,x]
		
		ms, ml, mb = maximize(x, components, bridge)
		if ms!=nil
			if maxlength==nil or ml > maxlength or (maxlength==ml and ms > maxstrength)
				maxstrength = ms
				maxlength = ml
				maxbridge = mb
			end
		end
		
		bridge.pop
		
		components[b] << x
		components[x] << b
	}
	if maxlength==nil
		return bridge.flatten.sum, bridge.size, bridge.dup
	end
	
	return maxstrength, maxlength, maxbridge	
end

maxlength = nil
maxstrength = nil
for a,b in starters
	bridge=[[a,b]]
	strength, len, bridge = maximize(b, components, bridge)
	
	if maxlength==nil or len > maxlength or (maxlength==len and strength > maxstrength)
		maxstrength = strength
		maxlength = len		
	end
	puts "#{a}/#{b}: strength=#{strength}, length=#{len} #{bridge}"
end

puts "Part 2: #{maxstrength}"

#Answers
#1854 - too low
#1877 - too high
#1868 - ok