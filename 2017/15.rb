require 'scanf'
a = gets.scanf("Generator A starts with %d")[0]
b = gets.scanf("Generator B starts with %d")[0]

factorA = 16807
factorB = 48271

N = 2147483647


n = 0
40_000_000.times{
  a = (a*factorA) % N
  b = (b*factorB) % N
  n+=1 if (a & 0xffff)==(b & 0xffff)
}

puts "Part #1: #{n}"
