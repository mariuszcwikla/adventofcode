f = $stdin
#f = File.open('18.1.in')
$program=[]
while line=f.gets
  instr, *args = line.strip.split
  $program << [instr, args]
end

require 'pry'
require 'pry-byebug'
#binding.pry


class String
  def arg_to_i(regs)
    if self =~ /-?\d+/
      return self.to_i
    else
      return regs[self]
    end
  end
end

def run_program(regs, ip, my_queue, other_queue)
  sent = 0
  cycles=0
  while true
    instr, args = $program[ip]
    case instr 
    when 'snd'
      other_queue << (args[0].arg_to_i(regs))
      sent+=1
    when 'set'
      regs[args[0]]=args[1].arg_to_i(regs)
    when 'add'
      regs[args[0]]+=args[1].arg_to_i(regs)
    when 'mul'
      regs[args[0]]*=args[1].arg_to_i(regs)
    when 'mod'
      regs[args[0]]%=args[1].arg_to_i(regs)
    when 'rcv'
      if !my_queue.empty?
        regs[args[0]]=my_queue.shift
      else
        return ip, sent, cycles==0
      end
    when 'jgz'
      if args[0].arg_to_i(regs)>0
        ip+=args[1].arg_to_i(regs)
        next
      end
    end
    ip+=1
    cycles+=1
  end
  return ip, sent, cycles==0
end


ip = [0, 0]
regs = [{}, {}]
regs[0].default=0
regs[1].default=0
regs[0]['p']=0
regs[1]['p']=1
queues=[[],[]]

sent=0
require 'logger'
log = Logger.new(STDOUT)
while true
  log.info "program 0: #{regs[0]}, q=#{queues[0].uniq}"
  ip[0],_,wait0=run_program(regs[0], ip[0], queues[0], queues[1])
  log.info "program 1: #{regs[1]}, q=#{queues[1].uniq}"
  ip[1],s,wait1=run_program(regs[1], ip[1], queues[1], queues[0])
  sent+=s
  break if wait0 and wait1
end

puts "Day 2: #{sent}"


# WA: 457
