require 'scanf'
ranges={}


while line=gets
	a,b=line.scanf("%d: %d")
	ranges[a]=b	
end


i=0
while true
	match=true
	ranges.each{|k, v|
		x=2*(v-1)
		r=(i+k)%x
		# puts "i=#{i} depth=#{k} range=#{v} period=#{x} formula=#{r}"
		match=false if r == 0
		break if !match
	}	
	#puts ""
	if match
		puts "Part #2=#{i}"
		return
	end
	i+=1
end