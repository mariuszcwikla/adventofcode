. ../../assert.sh -v

#assert "ruby 11.rb < 11.in"               "-416, -626. Solution for part #1: 824"
assert "echo se | ruby 11.rb | head -n1"             "1, 0. Solution for part #1: 1"
assert "echo se,se | ruby 11.rb | head -n1"          "2, -1. Solution for part #1: 2"
assert "echo se,se,s | ruby 11.rb | head -n1"        "2, -2. Solution for part #1: 3"
assert "echo sw | ruby 11.rb | head -n1"             "-1, 0. Solution for part #1: 1"
assert "echo sw,sw | ruby 11.rb | head -n1"          "-2, -1. Solution for part #1: 2"
assert "echo sw,sw,s | ruby 11.rb | head -n1"        "-2, -2. Solution for part #1: 3"
assert "echo ne | ruby 11.rb | head -n1"             "1, 1. Solution for part #1: 1"
assert "echo ne,n | ruby 11.rb | head -n1"           "1, 2. Solution for part #1: 2"
assert "echo ne,ne | ruby 11.rb | head -n1"          "2, 1. Solution for part #1: 2"
assert "echo ne,ne,n | ruby 11.rb | head -n1"        "2, 2. Solution for part #1: 3"
assert "echo nw,n | ruby 11.rb | head -n1"           "-1, 2. Solution for part #1: 2"
assert "echo nw,nw | ruby 11.rb | head -n1"          "-2, 1. Solution for part #1: 2"
assert "echo nw,nw,s | ruby 11.rb | head -n1"        "-2, 0. Solution for part #1: 2"
assert "echo nw,nw,n | ruby 11.rb | head -n1"        "-2, 2. Solution for part #1: 3"
assert "echo nw,sw | ruby 11.rb | head -n1"          "-2, 0. Solution for part #1: 2"
assert "echo nw,sw,nw | ruby 11.rb | head -n1"          "-3, 1. Solution for part #1: 3"
assert "echo nw,sw,nw,sw | ruby 11.rb | head -n1"          "-4, 0. Solution for part #1: 4"



assert "echo sw,sw,nw | ruby 11.rb | head -n1"          "-3, 0. Solution for part #1: 3"
assert "echo sw,sw,sw | ruby 11.rb | head -n1"          "-3, -1. Solution for part #1: 3"
assert "echo sw,sw,sw,s | ruby 11.rb | head -n1"        "-3, -2. Solution for part #1: 4"

assert_end "Advent of code 2017/11"
