
# przeczytaj po zrobieniu zadania https://www.redblobgames.com/grids/hexagons/


def hex_distance(x, y)
  if y>=0
      v = x.abs + y.abs/2
  else
      v = y.abs + (x.abs+1)/2
  end
end

dirs=gets.strip.split ','
x,y=0,0
maxdist=0
dirs.each{|dir|
  case dir
  when 'n' then y+=1
  when 'ne' 
    y+=1 if x%2==0
    x+=1
  when 'se'
    y-=1 if x%2==1
    x+=1
  when 'nw' 
    y+=1 if x%2==0
    x-=1
  when 'sw' 
    y-=1 if x%2==1
    x-=1
  when 's' then y-=1
  end
  maxdist=[maxdist, hex_distance(x,y)].max
}

puts "#{x}, #{y}. Solution for part #1: #{hex_distance(x,y)}"
puts "Part #2: #{maxdist}"

# solution - wrong aswer: 651
# 626 - too low
# 627 - WA
# 834 - ACCEPTED

# invalid test cases:
# se,se,s - should be 3, but got 2
# se,s - should be 2, but got 1
