registers = {}
registers.default = 0

$max_value_ever=0

def evaluateCondition(registers, r, o, v)
	r = registers[r]
	case o 
		when '==' then r==v
		when '>=' then r>=v
		when '<=' then r<=v
		when '>' then r>v
		when '<' then r<v
		when '!=' then r!=v
		else raise 'unknown operator ' + o
	end
end

def evaluateStatement(registers, r, instr, v)
	case instr
		when 'inc' then registers[r]+=v
		when 'dec' then registers[r]-=v
		else raise 'unknown instruction: ' + instr
	end
	$max_value_ever=[registers[r], $max_value_ever].max
end

while line=gets
	register, instr, v, _, register2, operator, v2 = line.split
	if evaluateCondition(registers, register2, operator, v2.to_i)
		evaluateStatement(registers, register, instr, v.to_i)
	end
end

puts "Part 1: #{registers.values.max}"
puts "Part 2: #{$max_value_ever}"