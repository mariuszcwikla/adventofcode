vals = gets.split(',').map &:to_i
list = *(0..255)
skip=0
pos =0
vals.each{|length|
  0.upto(length/2-1){|i|
    a=(pos+i) % list.size
    b=(pos+length-i-1) % list.size
    #puts "swap #{list[a]} <=> #{list[b]}"
    list[a],list[b]=list[b],list[a]
  }
  pos=(pos + length + skip) % list.size
  skip+=1

  #puts "#{list.join ' '}, skip=#{skip}, pos=#{pos} [#{list[pos]}]"
}

puts list[0]*list[1]

#first answer: 27722
