CLEAN=0
WEAKENED=1
INFECTED=2
FLAGGED=3

lines=[]
while line=gets
    lines << line.strip
end

x=lines[0].size/2
y=lines.size/2

map={}
lines.each_with_index do |line, i|
    for j in 0..(line.length-1)    
        if line[j]=='#'
            state=INFECTED
        else
            state=CLEAN
        end
        map[[j,i]]=state
    end
end

LEFT=0
UP=1
RIGHT=2
DOWN=3

def do_step(map, x, y, dir)
    c = map[[x,y]] || CLEAN    
    
    case c
        when CLEAN
            dir=(dir-1)%4
        when INFECTED
            dir=(dir+1)%4
        when FLAGGED
            dir=(dir+2)%4            
    end
    
    
    
    c=(c+1)%4
    map[[x,y]]=c    
    
    infected=false
    infected=true if c==INFECTED
    
    case dir 
        when LEFT then x-=1 
        when RIGHT then x+=1
        when UP then y-=1 
        else y+=1
    end
    
    [x,y,dir,infected]
end


dir=UP

def dump(map, x, y)
    x1,x2=map.keys.map{|xx,yy| xx}.minmax
    y1,y2=map.keys.map{|xx,yy| yy}.minmax
    x1,x2=[x1,x, x2].minmax
    y1,y2=[y1,y, y2].minmax
    
    for yy in y1..y2
        for xx in x1..x2
            c = map[[xx,yy]] || CLEAN
            c = case c 
                when INFECTED then '#'
                when FLAGGED then 'F'
                when WEAKENED then 'W'
                else '.'
                end
                
            if x==xx and y==yy
                print "[#{c}]"
            else
                print " #{c} "
            end
        end
        puts ""
    end
end

    
#N=70
N=10_000_000
#N=100

total_infected=0
N.times do |i|
    if false
        puts "Iteration #{i}"
        dump(map, x,y)    
    end
    
    x,y,dir,infected=do_step(map, x, y, dir)    
    
    total_infected+=1 if infected
    
    puts i if i%100_000==0
end

puts "Part #2: #{total_infected}"

#Answers:
#2511944 - too high
