input = gets.to_i

# k=Math.sqrt(input).to_i
#
# k-=1 if k%2==0
#  
# if k**2+k+1 >= input
#   #up
#   x, y=k, k**2+k+1-input-(k-1)
# end
#

#puts x, y

def run(input)
  h={}
  h.default=0
  h[[0,0]]=1
  
  x,y,i=0,0,1
  k=1

  def update(h, x, y)
    h[[x,y]]=[[0, 1],
     [1, 1],
     [1, 0],
     [-1, 1],
     [-1, 0],
     [-1, -1],
     [0, -1],
     [1, -1]].map{|a,b| h[[x+a, y+b]]}.sum
  end

  while true 
    1.upto(k){x+=1; i+=1; update(h, x, y); return [x,y,h] if i==input}
    1.upto(k){y+=1; i+=1; update(h, x, y); return [x,y,h] if i==input}
    1.upto(k+1){x-=1; i+=1; update(h, x, y); return [x,y,h] if i==input}
    1.upto(k+1){y-=1; i+=1; update(h, x, y); return [x,y,h] if i==input}

    k+=2
  end

end
x,y,h=run(input)
puts "#{x}, #{y}, solution to part#1=#{x.abs+y.abs}"

x=h.values.filter{|v|v>input}.min

puts "part #2=#{x}"
#puts h
