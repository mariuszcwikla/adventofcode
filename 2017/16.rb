require 'scanf'
programs = *('a'..'p')
gets.strip.split(',').each{|move|
  if move[0]=='s'
    x = move[1..move.size-1].to_i
    programs = programs[programs.size-x..(programs.size-1)] + programs[0..programs.size-1-x]
  elsif move[0]=='x'
    a, b = move.scanf("x%d/%d")
    programs[a], programs[b]=programs[b],programs[a]
  elsif move[0]=='p'
    a, b = move.scanf("p%c/%c")
    a = programs.index(a)
    b = programs.index(b)
    programs[a], programs[b]=programs[b],programs[a]
  end
}
puts programs.join ''
