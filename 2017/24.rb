require 'scanf'

starters = []
components = Hash.new{|h, k| h[k]=[]}

while line=gets
	a,b=line.scanf("%d/%d")
	if a==0
		starters << [a,b]
	elsif b==0
		starters << [b,a]
	else
		components[a] << b
		components[b] << a
	end
end

def maximize(b, components, bridge)
	list = components[b].dup
	max = nil
	maxbridge = nil
	
	list.each{|x|
		components[b].delete x
		components[x].delete b
		
		bridge << [b,x]
		
		mm, mb = maximize(x, components, bridge)
		if mm!=nil
			if max==nil or mm > max
				max = mm
				maxbridge = mb
			end
		end
		
		bridge.pop
		
		components[b] << x
		components[x] << b
	}
	if max==nil
		return bridge.flatten.sum, bridge.dup
	end
	
	return max, maxbridge	
end

max = nil
for a,b in starters
	bridge=[[a,b]]
	m, bridge = maximize(b, components, bridge)
	if max==nil or m > max
		max = m
	end
	puts "#{a}/#{b}: strength=#{m} #{bridge}"
end

#Answers
#1854 - too low
#1877 - too high
#1868 - ok