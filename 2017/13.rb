require 'scanf'
ranges={}


while line=gets
	a,b=line.scanf("%d: %d")
	ranges[a]=b	
end

def init(ranges)
	scanner={}
	dir={}
	ranges.each{|k, v|
		scanner[k]=0
		dir[k]=:down
	}
	return [scanner, dir]
end


def doSimulation(ranges, scanner, dir)
	scanner.each{|k,v|
		if dir[k]==:down
			if v==ranges[k]-1
				v-=1
				dir[k]=:up
			else
				v+=1
			end
		else
			if v==0
				v+=1
				dir[k]=:down
			else
				v-=1
			end
		end
		scanner[k]=v
	}
end

scanner, dir=init(ranges)
n = ranges.keys.max
severity = 0
0.upto(n){|i|
	if scanner[i]==0	
		severity += i * ranges[i]		
	end
	doSimulation(ranges, scanner, dir)
}

puts "Part #1: #{severity}"


scanner, dir=init(ranges)
sleep = 0
while true
	tscanner = scanner.dup
	tdir = dir.dup
	caught=false
	0.upto(n){|i|
		caught = true if tscanner[i]==0
		break if caught
		doSimulation(ranges, tscanner, tdir)
	}
	
	if !caught
		puts "Part #2: #{sleep}"
		return
	end
	sleep+=1
	doSimulation(ranges, scanner, dir)
	puts "sleep=#{sleep}" if sleep%10==0
end

# Answer 3876272