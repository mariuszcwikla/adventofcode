class Node
  attr_reader :value
  attr_accessor :parent
  def initialize(value)
    @value=value
    @parent = nil
  end
  def root
    n = self
    while n.parent!=nil
      n=n.parent
    end
    return n
  end
end

trees=Hash.new {|h, k|
  h[k]=Node.new(k)
}

while line=gets
  a, _, *right=line.split
  a=trees[a.to_i].root  
  right.each{|x| 
    x=x.to_i
    x=trees[x].root
    x.parent=a if x!=a
  }  
end

z = trees[0].root.value
n = trees.values.filter{|n| n.root.value==z}.count
puts "Part #1: #{n}"

n = trees.values.map{|n| n.root.value}.uniq.size
puts "Part #2: #{n}"
