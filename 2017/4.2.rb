require 'set'
invalid = 0
total=0
while line=gets
  s = Set.new  
  total+=1
  line.split.each{|word|
    sorted=word.chars.sort.join ""
    if s.include?(sorted)
      invalid+=1
      puts "invalid: #{word}"
      break
    end
    s << sorted    
  }
end
puts "Part #2=#{total-invalid} (total=#{total}, invalid=#{invalid}"

