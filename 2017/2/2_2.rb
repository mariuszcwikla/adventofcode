checksum=0
while line=gets
  numbers = line.split.map{|i|i.to_i}
  next if numbers.size==0

  for i in 0..numbers.size-2
    for j in (i+1)..numbers.size-1
      a,b=numbers[i], numbers[j]
      a,b=b,a if a>b

      if b%a==0
        checksum+=(b/a)
      end
    end
  end
end
puts checksum

