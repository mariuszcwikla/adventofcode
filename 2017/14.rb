def hash_block(vals, list, pos, skip)

  vals.each{|length|
    0.upto(length/2-1){|i|
      a=(pos+i) % list.size
      b=(pos+length-i-1) % list.size
      list[a],list[b]=list[b],list[a]
    }
    pos=(pos + length + skip) % list.size
    skip+=1

  }
  [pos, skip]
end

# Dziwne - gdy funkcja nazywala sie "hash"
# to wywalalo bledem
# 14.rb:16:in `hash': wrong number of arguments (given 0, expected 1)

def hash_string(string)
  vals = string.bytes.map{|c| c.ord}
  list = *(0..255)
  skip=0
  pos =0


  vals+=[17, 31, 73, 47, 23]
  64.times{
    pos, skip=hash_block(vals, list, pos, skip)
  }

  out = ""
  16.times{|i|
    v = 0
    16.times{|j|
      v^=list[i*16+j]
    }
    v=(v.to_s(16))
    v = '0' + v if v.length==1
    out << v
  }

  return out
end

map=[]

line = gets.strip
squares=0
0.upto(127){|i|
  h = hash_string("#{line}-#{i}")
  s=h.to_i(16).to_s(2).rjust(128, '0')
  #puts s.gsub('0', '.').gsub('1', '#')
  squares+=s.count('1')
  #map<<s.gsub('0', '.').gsub
  map<<s
}
puts "Part 1: #{squares}"

visited={}
groups=0
for y in 0..127
  for x in 0..127
    if map[y][x]=='1' and !visited[[x,y]]
      q = [[x,y]]
      groups+=1
      while !q.empty?
        xx,yy=q.shift
        [[-1,0],
         [ 1,0],
         [ 0,-1],
         [ 0,1]].each{|dx,dy|
           nx, ny = xx+dx, yy+dy
           if !visited[[nx,ny]] and 
               nx>=0 and nx<=127 and ny>=0 and ny<=127 and 
               map[ny][nx]=='1'
            visited[[nx,ny]]=true
            q << [nx,ny]
           end
         }
      end
    end
  end
end


puts "Part 2: #{groups}"
