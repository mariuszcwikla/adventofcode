class SolutionFound < Exception
end

class Node
	attr_accessor :parent, :name, :weight, :children
	def initialize(name, weight)
		@children=[]
		@name = name
		@weight = weight
	end	
	def calculate_weight
		w = @weight
		weights = @children.map{|c| c.calculate_weight}
        if weights.uniq.size==2
            a,b=weights.uniq.sort
            torebalance = @children.select{|c| c.calculate_weight==b}[0]
            puts "Part #2: #{torebalance.name} (#{torebalance.weight}), answer=#{torebalance.weight-(b-a)}"            
            throw :solution_found
        end
        w+=weights.sum
		return w
	end
end

nodes = {}

require 'scanf'
lines = []
while line=gets	
	lines << line
	elements = line.split
	parent=elements[0]
	n = Node.new(elements[0], elements[1].scanf("(%d)")[0])
	nodes[elements[0]]=n
end
	
	
lines.each do |line|
	elements = line.split
	
	parent = nodes[elements[0]]
	if elements.size>2
		3.upto(elements.size-1){|i|
			x = elements[i].delete ","
			x = nodes[x]
			
			parent.children << x
			x.parent = parent
		}
	end
end

n = nodes.first[1]
while n.parent!=nil
	n = n.parent
end
puts "Part #1: #{n.name}"

catch(:solution_found){n.calculate_weight}
