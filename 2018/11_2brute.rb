#brute force algorithm for part #2

def power_level(x, y, serial_num)
	r = x + 10
	v = (r*y+serial_num)*r
	v /= 100
	v = v % 10
	return v - 5
end
	

def total_power(x, y, serial_num, size)
	sum = 0
	for i in 1..size
		for j in 1..size
			sum+=power_level(x+i, y+j, serial_num)
		end
	end
	return sum
end

max = nil

serial_num=8444
#serial_num=42

max = nil
for size in 1..300
	puts "size=#{size}"
	max_current = nil
	for y in 1..(300-size)
		for x in 1..(300-size)
			s = total_power(x, y, serial_num, size)
			if max==nil or max[2] < s
				max = [x, y, s]
			end
			if max_current == nil or max_current[2] < s
				max_current = [x, y, s]
			end
		end
	end
	puts "p=#{max_current[2]}, [#{max_current[0]}, #{max_current[1]}]"
end
puts "#{max[0]+1},#{max[1]+1}"