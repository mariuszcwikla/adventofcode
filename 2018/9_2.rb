class Node
	def initialize(value)
		@value = value
		@prev = nil
		@next = nil
	end
	
	attr_accessor :next, :prev, :value
	
	def go_left(n)
		node = self
		n.times{ node = node.prev}
		return node
	end
	def go_right(n)
		node = self
		n.times{ node = node.next}
		return node
	end
	
	def remove()
		@prev.next = @next
		@next.prev = @prev		
	end
	
	def add(v)
		n = Node.new(v)				
		n.prev = @prev
		n.next = self
		@prev.next=n
		@prev = n
		return n
	end
	
	def dump
		s = ""
		n = self
		begin
			s<< "#{n.value},"
			n = n.next
		end while n!=self
		return s
	end
end

def play_game(num_of_players, points)		
	player_points = Array.new(num_of_players){0}
	node = Node.new(0)
	node.next = node
	node.prev = node
		
	zero = node
	current_player = 0
	for i in 1..points	
		puts "#{i}/#{points} iterations" if i%100000==0
		#puts "#{i} #{zero.dump} (#{node.value})"				
		if i%23 == 0 
			p = i
			m_left = node.go_left(7)
			p += m_left.value
			player_points[current_player] += p
			node = m_left.next
			m_left.remove()			
		else
			node = node.go_right(2)
			node = node.add(i)					
		end
		current_player += 1
		current_player = 0 if current_player == num_of_players
	end
	
	return player_points.max	
end

while line=gets
	e = line.split
	n, p = e[0].to_i, e[6].to_i
	score = play_game(n, p)
	puts score
	puts "100x = #{play_game(n, p*100)}"	
end

