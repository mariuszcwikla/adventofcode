
require 'optparse'




$options = { :simulation => false, :draw => false}

OptionParser.new do |opts|
  opts.banner = "Usage: 13.rb [options]"

  opts.on("-mMODE", "--mode=MODE", [:s, :d, :simulation, :draw, :sim],
                                   %(Run using given mode, where MODE=[s|d|sim|simulation|draw]
                                     If no mode is specified - prints only final results.
                                     If mode=s - simulation mode/displays animation
                                     If mode=d - draw - draws each frame independently) )do |v|    
									
	if [:s, :simulation, :sim].include? v
		$options[:simulation] = true
		$options[:draw] = true
	elsif [:d, :draw].include? v
		$options[:draw] = true
	else
		puts "Unsupported mode: #{v}"
		return
	end
  end
  
  opts.separator <<~EOS
                   Examples:

                     13.rb < 13.test.in
                     13.rb -md < 13.test.in
                     13.rb -ms < 13.test.in
				   EOS
end.parse!



LEFT = '<'
RIGHT = '>'
UP = '^'
DOWN = 'v'

DIRS = [LEFT, UP, RIGHT, DOWN]

TRACKS = ['-', '|', '/', '\\', '+']

C_LEFT = 0
C_STRAIGHT = 1
C_RIGHT = 2

class Cart
	def initialize(x, y, dir)
		@x = x
		@y = y
		@dir = dir
		@crossing_dir = C_LEFT
	end
	
	attr_reader :x, :y, :dir
	
	def move(map)
		@x+=1 if @dir==RIGHT
		@x-=1 if @dir==LEFT
		@y+=1 if @dir==DOWN
		@y-=1 if @dir==UP
		
		if map[y][x]=='/'			
			@dir = case 
				when @dir==RIGHT then UP
				when @dir==LEFT then DOWN
				when @dir==UP then RIGHT
				else LEFT
			end
		elsif map[y][x]=='\\'
			@dir = case 
				when @dir==RIGHT then DOWN
				when @dir==LEFT then UP
				when @dir==UP then LEFT
				else RIGHT
			end
		elsif map[y][x]=='+'
			cur_idx = DIRS.find_index(@dir)
			if @crossing_dir==C_LEFT
				cur_idx -=1
			elsif @crossing_dir==C_RIGHT
				cur_idx +=1
			end
			cur_idx %= DIRS.size
			
			@dir = DIRS[cur_idx]
			
			@crossing_dir = (@crossing_dir + 1) % 3
		end
	end
end

def get_tracks_around(x, y, map, width)
	tracks = ''
		
	tracks << (x==0          ? ' ' : map[y][x-1])
	tracks << (x==width-1    ? ' ' : map[y][x+1])
	tracks << (y==0          ? ' ' : map[y-1][x])
	tracks << (y==map.size-1 ? ' ' : map[y+1][x])
	
	return tracks
end


def is_track(x)
	return DIRS.include? x
end

HORIZONTAL_TRACKS = '-/\\+'
VERTICAL_TRACKS = '|/\\+'

def determine_track_type(around, x, y)
	
	def is_horizontal(around)
		return true if HORIZONTAL_TRACKS.include?(around[0]) and HORIZONTAL_TRACKS.include?(around[1])
		return false
	end
	
	def is_vertical(around)
		return true if VERTICAL_TRACKS.include?(around[2]) and VERTICAL_TRACKS.include?(around[3])
		return false
	end
	
	if is_horizontal(around) and is_vertical(around)
		return '+'
	elsif is_horizontal(around)
		return '-'
	elsif is_vertical(around)
		return '|'
	else
		puts "unknown at #{x}, #{y}"
		return 'x'				#corner! TODO
	end
end

def extract_carts(map, width)
	carts = []
#sanitize input - remove carts from map
	for y in 0..(map.size-1)
		line = map[y]
		for x in 0..(line.size-1)			
			s = line[x]
			if DIRS.include? s
				carts << Cart.new(x, y, s)
				#ASSUMPTION: I am assuming that two carts can't be side-by-side in input data, e.g    ->>- or -<>- is not possible
				#is intersection						
				around = get_tracks_around(x, y, map, width)			
				t = determine_track_type(around, x, y)
				line[x]=t		
			end					
		end
	end
	return carts
end


map = []

width = 0 
while line = gets
	map << line
	width = [line.size, width].max
end


carts = extract_carts(map, width)


def simulation_step(map, carts)
	
	carts.sort!{|a, b|
		r = a.y<=>b.y
		if r == 0
			a.x <=> b.x
		else
			r
		end
	}
		

    allcrashes = []	
	carts.each_with_index{|c, i|
		c.move(map)
		
		crashed = carts.select{|c2| c!=c2 and c2.x==c.x and c2.y==c.y}
        if crashed.size>0
            allcrashes += crashed
            allcrashes << c
        end
         
	}

    allcrashes.each{|c| carts.delete c}

	return allcrashes
end


def draw(map, carts)    
	map.each_with_index{|line, y|
		drawn = line.clone
		carts.select{|c| c.y==y}.each{|c| drawn[c.x]=c.dir}
		puts drawn
	}
    puts ""
end

i = 0


def cls	
	puts "\e[H\e[2J"
end

	

draw(map, carts) if $options[:draw]

while true
	i+=1
	crash = simulation_step(map, carts)
    cls               if $options[:simulation]
	draw(map, carts)  if $options[:draw]

    if crash.size>0
		puts "iterations #{i}: #{crash[0].x},#{crash[1].y}"
		break
	end
	
	sleep 0.3 	      if $options[:simulation]
end

puts "Part #2"
while carts.size>1
    simulation_step(map, carts)
    cls if $options[:simulation]
    draw(map, carts) if $options[:draw]
    sleep 0.3 if $options[:simulation]
end

if carts.size>0
	puts "#{carts[0].x},#{carts[0].y}"
else
	puts "No carts left"
end
