
def power_level(x, y, serial_num)
	r = x + 10
	v = (r*y+serial_num)*r
	v /= 100
	v = v % 10
	return v - 5
end

#puts power_level(122,79,57)
#puts power_level(217,196,39)
#puts power_level(101,153,71)


def total_power(x, y, serial_num)
	sum = 0
	for i in 1..3
		for j in 1..3	
			sum+=power_level(x+i, y+j, serial_num)
		end
	end
	return sum
end

max = nil

serial_num=8444
#serial_num=42

for y in 1..(300-3)
	for x in 1..(300-3)
		s = total_power(x, y, serial_num)
		if max==nil or max[2] < s
			max = [x, y, s]
		end
	end
end

puts "#{max[0]+1},#{max[1]+1}"