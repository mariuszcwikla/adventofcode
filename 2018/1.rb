require 'set'
sum = 0

freqs = []

while line = gets
	f = line.to_i
	sum += f
	freqs << f	
end
puts sum


#2nd part

visited = Set.new

sum = 0
solved = false
while !solved
	freqs.each do |x|
		if visited.include? sum
			puts sum
			solved = true
			break
		end
		visited << sum
		sum += x
	end
end