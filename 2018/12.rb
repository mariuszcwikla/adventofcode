require 'scanf'

initial = gets.scanf('initial state: %s')[0].split ""
gets

states = {}
states.default = '.'

while state = gets
	a, _, b = state.split
	#states << [a,b]
	states[a]=b
end

def doGeneration(current, states)
	nextgen = []
	sample = "." * 5		
	
	current.each_with_index{|x, i|
		for k in 0..4			
			if i+k-2 < 0 or i+k-2 >=current.size
				sample[k]='.'
			else
				sample[k] = current[i+k-2]
			end
		end
		n = states[sample]
		#n = '.' if n==nil
		nextgen << n
	}
	
	left    ="..." + (current[0..1].join "")
	leftleft="...." + current[1]
		
	nextgen.unshift states[left]
	nextgen.unshift states[leftleft]		
	
	right     = (current[-2..-1].join "") + "..."
	rightright= current[-1] + "...."
		
	nextgen<<states[right]
	nextgen<<states[rightright]			
	
	return nextgen
end

gen = initial

lines = [(gen.join "")]

#puts gen.join ""
maxlen = 0
(0..19).each{
	gen = doGeneration(gen, states)
	lines << (gen.join "")
	maxlen = [gen.size, maxlen].max
}

lines.each{|line|
	t = ("." * ((maxlen - line.size)/2))
	puts t + line + t
}

sum = 0
gen.each_with_index{|x, i|
	sum += (i-20*2) if x=='#'
}
puts sum