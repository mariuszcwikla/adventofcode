

geol = {}
geol[[0,0]]=0


depth = 510

target = [10, 10]

require 'scanf'
depth, _ = STDIN.gets.scanf 'depth: %d'
target = STDIN.gets.scanf 'target: %d,%d'

width = target[0]
height = target[1]

width, height = ARGV[0], ARGV[1] if ARGV.size==2
	

map = Array.new(height+1){Array.new(width+1)}

1.upto(width){|x|
	geol[[x, 0]]=x*16807
}

1.upto(height){|y|
	geol[[0, y]]=y*48271
	1.upto(width){|x|
		if target==[x,y]
			geol[[x,y]]=0
		else
			lvl1 = (geol[[x-1,y]] + depth) % 20183
			lvl2 = (geol[[x,y-1]] + depth) % 20183
			geol[[x,y]]=lvl1 * lvl2 
		end
	}	
}

ROCK = '.'
WET = '='
NARROW = '|'

regions = {
	0 => ROCK,
	1 => WET,
	2 => NARROW
}
sum = 0
0.upto(height){|y|
	0.upto(width){|x|
		lvl = (geol[[x,y]]+depth) % 20183
		#lvl = geol[[x, y]] % 3
		lvl %= 3
		sum += lvl
		map[y][x]=regions[lvl]
	}
}

map[0][0]='M'
map[target[1]][target[0]]='T'

def draw(map)
	map.each{|row| puts row.join ""}
end

draw(map)

puts "Part #1: #{sum}"




#Part 2

gears_by_tile = {
	ROCK   => [:climbing, :torch],
	WET    => [:climbing, :neither],
	NARROW => [:torch, :neither]
}


q = []
q << [0,0,:torch,0]


visited = {}
visited[ [0, 0] ] = [0, :torch]

puts "Part #2"

steps = 0
while !q.empty?
	x, y, gear, t = q.shift
	steps+=1
	if steps%100000==0
		puts "#{x}, #{y} -> #{target.join ', '}, step=#{steps}"
	end
	
	[[-1, 0],
	 [ 1, 0],
	 [ 0, -1],
	 [ 0, 1]].each{|dx, dy|
		nx, ny = x+dx, y+dy
		if nx >=0 and nx <= width and ny >=0 and ny<=height
			
			tile = map[ny][nx]
			next if tile=='M'
			if tile=='T'
				p = visited[ [nx, ny, :torch ] ]
				t2 = t + 1				
				t2 += 7 if gear!= :torch
				if p==nil or t2 < p[0]					
					visited[[nx, ny, :torch]]=[t2, :torch, x, y]
				end
			
			else
				#puts "nil: #{tile}" if gears_by_tile[tile]==nil
				gears_by_tile[tile].each{|new_gear|
					p = visited[ [nx, ny, new_gear ] ]
					t2 = t + 1
					t2 += 7 if gear!=new_gear
					if p==nil or t2 < p[0]
						# if t2==p[0] -> should be put into queue as well
						# but what to do with path?
						q << [nx, ny, new_gear, t2]
						visited[[nx, ny, new_gear]]=[t2, new_gear, x, y]
					end				
				}
			end
		end
	}
	#puts visited[[x, y]] if map[y][x]=='T'
end
puts "t, gear, prev_x, prev_y at Target = " + (visited[[target[0], target[1], :torch]].join ", ")

#puts visited
pp = []
x, y = target
gear = :torch
while x!=0 or y!=0
	p = visited[[x, y, gear]]
	puts p
	pp << [x, y, p[1]]
	x, y, gear = p[2], p[3], p[1]
end
pp.reverse!
pp.each {|p| puts p.join " "}


puts "-----------"

[[0,1],
 [1,1],
 [2,1],
 [3,1]
].each{|x, y|
	puts visited[[x, y]].join " "
}