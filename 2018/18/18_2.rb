map = []

while line = gets
	map << line.strip
end

def count(map, x, y, c)
	n = 0
	width = map[0].size
	height = map.size
	[[-1, -1],
	 [-1, 0],
	 [-1, 1],
	 [0, -1],
	 [0, 1],
	 [1, -1],
	 [1, 0],
	 [1, 1]].each{|a, b| 
		next if y+a<0 or y+a>=height or x+b<0 or x+b>=width
		n+=1 if map[y+a][x+b]==c
	}
	return n
end

def transform(map, buf)	
	width = map[0].size
	height = map.size
	0.upto(height-1){|y|		
		0.upto(width-1){|x|
			ntrees = count(map, x, y, '|')
			nlumber = count(map, x, y, '#')
			buf[y][x]=map[y][x]
			if map[y][x]=='.'
				buf[y][x]='|' if ntrees >= 3
			end
			
			if map[y][x]=='|'
				buf[y][x]='#' if nlumber >= 3
			end
			if map[y][x]=='#'
				if ntrees>=1 and nlumber >=1
					buf[y][x]='#' 
				else
					buf[y][x]='.'
				end
			end
		}
	}
	return buf, map
end	

def draw(map)
	puts map
end



Dir.mkdir('tmp') unless File.exists?('tmp')
def write_map_to_file(i, map)
	
	f = File.open("tmp/%06d.txt" % i, "w")
	map.each{|line| 
		f<<line
		f<<"\n"
	}
	f.close
end

MAX=1000000000
#MAX=10
buf=[]
map.each{|line| buf<<line.clone}

hashes = {}
hashes[map.hash]=0

1.upto(MAX){|i|
	map, buf = transform(map, buf)		
	h = map.hash
	#write_map_to_file(i, map)
	if hashes[h]!=nil
		#possible cycle
		i0 = hashes[h]
		i1 = i		
		cycle = i1 - i0
				
		puts "Possible cycle detected at iterations #{i0} <-> #{i1}, cycle=#{cycle}"
		
		((MAX-i0)%cycle).times{
			map, buf = transform(map, buf)
									
		}
		ntrees, nlumber = 0,0
		map.each{|line|
			ntrees+=line.count('|')
			nlumber+=line.count('#')
		}
		puts "Part #2: #{ntrees}*#{nlumber}=#{ntrees * nlumber}"
		# 0 2 3 5 8 1 3 5 8 1 3 5 8 1....
		# i0=2
		# i1=6
		# cycle = 4
		# a[1000]=?
		
		# (N-i0)%cycle
		
		return
	end
	
	hashes[h]=i
}



