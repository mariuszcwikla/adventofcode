map = []

while line = gets
	map << line.strip
end

def count(map, x, y, c)
	n = 0
	width = map[0].size
	height = map.size
	[[-1, -1],
	 [-1, 0],
	 [-1, 1],
	 [0, -1],
	 [0, 1],
	 [1, -1],
	 [1, 0],
	 [1, 1]].each{|a, b| 
		next if y+a<0 or y+a>=height or x+b<0 or x+b>=width
		n+=1 if map[y+a][x+b]==c
	}
	return n
end

def transform(map)
	tr = []
	width = map[0].size
	height = map.size
	0.upto(height-1){|y|
		tr << map[y].clone
		0.upto(width-1){|x|
			ntrees = count(map, x, y, '|')
			nlumber = count(map, x, y, '#')
			tr[y][x]=map[y][x]
			if map[y][x]=='.'
				tr[y][x]='|' if ntrees >= 3
			end
			
			if map[y][x]=='|'
				tr[y][x]='#' if nlumber >= 3
			end
			if map[y][x]=='#'
				if ntrees>=1 and nlumber >=1
					tr[y][x]='#' 
				else
					tr[y][x]='.'
				end
			end
		}
	}
	return tr
end	

def draw(map)
	puts map
end

draw(map)
0.upto(9){|i|
	tr = transform(map)
	puts "After #{i+1} minutes"
	draw(tr)
	map = tr
}


ntrees, nlumber = 0,0
map.each{|line|
	ntrees+=line.count('|')
	nlumber+=line.count('#')
}

puts "Part #1: #{ntrees}*#{nlumber}=#{ntrees * nlumber}"