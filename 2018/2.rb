require 'set'

def count_letters(line)
	h = Hash.new
	line.each_char do |x|
		if h.include? x
			h[x]+=1
		else
			h[x]=1
		end
	end
	return h
end


def getCounts(h, n)
	cnt = []
	h.each do |x, c|
		if c==n
			cnt << x
		end
	end
	return cnt
end


twos=0
threes=0

lines = []

while line=gets	
	line.strip!
	lines << line	
	cnt = count_letters(line)
		
	twos+=1 unless getCounts(cnt, 2).empty?
	threes+=1 unless getCounts(cnt, 3).empty?
end

puts twos * threes



def diff_by_one(a,b)
	diff_at = nil
	for i in (0..a.length)
		if a[i]!=b[i]
			if diff_at!=nil
				return nil
			end
			diff_at = i
		end	
	end
	if diff_at == nil
		return nil
	end
	
	puts a
	puts b
	
	if diff_at==0
		return a[1..a.length]
	elif diff_at==a.length
		return a[0..a.length-1]
	else
		return a[0..diff_at-1] + a[diff_at+1..a.length]
	end
end

catch(:done) do
	for i in (0..lines.length-2)
		for j in (i+1..lines.length-1)
			x=diff_by_one(lines[i], lines[j])
			if x!=nil
				puts x
				throw :done
			end
		end
	end
end