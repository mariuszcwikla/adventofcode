require 'etc'
require 'thread/pool'

line = gets.strip



def isupcase(c)
	return true if c>='A' and c<='Z'
	return false
end

def ispolarity(a,b)
	if a.downcase==b.downcase
		if (isupcase(a) and !isupcase(b)) or
		   (isupcase(b) and !isupcase(a))		   
		   return true
		end
	end
	return false	
end

def react_polymer(line)
	changed = true
	while changed
		line2 = ""
		last = line[0]
		changed=false
		i = 0
		while i < line.length
			if i < line.length-1 and ispolarity(line[i], line[i+1])
				i += 2
				changed = true			
			else
				line2 << line[i]
				i+=1
			end		
		end
		#puts line2
		line = line2
	end
	return line
end

beg = Time.now
reacted = react_polymer(line)


puts reacted
puts reacted.length
puts "time: #{Time.now-beg}"

#TODO: refactor to use parallel processing

pool = Thread.pool(Etc.nprocessors)
minlength = line.length
('a'..'z').each {|c| 	
    pool.process {
        beg = Time.now
		puts "started: #{c}"
		STDOUT.flush
        shortened = line.gsub(c, '').gsub(c.upcase, '')
        shortened = react_polymer(shortened)
        minlength = shortened.length if shortened.length < minlength	
        puts "#{c}=#{shortened.length} (#{Time.now-beg} seconds)"
		STDOUT.flush
    }
}
pool.shutdown
puts minlength
