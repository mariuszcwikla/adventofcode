require 'scanf'



ops = {}
ops[:addr] = lambda {|r, a, b, c| r[c]=r[a]+r[b]}
ops[:addi] = lambda {|r, a, b, c| r[c]=r[a]+b}

ops[:mulr] = lambda {|r, a, b, c| r[c]=r[a]*r[b]}
ops[:muli] = lambda {|r, a, b, c| r[c]=r[a]*b}

ops[:banr] = lambda {|r, a, b, c| r[c]=r[a] & r[b]}
ops[:bani] = lambda {|r, a, b, c| r[c]=r[a] & b}

ops[:borr] = lambda {|r, a, b, c| r[c]=r[a] | r[b]}
ops[:bori] = lambda {|r, a, b, c| r[c]=r[a] | b}

ops[:setr] = lambda {|r, a, b, c| r[c]=r[a]}
ops[:seti] = lambda {|r, a, b, c| r[c]=a}

ops[:gtir] = lambda {|r, a, b, c| r[c]= (a > r[b] ? 1 : 0)}
ops[:gtri] = lambda {|r, a, b, c| r[c]= (r[a] > b ? 1 : 0)}
ops[:gtrr] = lambda {|r, a, b, c| r[c]= (r[a] > r[b] ? 1 : 0)}

ops[:eqir] = lambda {|r, a, b, c| r[c]= (a == r[b] ? 1 : 0)}
ops[:eqri] = lambda {|r, a, b, c| r[c]= (r[a] == b ? 1 : 0)}
ops[:eqrr] = lambda {|r, a, b, c| r[c]= (r[a] == r[b] ? 1 : 0)}


program = []

line = gets
ip_reg, _ = line.scanf("#ip %d")
ip = 0
reg = [0]*6

while line=gets	
	i, a, b, c = line.scanf("%s%d%d%d")
	program << [i,a,b,c]
end

loop{|i|
	before = reg.clone
	
	#reg[ip_reg]=ip
	instr_idx = reg[ip_reg]
	if instr_idx >= program.size
		puts "Part #1: #{reg[0]}"
		break
	end
	instr = program[instr_idx]
	opcode = instr[0].to_sym	
	if opcode==:addi
		
	end
    ops[opcode].call(reg, instr[1], instr[2], instr[3])
	ip = reg[ip_reg]
	
	#puts (before.join " ") + " " + opcode.to_s + " " + (reg.join " ")
	
	ip +=1
	reg[ip_reg]=ip
}

