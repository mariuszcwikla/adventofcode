#Brute force gave correct answer!


require 'scanf'
require 'set'
$graph = {}

$incoming = Set.new

incoming = Set.new

while line = gets
	a,b = line.scanf("Step %s must be finished before step %s can begin.")	
	$graph[a]=[] if not $graph.has_key?(a)
	$graph[b]=[] if not $graph.has_key?(b)
	$graph[a] << b 
	$incoming << b
end

def is_root(v)
	$graph.each{|v2,e|
		next if v==v2
		return false if e.include? v
	}
	return true
end

def get_roots()	
	roots = []
	
	$graph.select{|v, e| is_root(v)}
end

result = ""

while !(roots=get_roots()).empty?
	x = roots.sort.first[0]
	result << x
	$graph.delete x
end

puts result

