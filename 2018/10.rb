require 'scanf'

stars = []

while line=gets
	x, y, vx, vy = line.scanf("position=<%d,  %d> velocity=<%d,  %d>")
	stars << [x, y, vx, vy]
end

def move(stars, speed)	
	stars.each{|s|
		s[0]+=s[2]*speed
		s[1]+=s[3]*speed
	}
end

def draw(i, stars)
	Dir.mkdir('Day10') unless Dir.exists? ('Day10')
	commands = "$MYDATA << EOD\n"
	stars.each{|s| commands << "#{s[0]}, #{s[1]}\n"}
	commands << "EOD\n"
	commands << "unset tics; unset border; unset key\n"
	commands << "set term png\n"
	commands << ("set output 'Day10/%05d.png'\n" % i)
	commands << "plot $MYDATA using 1:2 with points pt 3\n"
		
	IO.popen("gnuplot", "w") { |io| io.puts commands }
end

# by trial and error I discovered that at time=10086 stars represent characters
START_WITH = 1000*10

move(stars, START_WITH)

for i in 0..1000
	puts "Iteration #{i}"
	a, b = stars.minmax_by{|e|e[0]}
	c, d = stars.minmax_by{|e|e[1]}
	
	x1, x2, y1, y2 = a[0], b[0], c[1], d[1]
	
	#printf "#{b[0]-a[0]}, #{d[1]-c[1]} "
	draw(i, stars)
	move(stars, 1)
end