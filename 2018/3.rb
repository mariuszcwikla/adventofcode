require 'scanf'

N = 1000

map = Array.new(N){Array.new(N){0}}

data = []

while line=gets
    n, x, y, w, h = line.scanf('#%d @ %d,%d: %dx%d')
	data << [n, x, y, w, h]
    for i in (x..x+w-1)
        for j in (y..y+h-1)
            c = map[i][j]
            if c==0
                map[i][j]=n
            else
                map[i][j]=-1
            end
        end
    end
    
    
end

total = 0

map.flatten.each { |v| total+=1 if v ==-1}

puts total

=begin
for i in 0..10
	for j in 0..10
		c = map[i][j]
		print c%10 if c!=-1
		print 'x' if c==-1
	end
	puts ""
end
=end

data.each{ |n, x, y, w, h| 
	#puts "#{n} #{x} #{y} #{w} #{h}"
	catch(:nonunique){
		for i in x..x+w-1
			for j in y..y+h-1
				throw :nonunique if map[i][j]!=n
			end
		end
		puts n
	}	
}