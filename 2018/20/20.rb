
line = STDIN.gets.strip

map={}
stack = []
loc = [0,0]
map[loc]='X'

def move(loc, dir)
	
	return [loc[0]+1, loc[1]] if dir=='E' 
	return [loc[0]-1, loc[1]] if dir=='W'
	return [loc[0], loc[1]-1] if dir=='N' 
	return [loc[0], loc[1]+1] if dir=='S' 
	
end

def door_wall(d, loc)
	if ['E', 'W'].include? d
		return  '|', 
		        [loc[0], loc[1]-1],
				[loc[0], loc[1]+1]
	else
		return '-',
				[loc[0]-1, loc[1]],
				[loc[0]+1, loc[1]]				
				
	end
end


def to_bitmap(map)	
	x1, x2 = map.keys.map{|x, y| x}.minmax
	y1, y2 = map.keys.map{|x, y| y}.minmax
	
	bitmap = Array.new(y2-y1+1){Array.new(x2-x1+1){' '}}
	map.each{|p, c| bitmap[p[1]-y1][p[0]-x1]=c}
	
	return bitmap	
end


def draw(bitmap)
	bitmap.each{|line| puts line.join "" }
end


lastdir = nil

map[ [loc[0]-1, loc[1]-1 ] ]='#'
map[ [loc[0]+1, loc[1]+1 ] ]='#'
map[ [loc[0]+1, loc[1]-1 ] ]='#'
map[ [loc[0]-1, loc[1]+1 ] ]='#'

#draw(bmap)
#puts "-------------"

1.upto(line.size-2){|i|
	if line[i]=='('
		stack << loc		
	elsif line[i]==')'
		loc = stack.pop
	elsif line[i]=='|'
		loc = stack.last		
	else		
		lastdir = line[i]
		loc = move(loc, line[i])
		door_sym, wall1, wall2 =door_wall(line[i], loc)
		map[loc]=door_sym
		#map[wall1]='#'
		#map[wall2]='#'
		loc = move(loc, line[i])
		map[loc]='.'
		
		map[ [loc[0]-1, loc[1]-1 ] ]='#'
		map[ [loc[0]+1, loc[1]+1 ] ]='#'
		map[ [loc[0]+1, loc[1]-1 ] ]='#'
		map[ [loc[0]-1, loc[1]+1 ] ]='#'

	end
}


bmap = to_bitmap(map)

height = bmap.size
width  = bmap[0].size
start_pos = nil

bmap.each_with_index{|line, y|
	line.each_with_index{|c, x|
		if c==' '
			if y==0 or y==height-1 or x==0 or x==height - 1
				line[x]='#'
			else
				line[x]='#'
			end
		elsif c=='X'
			start_pos = [x, y]
		end
	}
}

draw(bmap)


def find_max_bfs(bmap, start_pos)
	width = bmap[0].size
	height = bmap.size
	q = []
	q << [0, start_pos[0], start_pos[1]]
	visited={}
	visited[start_pos]=true
	max = 0
	more_than_1000 = 0
	while !q.empty?
		n, x, y = q.shift
		max = [n, max].max
		more_than_1000 += 1 if n >= 1000
		[[-1, 0],
		 [ 1, 0],
		 [ 0,-1],
		 [ 0, 1]].each{|dx, dy|			
			if x+dx >=0 and x+dx < width and y+dy >= 0 and y < height and !visited[[x+dx*2, y+dy*2]] and ['|', '-'].include? bmap[y+dy][x+dx]				
				visited[[x+dx*2, y+dy*2]]=true
				q << [n+1, x+dx*2, y+dy*2]					
			end
		}
		
	end
	return max, more_than_1000
end

a, b = find_max_bfs(bmap, start_pos)
puts "Part #1: #{a}"
puts "Part #1: #{b}"