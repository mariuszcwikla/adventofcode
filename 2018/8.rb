$values = gets.split.map! {|v| v.to_i}


$sum = 0

def aggregate()
	child = $values.shift
	meta = $values.shift	
	child.times {aggregate}
	meta.times {
		m = $values.shift
		$sum += m
		
	}	
end

aggregate
puts $sum