# useful for testing "ascii-art map" instead of x,y location

#	ruby pour_simulator.rb < 17.test.sim.in

require_relative 'pour'

map = []
while line=gets
	map<<line.strip.split("")
end

x, y = map[0].index('+'), 0

pour(map, x, y)
draw(map)