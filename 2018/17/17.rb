# TODO: work in progress - generating animation

# ruby 17.rb < 17.in
# convert -delay 10 *.png out.gif

# Improvments: render 1000x600 images or smaller...

GENERATE_PNG=true

def to_range(s)
    c, s=s.split("=")
    a,b=s.split[0].scan(/\d+/)
    a=a.to_i
    b=b.to_i if b!=nil
    return [c.to_sym, a,a] if b==nil
    return [c.to_sym, a,b]
end


xs, xe, ys, ye = nil, nil, nil, nil

map_def=[]

while line = gets
    a, b = line.split
    c, t1, t2 = to_range(a)
    if c==:x
        x1,x2=t1,t2
        _, y1, y2 = to_range(b)
    else
        y1, y2 = t1, t2
        _, x1, x2 = to_range(b)
    end
    #puts [x1, x2, y1, y2].join " "
    map_def << [x1, x2, y1, y2]
    
    if xs==nil
        xs, xe = x1, x2
        ys, ye = y1, y2
    else
        xs, xe = [x1, x2, xs, xe].minmax
        ys, ye = [y1, y2, ys, ye].minmax
    end

    
end

width = xe -xs + 1 + 2
height = ye - ys + 1+1

map = Array.new(height){Array.new(width){'.'}}

x, y = 500-xs, 0
map[y][x]='+'

map_def.each{|x1, x2, y1, y2|
    (x1-xs).upto(x2-xs){|x|
        (y1-ys).upto(y2-ys){|y|
            map[y+1][x+1]='#'
        }
    }	
}


require_relative 'pour'
require 'RMagick'
include Magick

def draw_png_map(map, width, height, filename)
	png = Image.new(width, height) { self.background_color = "white" }

	map.each_with_index{|line, y|
		line.each_with_index{|c, x|
			color = 'red' if c=='#'
			color = 'blue' if ['~', '|'].include? c
			png.pixel_color(x, y, color) if color!=nil
		}
	}

	#png.save(filename, :interlace => true)
	png.write(filename)
end

#draw(map)

#puts "width=#{width}, height=#{height}"

frame = 0
Dir.mkdir('tmp') unless File.exist?('tmp')
pour(map, x, y)	{|map|
	next if !GENERATE_PNG
	puts "Frame #{frame}"
	draw_png_map(map, width, height, "tmp/%05d.png" % frame)
	frame+=1
	
}
#draw(map)


n = 0
map.each{|line| 
	n+=line.select{|c| c=='|' or c=='~'}.size
}
puts "Part #1 solution: #{n}"

n = 0
map.each{|line| 
	n+=line.select{|c| c=='~'}.size
}
puts "Part #2 solution: #{n}"



#draw_png_map(map, width, height, '17.png')