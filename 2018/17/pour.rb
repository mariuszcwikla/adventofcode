
def draw(map)
	map.each{|line| puts line.join ""}
end

def get_wall_left(map, x, y)
	while x>=0
		return x if map[y][x]=='#'
		x-=1
	end
	return nil
end

def get_wall_right(map, x, y)
	while x>=0
		return x if map[y][x]=='#'
		x+=1
	end
	return nil
end

def fill_horz(map, c, y, x1, x2, &block)
	x1.upto(x2){|x|
		map[y][x]=c		
	}
	block.call(map) if block_given?
end

def pour_down(map, x, y)
	height = map.size
	while y<height-1 and map[y+1][x]!='#'
		y+=1
		map[y][x]='|'
	end
	return [x,y]
end

def overflow_location_left(map, x, y)
	return nil if y==map.size-1
	
	while x >= 0 
		return nil if map[y][x]=='#'
		return x if map[y+1][x]=='.' or map[y+1][x]=='|'
		x-=1
	end
	return nil
end

def overflow_location_right(map, x, y)
	return nil if y==map.size-1
	width = map[0].size
	while x < width
		return nil if map[y][x]=='#'
		return x if map[y+1][x]=='.' or map[y+1][x]=='|'
		x+=1
	end
	return nil
end

$g_steps = 0

def pour(map, x, y, &block)
	#return if $g_steps>=2000
	$g_steps+=1
	#puts $g_steps
	x, y = pour_down(map, x, y)
	puts y
	return if y>=map.size-1
	while true
		left_o  = overflow_location_left(map, x, y)
		right_o = overflow_location_right(map, x, y)
		break if left_o !=nil or right_o !=nil	
		left  = get_wall_left(map, x, y)
		right = get_wall_right(map, x, y)
		fill_horz(map, '~', y, left+1, x, &block) if left!=nil
		fill_horz(map, '~', y, x, right-1, &block) if right!=nil	
		y-=1
	end	
		
	block.call(map) if block_given?	
	
	if right_o!=nil
		if left_o==nil
			left = get_wall_left(map, x, y)
			fill_horz(map, '|', y, left+1, x, &block) if left!=nil
		end
		if map[y+1][right_o]!='|'			#i.e. already poured here by some previous "branch"
			fill_horz(map, '|', y, x, right_o, &block)
			pour(map, right_o, y, &block)
		end
	end
	
	
	if left_o!=nil
		if right_o==nil
			right = get_wall_right(map, x, y)
			fill_horz(map, '|', y, x, right-1, &block) if right!=nil
		end
		if map[y+1][left_o]!='|'		    #i.e. already poured here by some previous "branch"			
			fill_horz(map, '|', y, left_o, x)
			pour(map, left_o, y, &block)
		end
	end
end
