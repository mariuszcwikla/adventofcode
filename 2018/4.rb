require 'scanf'
logs = []
while line = gets
	logs << line
end

logs.sort!

#puts logs

current = -1

min_start = nil
min_end = nil

ranges = []

$guards = {}
total_asleep={}

logs.each {|log|

	#[1518-07-01 23:59] Guard #1301 begins shift
	#[1518-10-31 00:58] wakes up
	#[1518-07-06 00:01] falls asleep
	
	words = log.split	
	_,min = words[1].scanf("%d:%d]")
	
	if words[2]=='Guard'		
		current,_= words[3].scanf("#%d")		
		if $guards[current]==nil		
			$guards[current]=[]
			total_asleep[current]=0
		end
	elsif words[2]=='falls'		
		min_start = min
	elsif words[2]=='wakes'
		min_end = min
		$guards[current] << [min_start, min_end]
		total_asleep[current] += (min_end - min_start)
	else	
		throw "Uknown word: " + log
	end
	
}

max_asleep_guard = nil
max_asleep_at = nil

def intersect(a,b)
	if b[0] >= a[1]
		return nil
	else
		left = b[0]
		right = [a[1]-1, b[1]].min
		return [left, right]
	end
end

$guards.each {|guardno,ranges| ranges.sort!}

most_sleepy, _ = total_asleep.max_by{ |g, n| n}

def count_at(guardno, minute)
    n = 0
    $guards[guardno].each{|x,y| 
        n+=1 if minute>=x and minute <y
        #tip tu moznaby zoptymalizowac aby wyskoczyc jesli minute >y ;) ale po co :D
    }
	return n
end

minute = nil
quantity = 0
(0..59).each do |m|
	n = count_at(most_sleepy, m)
	if n > quantity
		quantity = n
		minute = m
	end
end


puts "id/minute/no of times: #{most_sleepy}:#{minute}:#{quantity}"
puts "solution #1 = #{most_sleepy * minute}"


at_min = 0
cnt_at_min = 0
guard = nil
$guards.each{|guardno,_|	
	(0..59).each {|m| 
		n = count_at(guardno, m)
		if n > cnt_at_min
			cnt_at_min = n
			at_min = m
			guard = guardno
		end
	}
	
}
puts "solution #2=#{guard} * #{at_min} = #{guard * at_min}"