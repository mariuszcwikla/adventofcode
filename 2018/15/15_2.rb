#!ruby

#$stdin = File.new('15/15.1.in')

require 'optparse'

options = {}
options[:elf_power] = 3
options[:simulation] = false
options[:draw] = false
OptionParser.new do |opts|
  #opts.banner = "Usage: example.rb [options]"

  opts.on("-pPOWER", "--power=POWER", "Elf power. If not specified, then power=3") do |v|
    options[:elf_power] = v.to_i 
  end
  opts.on("-s", "--simulation", "Run simulation") do |v|
	options[:simulation]=true
  end
  opts.on("-d", "--draw", "Draw all steps") do |v|
	options[:draw]=true
  end
end.parse!


class String
	# colorization
	def colorize(color_code)
	  "\e[#{color_code}m#{self}\e[0m"
	end
  
	def red
	  colorize(31)
	end
  
	def green
	  colorize(32)
	end
  
end

map = []

TYPE_ELF = 'E'
TYPE_GNOME = 'G'

class Location
	def initialize(x, y, steps, parent)
		@x = x
		@y = y
		@steps = steps
		@parent = parent
	end
	attr_reader :x, :y, :steps, :parent
	
	def hash
		return @x.hash * 13 + @y.hash
	end
	def ==(other)
		return @x==other.x && @y==other.y
	end
	alias eql? ==
	
	def <=> other
		(r = @y <=> other.y) == 0 ? @x <=> other.x : r
	end
	

	def to_s
		"#{@x}, #{@y}"
	end
end

class Creature
	attr_reader :x, :y, :type, :hit_points,  :attack_power
	attr_writer :hit_points, :attack_power
	
	def initialize x, y, type
		@x = x
		@y = y
		@type = type
		@attack_power = 3
		@hit_points = 200
	end
	
	def <=> other
		(r = @y <=> other.y) == 0 ? @x <=> other.x : r
	end
	
	def to_s
		return "#{@type}: #{x}, #{y}"
	end
	
	def is_opponent(c)
		return c.type != @type
	end
	def dead?
		hit_points <=0
	end

	def hit(c)
		c.hit_points-=@attack_power
	end
	
	def do_bfs(creatures_by_location, map)		
		
		width = map[0].size
		q = []
		q << Location.new(x, y, 0, nil)
		visited = {}		
		visited[[@x, @y]]=true
		
		
		def can_move(x, y, visited, creatures_by_location, map, width)
			return false if x<0 or x>=width or y<0 or y >= map.size-1			
			return false if visited[[x,y]]		
			return false if map[y][x]!='.'
			return true
		end
		
		opponents_found = {}
		
		while !q.empty?
			p = q.shift
			[
				Location.new(p.x, p.y-1, p.steps+1, p),
				Location.new(p.x-1, p.y, p.steps+1, p),
				Location.new(p.x+1, p.y, p.steps+1, p),
				Location.new(p.x, p.y+1, p.steps+1, p),													
				
				
			].each{|pp|			
				if can_move(pp.x, pp.y, visited, creatures_by_location, map, width)
					creature_at = creatures_by_location[[pp.x, pp.y]]
					if creature_at!=nil
						if is_opponent(creature_at)
							if opponents_found.size > 0
								e = opponents_found.first[0]
								raise "Not possible" if e.steps > pp.steps
								break if e.steps < pp.steps		# no need to go further

								opponents_found[pp]=creature_at								
							else
								opponents_found[pp]=creature_at
							end
						end
						#else - same species. No need to track same species.
					else
						visited[[pp.x, pp.y]]	=true
						q << pp
					end
				end
			}			
		end			
		return opponents_found
	end

	def move(creatures_by_location, map)
        if fight(creatures_by_location)
            return
        end        
        opponents = do_bfs(creatures_by_location, map)
		if opponents.size > 0
			loc = opponents.keys.sort[0].parent
			while loc.steps > 1
				loc = loc.parent
			end
			
			@x, @y = loc.x, loc.y if loc.steps == 1
		end
        fight(creatures_by_location)
        
	end
    
    def get_opponent(creatures_by_location)
        opponent = nil
		[
		Location.new(@x, @y-1, 0, nil),
		Location.new(@x-1, @y, 0, nil),
		Location.new(@x+1, @y, 0, nil),		
		Location.new(@x, @y+1, 0, nil),													
		].each{|pp|			
			c_at = creatures_by_location[[pp.x, pp.y]]
			if c_at!=nil and is_opponent(c_at) and !c_at.dead?
				if opponent==nil or c_at.hit_points < opponent.hit_points
					opponent = c_at
				end				
			end
		}
        return opponent
    end
	
	def fight(creatures_by_location)
        return false if dead?
        opponent = get_opponent(creatures_by_location)	
		if opponent !=nil
            hit(opponent) 
            return true
        else
            return false
        end
	end
end


def move(creatures, map)
	creatures.sort!
	
	h = {}
	creatures.each{|cc| h[[cc.x, cc.y]]=cc}			
	
	creatures.each{|c|
		prev = [c.x, c.y]		
		c.move(h, map)
		h.delete prev
		h[[c.x, c.y]] = c
	}
end

def draw(creatures, map)	
	map.each_with_index{|line, y|
		x = line.split ""
		creatures.each{|c| 
			if c.y==y
				s = c.type==TYPE_ELF ? c.type.green() : c.type.red()
				x[c.x]=s 
			end
		}		
		puts x.join ""
	}	
	puts "hit points: " + (creatures.map{|c| "(#{c.type})=#{c.hit_points}"}.join " ")
	puts ""
end


def fight(creatures)
	h = {}
	creatures.each{|c| h[[c.x, c.y]]=c}			
	creatures.each{|p|
		next if p.dead?
	#	p.fight(h)		
	}
	creatures.delete_if{|c| c.dead?}
end

creatures = []
y = 0

while line = gets
	line = line.strip
	for x in 0..(line.size-1)
		if ['G', 'E'].include? line[x]
			creatures << Creature.new(x, y, line[x])
			line[x]='.'
		end
	end
	map<<line
	y+=1
end

def cls	
	puts "\e[H\e[2J"
end
def has_both(creatures)
	a = creatures.find{|c| c.type==TYPE_ELF}
	b = creatures.find{|c| c.type==TYPE_GNOME}
	return true if (a!=nil and b!=nil)
	return false
end


def play(creatures, elf_power, map ,draw, simulation)
	creatures = creatures.map{|c|
		cloned = c.clone
    cloned.attack_power = elf_power if c.type == TYPE_ELF
    cloned
	}
	rounds = 0
	while has_both(creatures)
		cls if simulation
		if draw or simulation
			puts "round #{rounds}"
			draw(creatures, map)
		end
		move(creatures, map)
		fight(creatures)
		sleep 0.1 if simulation
		rounds +=1 
	end

	#draw(creatures, map)
	return [rounds, creatures]
end

p = 3
begin 
  rounds, final_creatures = play(creatures, p, 
		map, options[:draw], options[:simulation])
	won_by = final_creatures[0].type 
	puts "At power=#{p} won by %s" % (won_by==TYPE_ELF ? "ELVES" : "GNOMES")
	p+=1

  num_of_elves = creatures.count{|x| x.type==TYPE_ELF}
  num_of_elves_after = final_creatures.count{|x| x.type==TYPE_ELF}
  puts "Elves died: #{num_of_elves - num_of_elves_after}"
  
end until num_of_elves==num_of_elves_after

draw(final_creatures, map)
	
hpsum = final_creatures.map{|c| c.hit_points}.sum
puts "-----------------------------------"
puts "rounds=#{rounds}, sum of hp=#{hpsum}"
puts "outcome=#{rounds * hpsum}"

puts "------------- HACK ----------------"
puts "I don't know why num of rounds in most of the cases greater by 1"
puts "score using round-1 is:"

puts "#{rounds-1} * #{hpsum} = #{(rounds-1) * hpsum}"
