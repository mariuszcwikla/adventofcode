$values = gets.split.map! {|v| v.to_i}


class Node
	def initialize()
		@nodes = []
		@meta  = []
	end
	def addnode(n)
		@nodes << n
	end
		
	attr_accessor :meta
	
	def sum()
		if @nodes.size==0
			return @meta.sum
		else
			sum = 0
			@meta.each{|m|
				if m-1 < @nodes.size
					sum += @nodes[m-1].sum()
				end
			}
			return sum
		end
	end
end

$nodes = {}


def buildtree()
	node = Node.new
	children = $values.shift
	meta = $values.shift	
	children.times {|i|
		c = buildtree()
		node.addnode(c)
	}	
	meta.times {
		m = $values.shift
		node.meta << m				
	}
	
	return node
end

root = buildtree()

sum = root.sum()

puts sum