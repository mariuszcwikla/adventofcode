

def play_game(num_of_players, points)	
	marbles = [0]
	player_points = Array.new(num_of_players){0}
	current_marble = 1
	current_player = 0
	for i in 1..points	
		#puts "#{i}/#{points} iterations" if i%100000==0
		#puts "#{i} #{marbles.join ','} (#{marbles[current_marble]})"
		if i%23 == 0 
			p = i
			m_left = (current_marble - 7) % marbles.size
			p += marbles[m_left]
			player_points[current_player] += p
			marbles.delete_at(m_left)
			current_marble = m_left
		else
			current_marble = (current_marble + 2) % marbles.size
			if current_marble==0			
				marbles << i
				current_marble = marbles.size-1
			else
				marbles.insert(current_marble, i)
			end
		end
		current_player += 1
		current_player = 0 if current_player == num_of_players
	end
	
	return player_points.max	
end

while line=gets
	e = line.split
	n, p = e[0].to_i, e[6].to_i
	score = play_game(n, p)
	puts score	
	#"brute force" runs too long - see 9_2 for solution using linked list
	#puts "100x = #{play_game(n, p*100)}"		
end

