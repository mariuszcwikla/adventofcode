require 'scanf'
require 'set'
class Instruction
    def initialize
        @reg_a=0
        @reg_b=0
        @reg_c=0
        @reg_d=0
    end
    def call(a, b)
    end
end

ops = {}
ops[:addr] = lambda {|r, a, b, c| r[c]=r[a]+r[b]}
ops[:addi] = lambda {|r, a, b, c| r[c]=r[a]+b}

ops[:mulr] = lambda {|r, a, b, c| r[c]=r[a]*r[b]}
ops[:muli] = lambda {|r, a, b, c| r[c]=r[a]*b}

ops[:banr] = lambda {|r, a, b, c| r[c]=r[a] & r[b]}
ops[:bani] = lambda {|r, a, b, c| r[c]=r[a] & b}

ops[:borr] = lambda {|r, a, b, c| r[c]=r[a] | r[b]}
ops[:bori] = lambda {|r, a, b, c| r[c]=r[a] | b}

ops[:setr] = lambda {|r, a, b, c| r[c]=r[a]}
ops[:seti] = lambda {|r, a, b, c| r[c]=a}

ops[:gtir] = lambda {|r, a, b, c| r[c]= (a > r[b] ? 1 : 0)}
ops[:gtri] = lambda {|r, a, b, c| r[c]= (r[a] > b ? 1 : 0)}
ops[:gtrr] = lambda {|r, a, b, c| r[c]= (r[a] > r[b] ? 1 : 0)}

ops[:eqir] = lambda {|r, a, b, c| r[c]= (a == r[b] ? 1 : 0)}
ops[:eqri] = lambda {|r, a, b, c| r[c]= (r[a] == b ? 1 : 0)}
ops[:eqrr] = lambda {|r, a, b, c| r[c]= (r[a] == r[b] ? 1 : 0)}

def determine_opcode(ops, before, stmt, after)
    opcodes = []
    ops.each{|k, v|
        r = before.clone
        v.call(r, stmt[1], stmt[2], stmt[3])
        opcodes << k if r==after
    }
    return opcodes
end

part1 = 0


program = []

samples=[]

while line=gets
    next if line.strip.empty?
    if line.start_with? "Before"
        before= line.scanf "Before: [%d, %d, %d, %d]"
        stmt  = gets.scanf("%d %d %d %d")
        after = gets.scanf("After: [%d, %d, %d, %d]")
        opcodes = determine_opcode(ops, before, stmt, after)
#        puts "opcodes at #{stmt.join ' '}: #{opcodes.join ' '}"
        part1 +=1 if opcodes.size>=3

        samples << [stmt[0], opcodes]
    else
        #Part 2
        stmt=line.split.map{|s| s.to_i}
        program << stmt if stmt.size==4
    end
end

puts "Part 1: #{part1}"


opcode_map={}

begin
    determined = false
    set = Set.new
    samples.each{|num, opcodes|
        if opcodes.size==1
            determined = true
            set << opcodes[0]
            if opcode_map[num]==nil
                opcode_map[num]=opcodes[0]
            elsif opcode_map[num]!=opcodes[0]
                raise 'Two opcodes map to the same number: #{num} -> #{opcode_map[num]}, #{opcodes[0]}'
            end
        end
    }

    samples.delete_if{|n, opcodes| opcodes.size==1}
    samples.each{|n, opcodes|
        set.each{|op| opcodes.delete op}
    }

end while determined and samples.size>0

#samples.each{|n, op| puts "#{n} => #{op.join ' '}"}

puts "Determined opcodes: " + opcode_map.to_s
puts "Undetermined size: #{samples.size}"

puts opcode_map[1].class

puts "Executing program..."
r = [0,0,0,0]
n = 0
program.each{|instr|
   #  puts instr
   #  puts "line #{n}"
   #  n+=1
    opcode = opcode_map[instr[0]]
    ops[opcode].call(r, instr[1], instr[2], instr[3])
}


puts "final registers: #{r.join ' '}"
