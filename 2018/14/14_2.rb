=begin

Run:

	ruby 14_2.rb 320851

=end

$recipes = Array.new(1000000)			#if I preallocate array, it runs a lot faster...
$recipes[0]=3
$recipes[1]=7
$size = 2


def next_step(a, b)
	
	c = $recipes[a] + $recipes[b]	
	(c.to_s.split "").each{|x| 
		$recipes[$size]=x.to_i
		$size+=1
	}
	
	
	a+=$recipes[a]+1
	b+=$recipes[b]+1
	a%=$size
	b%=$size
	
	return a, b
end

if ARGV[0]==nil
	puts 'argument missing. e.g.:'
	puts ''
	puts 'ruby 14_otpimiz.rb 320851'	
	return
end

def matches(target, i)
	#TODO improvement by just comparing 6-digit int values
	
	target.each_with_index{|x, j|			
		return false if x != $recipes[i+j]		
	}
	return true
end

a, b = 0, 1

target = (ARGV[0].split "").map{|i|i.to_i}
i = 1

while true
	a, b = next_step(a, b)
	#puts $recipes.join " "
	#print some progress
	puts $size if [0, 1].include? $size % 10000				#0, 1 because sometimes only 1 recipe is added
	
	if $size > target.size
		if matches(target, $size - target.size-1)
			puts $size - target.size - 1 
			return
		elsif matches(target, $size - target.size)
			puts $size - target.size
			return
		end
	end
	i+=1
end