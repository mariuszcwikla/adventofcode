
$recipes = [3, 7]


def next_step(a, b)
	
	c = $recipes[a] + $recipes[b]	
	
	$recipes += (c.to_s.split "").map{|x| x.to_i}
	
	a+=$recipes[a]+1
	b+=$recipes[b]+1
	a%=$recipes.size
	b%=$recipes.size
	
	return a, b
end




a, b = 0, 1

16.times{|i|
	puts %((#{i+1}) [#{$recipes[a]}, #{$recipes[b]}]: #{$recipes.join " "})
	
	a, b = next_step(a, b)	
}


target = ARGV[0].to_i

while target + 10 > $recipes.size
	a, b = next_step(a, b)
	puts $recipes.size if [0, 1].include? $recipes.size % 10000				#0, 1 because sometimes only 1 recipe is added
end


puts %(#{$recipes[target..(target+9)].join ""})
