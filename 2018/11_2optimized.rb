#brute force algorithm for part #2

def power_level(x, y, serial_num)
	r = x + 10
	v = (r*y+serial_num)*r
	v /= 100
	v = v % 10
	return v - 5
end
	


SIZE = 300
serial_num=8444
#serial_num=42

$powers = Array.new(SIZE){Array.new(SIZE)}
for y in 1..(SIZE)
	for x in 1..(SIZE)
		$powers[y-1][x-1]=power_level(x, y, serial_num)
	end
end


def total_power(x, y, size)
	sum = 0
	for i in 0..size-1
		for j in 0..size-1
			sum+=$powers[y-1+i][x-1+j]
			#sum+=power_level(x+i, y+j, 8444)
		end
	end
	return sum
end

def vertical_sum(x, y, size)
	s = 0
	for yy in y..(y+size-1)
		s+=$powers[yy-1][x-1]
	end
	return s
end

def total_power_from_previous(x, y, size, s)
	return s - vertical_sum(x-1, y, size) + vertical_sum(x+size-1, y, size)
end

def dump(n)
	for y in 1..n
		for x in 1..n
			printf "%d " % $powers[y-1][x-1]
		end
		puts ""
	end
end

dump(5)

def maxof(m, x, y, s, size)
	if m==nil or m[2] < s
		return [x, y, s, size]
	end
	return m
end

max = nil
for size in 3..(SIZE-1)
	
	max_current = nil	
	for y in 1..(SIZE-size)
		s = total_power(1, y, size)		
		max = maxof(max, 1, y, s, size)
		max_current = maxof(max_current, 1, y, s, size)
		for x in 2..(SIZE-size)
			s = total_power_from_previous(x, y, size, s)	
			#puts "s=#{s}" if x==242 and y==67
			max = maxof(max, x, y, s, size)
			max_current = maxof(max_current, x, y, s, size)			
		end
	end
	#puts "p=#{max_current[2]}, [#{max_current[0]}, #{max_current[1]}]"
	puts "size=#{size}, max=#{max_current[2]}"
end


#answer: 236,252,12
puts "#{max[0]},#{max[1]},#{max[3]}"